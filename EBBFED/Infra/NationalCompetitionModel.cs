﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EBBFED.Infra
{
    public class NationalCompetitionModel
    {
        public NationalCompetitionModel()
        {
            Ages = new List<long>();
        }
        public long ID { get; set; }
        [Display(Name ="الاسم عربى")]
        public string NameA { get; set; }
        [Display(Name = "الاسم انجليزى")]
        public string NameE { get; set; }
        [Display(Name = "الجنس")]
        public long Gender { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        [Display(Name = "التاريخ")]
        public DateTime Date { get; set; }
        [Display(Name = "العمر")]
        public List<long> Ages { get; set; }
    }
}