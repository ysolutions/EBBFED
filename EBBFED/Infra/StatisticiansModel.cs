﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EBBFED.Infra
{
    public class StatisticiansModel
    {
        public long ID { get; set; }
        [Display(Name = "الاسم انجليزى")]
        public string NameE { get; set; }
        [Display(Name = "الاسم عربى")]
        public string NameA { get; set; }
        [Display(Name = "الفرع")]
        public long? BranchId { get; set; }
        [Display(Name = "النادى")]
        public long? ClubId { get; set; }

        //public virtual Branch Branch { get; set; }
        //public virtual Club Club { get; set; }
    }
}