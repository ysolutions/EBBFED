﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EBBFED.Infra
{
    public class ArticlesModel
    {
        public long ID { get; set; }
        [Display(Name ="الاسم عربى")]
        public string TitleA { get; set; }
        [Display(Name = "الاسم انجليزى")]
        public string TitleE { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه قصيره عربى")]
        public string ShortDescriptionA { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه قصيره انجليزى")]
        public string ShortDescriptionE { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه طويله انجليزى")]
        public string LongDescriptionE { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه طويله عربى")]
        public string LongDescriptionA { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        [Display(Name = "التاريخ")]
        public DateTime Date { get; set; }
        public string ImageUrl { get; set; }
    }
}