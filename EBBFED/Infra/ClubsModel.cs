﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EBBFED.Infra
{
    public class ClubsModel
    {
        public long ID { get; set; }
        [Display(Name ="الاسم انجليزى")]
        public string NameE { get; set; }
        [Display(Name = "الاسم عربى")]
        public string NameA { get; set; }
        [Display(Name = "الدرجه")]
        public long DivisionID { get; set; }
        [Display(Name = "العمر")]
        public long AreaID { get; set; }
        [Display(Name = "الشعار")]
        public string Logo { get; set; }
        [Display(Name = "سنه الانشاء")]
        public int EstablishedYear { get; set; }
        [AllowHtml]
        [Display(Name ="القاب ")]
        public string TitlesWon { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه عربى ")]
        public string DescriptionA { get; set; }
        [AllowHtml]
        [Display(Name = " مقدمه انجليزى")]
        public string DescriptionE { get; set; }
    }
}