﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EBBFED.Infra
{
    public class HistoryUnionModel
    {
        public long ID { get; set; }
        [AllowHtml]
        public string EstablishedYearA { get; set; }
        [AllowHtml]
        public string EstablishedYearE { get; set; }
        [AllowHtml]
        public string AchievementA { get; set; }
        [AllowHtml]
        public string AchievementE { get; set; }
    }
}