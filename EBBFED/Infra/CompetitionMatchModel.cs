﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EBBFED.Infra
{
    public class CompetitionMatchModel
    {
        public long ID { get; set; }
        [Display(Name ="الفريق الاول")]
        public long TeamA { get; set; }
        [Display(Name ="الفريق الثانى")]
        public long TeamB { get; set; }
        [Display(Name =" المسابقه")]
        public long CompetitionID { get; set; }
        [Display(Name = "العمر")]
        public long AgesID { get; set; }
        [Display(Name = "عدد اهداف الفريق الاول")]
        public int ScoreA { get; set; }
        [Display(Name = "عدد اهداف الفريق الثانى")]
        public int ScoreB { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        [Display(Name = "التاريخ")]
        public DateTime Date { get; set; }
        [DisplayFormat(DataFormatString = @"{0:hh\:mm}")]
        [Display(Name = "الوقت")]
        public TimeSpan Time { get; set; }
    }
}