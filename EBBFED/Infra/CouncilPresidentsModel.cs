﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EBBFED.Infra
{
    public class CouncilPresidentsModel
    {
        public long ID { get; set; }
        public string ImageUrl { get; set; }
        [Display(Name="الاسم عربى")]
        public string NameA { get; set; }
        [Display(Name = "الاسم انجليزى")]
        public string NameE { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه عربى")]
        public string BriefA { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه انجليزى")]
        public string BriefE { get; set; }
        [Display(Name = "اللقب")]
        public string TitleA { get; set; }
        [Display(Name = "اللقب")]
        public string TitleE { get; set; }
    }
}