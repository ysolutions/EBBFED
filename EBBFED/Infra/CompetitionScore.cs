﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBBFED.Infra
{
    public class CompetitionScore
    {
        public long ID { get; set; }
        public string TeamA { get; set; }
        public string TeamB { get; set; }
        public int ScoreA { get; set; }
        public int ScoreB { get; set; }
        public string Date { get; set; }
        public string LogoA { get; set; }
        public string LogoB { get; set; }
        public TimeSpan? Time { get; set; } //Added by Islam Mohamed
        public string Location { get; set; } //Added by Islam Mohamed
        public string CompetitionNameA { get; set; } //Added by Islam Mohamed
        public string CompetitionNameE { get; set; } //Added by Islam Mohamed

    }
}