﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EBBFED.Infra
{
    public class NewsModel
    {
        public NewsModel()
        {
            Clubs = new List<long>();
            Nationals = new List<long>();
            Players = new List<long>();
        }
        public long ID { get; set; }
        [Display(Name = "العنوان انجليزى")]
        public string TitleE { get; set; }
        [Display(Name = "العنوان عربى")]
        public string TitleA { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه قصيره انجليزى")]
        public string ShortDescriptionE { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه قصيره عربى")]
        public string ShortDescriptionA { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه طويله انجليزى")]
        public string LongDescriptionE { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه قصيره عربى")]
        public string LongDescriptionA { get; set; }
        [Display(Name = "لينك المشاهده")]
        public string ShareLink { get; set; }
        [Display(Name = "النوادى")]
        public List<long> Clubs { get; set; }
        [Display(Name = "المنتخبات")]
        public List<long> Nationals { get; set; }
        [Display(Name = "اللاعبين")]
        public List<long> Players { get; set; }
        [Display(Name = "الصوره")]
        public string ImageUrl { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Display(Name = "المشاهدات")]
        public long Views { get; set; }
        [Display(Name = "مهمه")]
        public bool Important { get; set; }
    }
}