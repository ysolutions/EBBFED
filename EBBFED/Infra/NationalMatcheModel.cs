﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EBBFED.Infra
{
    public class NationalMatcheModel
    {
        public long ID { get; set; }
        [Display(Name ="النادى الاول")]
        public long NationalA { get; set; }
        [Display(Name = "النادى الثانى")]
        public long NationalB { get; set; }
        [Display(Name = "المسابفه")]
        public long Competition { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        [Display(Name = "التاريخ")]
        public DateTime Date { get; set; }
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = @"{0:hh\:mm}")]
        [Display(Name = "الوقت")]
        public TimeSpan Time { get; set; }
        [Display(Name = "المكان")]
        public string Place { get; set; }
        [Display(Name = "البث المباشر")]
        public string LiveLink { get; set; }
        [Display(Name = "عدد اهداف الفريق الاول")]
        public int ScoreA { get; set; }
        [Display(Name = "عدد اهداف الفرق الثانى ")]
        public int ScoreB { get; set; }
        [Display(Name = "المسابقه")]
        public long CompetitionID { get; set; }
    }
}