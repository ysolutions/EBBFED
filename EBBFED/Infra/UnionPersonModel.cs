﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EBBFED.Infra
{
    public class UnionPersonModel
    {
        public long ID { get; set; }
        [Display(Name ="الصوره")]
        public string ImageUrl { get; set; }
        [Display(Name = "الاسم عربى")]
        public string NameA { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه عربى")]
        public string BriefA { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه انجليزى")]
        public string BriefE { get; set; }
        [Display(Name = "الاسم انجليزى")]
        public string NameE { get; set; }
    }
}