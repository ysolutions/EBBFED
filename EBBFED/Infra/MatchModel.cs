﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using EBBFED.Models;
namespace EBBFED.Infra
{
    public class MatchModel
    {
        public long ID { get; set; }
        [Required]
        [Display(Name ="النادى الاول")]
        public long ClubA { get; set; }
        [Required]
        [Display(Name = "النادى الثانى")]
        public long ClubB { get; set; }
        [Required]
        [Display(Name = " الدرجه")]
        public long DivisionID { get; set; }
        [Required]
        [Display(Name = "الجوله")]
        public long RoundID { get; set; }
        [Required]
        [Display(Name = " الجنس")]
        public long Gender { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        [Display(Name = " التاريخ")]
        public DateTime Date { get; set; }
        [Display(Name = " اهداف الفريق الاول")]
        public int ScoreA { get; set; }
        [Display(Name = "اهداف الفريق الثانى")]
        public int ScoreB { get; set; }
        [Display(Name = " المكان")]
        public string Place { get; set; }
        [Display(Name = "البث المباشر")]
        public string LiveLink { get; set; }
        [Required]
        [Display(Name = "العمر")]
        public long Age { get; set; }
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = @"{0:hh\:mm}")]
        [Display(Name = "الوقت")]
        public TimeSpan Time { get; set; }
    }
    public class MatchScoreModel
    {
        public long ID { get; set; }
        public string ClubA { get; set; }
        public string ClubB { get; set; }
        public long Division { get; set; }
        public string Gender { get; set; }
        public DateTime Date { get; set; }
        public int ScoreA { get; set; }
        public int ScoreB { get; set; }
        public string Place { get; set; }
        public string LiveLink { get; set; }
        public long Age { get; set; }
        public string LogoA { get; set; }
        public string LogoB { get; set; }
    }
    public class Gender
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}