﻿using EBBFED.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBBFED.Infra
{
    public class MatchesTableModel
    {
        public Club ClubA { get; set; }
        public Club ClubB { get; set; }
        public int NumberOfMatches { get; set; }
        public int Winner { get; set; }
        public int Lose { get; set; }
        public int Points { get; set; }
        public int INGoals { get; set; }
        public int OutGoals { get; set; }
        public int Draw { get; set; }
        public int Withdraw { get; set; }
        public int DivisionID { get; set; }
        public int AgeID { get; set; }
        public int RoundID { get; set; }
        public string Gender { get; set; }
        public Division Division { get; set; }
        public Round Round { get; set; }
        public Age Age { get; set; }
    }
}