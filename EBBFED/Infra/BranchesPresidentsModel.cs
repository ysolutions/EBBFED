﻿using EBBFED.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EBBFED.Infra
{
    public class BranchesPresidentsModel
    {
        public long ID { get; set; }
        public string ImageUrl { get; set; }
        [Display(Name = "الاسم عربى")]
        public string NameA { get; set; }
        [Display(Name = "الاسم انجليزى")]
        public string NameE { get; set; }
        [AllowHtml]
        [Display(Name = "نبذه عربى")]
        public string BriefA { get; set; }
        [AllowHtml]
        [Display(Name = "نبذه انجليزى")]
        public string BriefE { get; set; }
        [Display(Name = " اللقب عربى")]
        public string TitleA { get; set; }
        [Display(Name = "اللقب انجليزى")]
        public string TitleE { get; set; }
        [Display(Name = "إضافه كرئيس مجلس الاداره؟")]
        public bool? IsChairman { get; set; }
        public long? BranchId { get; set; }
        public virtual Branch Branch { get; set; }
    }
}