﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EBBFED.Infra
{
    public class BranchModel
    {
        public long ID { get; set; }
        [Display(Name = "الاسم انجليزى")]
        public string NameE { get; set; }
        [Display(Name = "الاسم عربى")]
        public string NameA { get; set; }
        [Display(Name = "هاتف 1")]
        public string Phone1 { get; set; }
        [Display(Name = "هاتف 2")]
        public string Phone2 { get; set; }
        [Display(Name = "العنوان انجليزى")]
        public string AddressE { get; set; }
        [Display(Name = "العنوان عربى")]
        public string AddressA { get; set; }
        [Display(Name = "البريد الالكترونى")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "البريد الالكترونى غير صحيح")]
        public string Email { get; set; }
        [Display(Name = "الفاكس")]
        public string Fax { get; set; }
    }
}