﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EBBFED.Infra
{
    public class RulesAndLawsModel
    {
        public long ID { get; set; }
        [Display(Name ="الاسم عربى")]
        public string NameA { get; set; }
        [Display(Name = "الفايل ")]
        public string PdfName { get; set; }
        [Display(Name = "الاسم انجليزى")]
        public string NameE { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه عربى")]
        public string ShortDescriptionA { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه انجليزى")]
        public string ShortDescriptionE { get; set; }
    }
}