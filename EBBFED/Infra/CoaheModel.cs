﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EBBFED.Infra
{
    public class CoaheModel
    {
        public long ID { get; set; }
        [Display(Name ="الاسم عربى")]
        public string NameA { get; set; }
        [Display(Name = "الاسم انجليزى")]
        public string NameE { get; set; }
        [Display(Name = " الصوره")]
        public string ImageUrl { get; set; }
        [Display(Name = " العمر")]
        public int Age { get; set; }
        [Display(Name = " النادى")]
        public long ClubID { get; set; }
        [Display(Name = " تصنيفه")]
        public string Category { get; set; }
        [AllowHtml]
        [Display(Name = " مقدمه عربى ")]
        public string DescriptionA { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه انجليزى")]
        public string DescriptionE { get; set; }
        [AllowHtml]
        [Display(Name = "الالقاب عربى")]
        public string TitleA { get; set; }
        [AllowHtml]
        [Display(Name = " الالقاب انجليزى")]
        public string TitleE { get; set; }
    }
}