﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EBBFED.Infra
{
    public class DecisionModel
    {
        public long ID { get; set; }
        [Display(Name="العنوان عربى")]
        public string TitleA { get; set; }
        [Display(Name = "العنوان انجليزى")]
        public string TitleE { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه قصيره عربى")]
        public string ShortDescriptionA { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه قصيره نجليزى")]
        public string ShortDescriptionE { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه طويله انجليزى")]
        public string LongDescriptionE { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه طويله عربى")]
        public string LongDescriptionA { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}