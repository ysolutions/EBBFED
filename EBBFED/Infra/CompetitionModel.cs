﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EBBFED.Infra
{
    public class CompetitionModel
    {
        public CompetitionModel()
        {
            Ages = new List<long>();
        }
        public long ID { get; set; }
        [Display(Name ="الاسم عربى")]
        public string NameA { get; set; }
        [Display(Name = "الاسم انجليزى")]
        public string NameE { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        [Display(Name = "التاريخ")]
        public DateTime Date { get; set; }
        [Display(Name = "المكان")]
        public string Location { get; set; }
        [Display(Name = "لينك التسجيل")]
        public string RegisterLink { get; set; }
        [Display(Name = "العمر")]
        public List<long> Ages { get; set; }
    }
    public class AllCompetitionModel
    {
        public long ID { get; set; }
        public string NameA { get; set; }
        public string NameE { get; set; }
        public string Date { get; set; }
        public string Location { get; set; }
        public string RegisterLink { get; set; }
    }
}