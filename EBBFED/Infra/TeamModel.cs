﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EBBFED.Infra
{
    public class TeamModel
    {
        public long ID { get; set; }
        [Display(Name = "اسم الفريق عربى")]
        public string NameA { get; set; }
        [Display(Name = "اسم الفريق انجليزى")]
        public string NameE { get; set; }

    }
}