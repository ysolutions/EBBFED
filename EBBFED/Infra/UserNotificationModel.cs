﻿using EBBFED.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Data;
using System.Data.Entity;
using System.Web.Mvc;
using EBBFED.Infra;
using System.Web.UI;
using System.Text.RegularExpressions;

namespace EBBFED.Infra
{
    public class UserSubscribe
    {
        public string Email { get; set; }
    }
    public class SendEmail:Page {
        public void Send(string EmailSubject="",string Category="",string HeaderTitle = "", string imgUrl = "", string LinkText = "", string shortBrief = "", string Body = "")
        {
            string filepath = Server.MapPath("~/Content/Email-Template.html");
            string dest = Server.MapPath("~/Content/");
            string newFileName = Guid.NewGuid()+"Email.html";
            string newPath = Path.Combine(dest, newFileName);
            File.Copy(filepath, newPath,true);

            StreamReader objReader = new StreamReader(newPath);
            string content = objReader.ReadToEnd();
            objReader.Close();

            content = Regex.Replace(content,"{Category}",Category);
            content = Regex.Replace(content, "{Date}", DateTime.Now.Date.ToShortDateString());
            content = Regex.Replace(content, "{HeaderTitle}", HeaderTitle);
            content = Regex.Replace(content, "{shortBrief}", shortBrief);
            content = Regex.Replace(content, "{imgUrl}", imgUrl);
            content = Regex.Replace(content, "{linkText}", LinkText);
            content = Regex.Replace(content, "{Body}", Body);

            StreamWriter writer = new StreamWriter(newPath);
            writer.Write(content);
            writer.Close();

            StreamReader EmailBody = new StreamReader(newPath);
            string EmailContent = EmailBody.ReadToEnd();
            EmailBody.Close();
            UserNotificationModel.SendEmail(EmailSubject, EmailContent);
            File.Delete(newPath);
        }
    } 
    public static class UserNotificationModel
    {
        
        public static bool SendEmail(string subject, string body)
        {
            KoraEntities db = new KoraEntities();
            #region Old
            //try
            //{
            //    List<string> emails = new List<string>();

            //    var registeredUsers = db.UserNotifications.ToList();

            //    for (int i = 0; i < registeredUsers.Count; i++)
            //    {
            //        if (registeredUsers[i].IsSubscribed==true)
            //        {
            //            emails.Add(registeredUsers[i].Email);
            //        }
            //    }

            //    string senderEmail = "aspmailuser@gmail.com";
            //    string senderPassword = "Allah1Akbar";

            //    SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            //    client.EnableSsl = true;
            //    client.Timeout = 10000;
            //    client.DeliveryMethod = SmtpDeliveryMethod.Network;
            //    client.UseDefaultCredentials = false;
            //    client.Credentials = new NetworkCredential(senderEmail, senderPassword);

            //    MailMessage mailMessage = new MailMessage();

            //    mailMessage.From = new MailAddress(senderEmail);
            //    for (int i = 0; i < emails.Count; i++)
            //    {
            //        mailMessage.To.Add(emails[i]);
            //    }
            //    mailMessage.Subject = subject;
            //    mailMessage.Body = body;

            //    mailMessage.IsBodyHtml = true;
            //    mailMessage.BodyEncoding = UTF8Encoding.UTF8;
            //    client.Send(mailMessage);

            //}
            //catch (Exception ex)
            //{

            //    throw;
            //} 
            #endregion

            MailMessage message = new MailMessage();
            SmtpClient smtpClient = new SmtpClient();
            string msg = string.Empty;
            try
            {
                MailAddress fromAddress = new MailAddress(ConfigurationManager.AppSettings["SendMail.From"]);
                List<string> emails = new List<string>();

                var registeredUsers = db.UserNotifications.ToList();

                for (int i = 0; i < registeredUsers.Count; i++)
                {
                    if (registeredUsers[i].IsSubscribed == true)
                    {
                        emails.Add(registeredUsers[i].Email);
                    }
                }
                for (int i = 0; i < emails.Count; i++)
                {
                    message.To.Add(emails[i]);
                }
                message.From = fromAddress;
                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = body;

                string fromPassword = ConfigurationManager.AppSettings["SendMail.Password"];
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = ConfigurationManager.AppSettings["SendMail.Host"];
                    smtp.Port = int.Parse(ConfigurationManager.AppSettings["SendMail.Port"]);
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["SendMail.From"], fromPassword);
                    smtp.Timeout = 20000;

                }
                smtp.Send(message);
            }
            catch (Exception ex)
            {
                return true;
            }
            return true;
        }
    }
}