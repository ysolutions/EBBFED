﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EBBFED.Infra
{
    public class UnoinCommitteModel
    {
        public UnoinCommitteModel()
        {
            MemberNameA = new List<string>();
            MemberNameE = new List<string>();
            MemberNameAe = new List<string>();
            MemberNameEe = new List<string>();
        }
        public long ID { get; set; }
        public string ImageUrl { get; set; }
        public string NameA { get; set; }
        public string NameE { get; set; }
        [AllowHtml]
        public string ObjectivesA { get; set; }
        [AllowHtml]
        public string ObjectivesE { get; set; }
        public List<string> MemberNameA { get; set; }
        public List<string> MemberNameE { get; set; }
        public List<string> MemberNameAe { get; set; }
        public List<string> MemberNameEe { get; set; }
    }
}