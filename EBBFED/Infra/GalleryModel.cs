﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EBBFED.Infra
{
    public class GalleryModel
    {
        public GalleryModel()
        {
            Clubs = new List<long>();
            Players = new List<long>();
        }
        public long ID { get; set; }
        public string ImageUrl { get; set; }
        public string VideoUrl { get; set; }
        [AllowHtml]
        [Display(Name ="مقدمه عربى")]
        public string DescriptionA { get; set; }
        [AllowHtml]
        [Display(Name = "مقدمه انجليزى")]
        public string DescriptionE { get; set; }
        [Display(Name ="النادى")]
        public List<long> Clubs { get; set; }
        [Display(Name = "اللاعبين")]
        public List<long> Players { get; set; }
        [Display(Name = "التاريخ")]
        public DateTime Date { get; set; }


    }
}