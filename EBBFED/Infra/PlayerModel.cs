﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EBBFED.Infra
{
    public class PlayerModel
    {
        public PlayerModel()
        {
            TransfarFrom = new List<string>();
            TransfarTo = new List<string>();
            Year = new List<int>();
            ClubName = new List<string>();
            YearFrom = new List<string>();
            YearTo = new List<string>();
            TransfarFrome = new List<string>();
            TransfarToe = new List<string>();
            Yeare = new List<int>();
        }
        public long ID { get; set; }
        [Display(Name ="الصوره")]
        public string ImageUrl { get; set; }
        [Display(Name = "العمر")]
        public int Age { get; set; }
        [Required]
        [Display(Name = "المكان")]
        public long Position { get; set; }
        [Display(Name = "الطول")]
        public int Height { get; set; }
        [Display(Name = "العرض")]
        public int Weight { get; set; }
        [Display(Name = "رقم اللاعب")]
        public int PlayerNumber { get; set; }
        [Display(Name = "الماتشات الدوليه")]
        public int InternationalMatches { get; set; }
        [Display(Name = "النادى")]
        public long Club { get; set; }
        [Display(Name = "انتقل من")]
        public List<string> TransfarFrom { get; set; }
        [Display(Name = "انتقل الى")]
        public List<string> TransfarTo { get; set; }
        [Display(Name = "سنه")]
        public List<int> Year { get; set; }
        [Display(Name = "النادى")]
        public List<string> ClubName { get; set; }
        [Display(Name = "سنه")]
        public List<string> YearFrom { get; set; }
        [Display(Name = "سنه")]
        public List<string> YearTo { get; set; }
        [Display(Name = "الطول")]
        public List<string> TransfarFrome { get; set; }
        [Display(Name = "الطول")]
        public List<string> TransfarToe { get; set; }
        [Display(Name = "الطول")]
        public List<int> Yeare { get; set; }

        [Display(Name = "النادى")]
        public List<string> ClubNameE { get; set; }
        [Display(Name = "سنه")]
        public List<string> YearFromE { get; set; }
        [Display(Name = "سنه")]
        public List<string> YearToE { get; set; }

        [Display(Name = "تصنيفه")]
        public string PlayerType { get; set; }
        [Display(Name = "اسم انجليزى")]
        public string NameE { get; set; }
        [Display(Name = "اسم عربى")]
        public string NameA { get; set; }
    }
}