﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EBBFED.Infra
{
    public class MatchPointModel
    {
        public long ID { get; set; }
        [Display(Name = "الدرجه")]
        public Nullable<long> DivisionId { get; set; }
        [Display(Name = "المرحلة العمرية")]
        public Nullable<long> AgeId { get; set; }
        [Display(Name = "نقاط الفائز")]
        public Nullable<long> WinnerPoints { get; set; }
        [Display(Name = "نقاط الخاسر")]
        public Nullable<long> LoserPoints { get; set; }
        [Display(Name = "نقاط التعادل")]
        public Nullable<long> DrawPoints { get; set; }
    }
}