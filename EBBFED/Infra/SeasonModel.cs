﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EBBFED.Infra
{
    public class SeasonModel
    {
        public long ID { get; set; }
        [Display(Name = "الموسم")]
        public string Name { get; set; }
        [Display(Name = "النوع")]
        public string Gender { get; set; }
        [Display(Name = "المرحلة العمرية")]
        public Nullable<long> AgeId { get; set; }
    }
}