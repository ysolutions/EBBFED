﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EBBFED.Infra;
using EBBFED.Models;

namespace EBBFED.API
{
    [RoutePrefix("api/HomePages")]
    public class HomePagesController : ApiController
    {
        private KoraEntities db = new KoraEntities();
        [HttpGet, Route("GetHeaderPhoto")]
        public BaseResponse GetHeaderPhoto()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                string HeaderImage = db.HomePages
                            .OrderByDescending(p => p.Id)
                            .Select(r => r.ImageUrl)
                            .First().ToString();

                response.Response = new
                {
                    ImageUrl = HeaderImage
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}