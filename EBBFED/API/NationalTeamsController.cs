﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EBBFED.Models;
using EBBFED.Infra;
namespace EBBFED.API
{
    [RoutePrefix("api/NationalMatches")]

    public class NationalTeamsController : ApiController
    {
        private KoraEntities db = new KoraEntities();

        [HttpGet, Route("AllNationalTeams")]
        public BaseResponse GetAllNationalTeams()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {

                    AllNationalTeams = db.NationalTeams.Select(t => new
                    {
                       t.ID,
                       t.NameA,
                       t.NameE,
                       t.ImageUrl,
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("MatcheScore")]
        public BaseResponse GetMatchesScore()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {

                    MatcheScore = db.NationalMatches.Where(m => m.ScoreA != null && m.ScoreB != null).Select(m => new
                    {
                        m.ID,
                        ClubA = m.NationalTeam.NameA,
                        ClubB = m.NationalTeam1.NameA,
                        m.Date,
                        m.ScoreA,
                        m.ScoreB,
                        m.LiveLink,
                        m.Place,
                        m.Time,
                        LogoA = m.NationalTeam.ImageUrl,
                        LogoB = m.NationalTeam1.ImageUrl
                    }).OrderBy(m => m.Date).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("TableMatches")]
        public BaseResponse GetAllTableMatches()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    TableMatches = db.NationalMatches.Where(m => m.ScoreA == null && m.ScoreB == null).Select(m => new
                    {
                        m.ID,
                        ClubA = m.NationalTeam.NameA,
                        ClubB = m.NationalTeam1.NameA,
                        m.Date,
                        m.ScoreA,
                        m.ScoreB,
                        m.LiveLink,
                        m.Place,
                        m.Time,
                        LogoA = m.NationalTeam.ImageUrl,
                        LogoB = m.NationalTeam1.ImageUrl
                    }).OrderBy(m => m.Date).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("NationalNews")]
        public BaseResponse NationalNews()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var NN = db.NationalNews.GroupBy(m => m.NewsID).Select(m => new { m.Key }).ToList();
                response.Response = new
                {
                    NationalNews = NN.Select(n => new
                    {
                        NationalTeams = db.NationalNews.Where(N => N.NewsID == n.Key).Select(N => N.NationalTeam.NameA).ToList(),
                        News =db.News.Where(g=>g.ID==n.Key).Select(u=>new {
                            u.ID,
                            u.TitleA,
                            u.TitleE,
                            u.LongDescriptionA,
                            u.LongDescriptionE,
                            u.ImageUrl,
                            CreationDate = (DateTime)u.CreationDate,
                            ModifiedDate = (DateTime)u.ModifiedDate,
                            Views = u.Views ?? 0,
                            u.ShareLink,
                            u.Important,
                            Clubs = db.NewsClubs.Where(c => c.NewsID == u.ID).Select(c => c.Club.NameE).ToList()
                        }).ToList()
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("MonthlyMatches")]
        public BaseResponse GetNationalMatche()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    MonthlyMatche = (from m in db.NationalMatches
                                  where m.Date.Value.Month== DateTime.Now.Month
                                  select new
                                  {
                                      m.ID,
                                      ClubA = m.NationalTeam.NameA,
                                      ClubB = m.NationalTeam1.NameA,
                                      m.Date,
                                      m.ScoreA,
                                      m.ScoreB,
                                      m.LiveLink,
                                      m.Place,
                                      m.Time,
                                      LogoA = m.NationalTeam.ImageUrl,
                                      LogoB = m.NationalTeam1.ImageUrl,
                                      CompetitionName =m.NationalCompetition.NameA
                                  }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

    }
}
