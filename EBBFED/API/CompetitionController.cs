﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EBBFED.Models;
using EBBFED.Infra;
namespace EBBFED.API
{
    [RoutePrefix("api/Competitions")]
    public class CompetitionController : ApiController
    {
        private KoraEntities db = new KoraEntities();

        [HttpGet, Route("AllCompetitions")]
        public BaseResponse GetAllCompetitions()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                List<AllCompetitionModel> competitionModels = new List<AllCompetitionModel>();
                var allcomptition = db.Competitions.ToList();
                foreach (var item in allcomptition)
                {
                    var c = item;
                    AllCompetitionModel cm = new AllCompetitionModel
                    {
                        ID = c.ID,
                        NameA = c.NameA,
                        NameE = c.NameE,
                        Location = c.Location,
                        RegisterLink = c.RegisterLink
                    };
                    DateTime date = (DateTime)c.Date;
                    var shortDate = date.Date;
                    cm.Date = shortDate.ToString("yyyy-MM-dd");
                    competitionModels.Add(cm);
                }

                response.Response = new
                {
                    AllCompetitions = competitionModels
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("CompetitionScore")]
        public BaseResponse GetMatchesScore(long id)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var matches = db.CompetitionMatches.Where(cm => cm.ScoreA != 0 && cm.ScoreB != 0 && cm.CompetitionID == id).ToList();
                List<CompetitionScore> CompScore = new List<CompetitionScore>();
                for (int i = 0; i < matches.Count; i++)
                {
                    var cm = matches[i];
                    CompetitionScore CM = new CompetitionScore
                    {
                        ID = cm.ID,
                        TeamA = cm.CompetitionTeam.NameA,
                        TeamB = cm.CompetitionTeam1.NameA,
                        ScoreA = cm.ScoreA ?? 0,
                        ScoreB = cm.ScoreB ?? 0,
                        LogoA = cm.CompetitionTeam.Logo,
                        LogoB = cm.CompetitionTeam1.Logo
                    };
                    DateTime date = (DateTime)cm.Date;
                    var shortDate = date.Date;
                    CM.Date = shortDate.ToString("yyyy-MM-dd");
                    CompScore.Add(CM);
                }
                response.Response = new
                {
                    CompetitionScore = CompScore
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("CompetitionsTable")]
        public BaseResponse GetAllTableMatches(long id)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    CompetitionsTable = db.CompetitionTables.Where(ct => ct.CompetitionID == id).Select(u => new
                    {
                        u.ID,
                        TeamName = u.CompetitionTeam.NameA,
                        u.CompetitionTeam.Logo,
                        u.NumberOfMatches,
                        u.Winner,
                        u.Lose,
                        u.Points,
                        INGoals = u.INGoals ?? 0,
                        OutGoals = u.OutGoals ?? 0,
                    }).OrderByDescending(u => u.Points).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("GetComptetionNextMatches")]
        public BaseResponse GetComptetionNextMatches(long id)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var ComptitionNextMatche = db.CompetitionMatches.Where(c => c.CompetitionID == id && c.ScoreA == 0 && c.ScoreB == 0).ToList();
                List<CompetitionScore> AllcompetitionTables = new List<CompetitionScore>();
                foreach (var item in ComptitionNextMatche)
                {
                    var cm = item;
                    CompetitionScore c = new CompetitionScore
                    {
                        ID = cm.ID,
                        TeamA = cm.CompetitionTeam.NameA,
                        TeamB = cm.CompetitionTeam1.NameA,
                        ScoreA = cm.ScoreA ?? 0,
                        ScoreB = cm.ScoreB ?? 0,
                        LogoA = cm.CompetitionTeam.Logo,
                        LogoB = cm.CompetitionTeam1.Logo
                    };
                    DateTime date = (DateTime)cm.Date;
                    var shortDate = date.Date;
                    c.Date = shortDate.ToString("yyyy-MM-dd");
                    AllcompetitionTables.Add(c);
                }
                response.Response = new
                {
                    CompetitionsTable = AllcompetitionTables
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("GetAllComptetionMatches")]
        public BaseResponse GetAllComptetionMatches(long? id = -1, long? ageId = -1, string gender = "")
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var ComptitionNextMatche = db.CompetitionMatches.ToList();
                ComptitionNextMatche = id != -1 ? ComptitionNextMatche.Where(c => c.CompetitionID == id).ToList() : ComptitionNextMatche;
                ComptitionNextMatche = ageId != -1 ? ComptitionNextMatche.Where(c => c.AgesID == ageId).ToList() : ComptitionNextMatche;
                ComptitionNextMatche = !String.IsNullOrEmpty(gender) ? ComptitionNextMatche.Where(c => c.Competition.Gender != null && c.Competition.Gender.ToLower() == gender.ToLower()).ToList() : ComptitionNextMatche;
                List<CompetitionScore> AllcompetitionTables = new List<CompetitionScore>();
                foreach (var item in ComptitionNextMatche)
                {
                    var cm = item;
                    CompetitionScore c = new CompetitionScore
                    {
                        ID = cm.ID,
                        CompetitionNameA = cm.Competition.NameA,
                        CompetitionNameE = cm.Competition.NameE,
                        TeamA = cm.CompetitionTeam.NameA,
                        TeamB = cm.CompetitionTeam1.NameA,
                        ScoreA = cm.ScoreA ?? 0,
                        ScoreB = cm.ScoreB ?? 0,
                        LogoA = cm.CompetitionTeam.Logo,
                        LogoB = cm.CompetitionTeam1.Logo,
                        Time = cm.Time,
                        Location = cm.Competition.Location
                    };
                    DateTime date = (DateTime)cm.Date;
                    var shortDate = date.Date;
                    c.Date = shortDate.ToString("yyyy-MM-dd");
                    AllcompetitionTables.Add(c);
                }
                response.Response = new
                {
                    CompetitionsTable = AllcompetitionTables.OrderByDescending(s => s.Date).ThenBy(s => s.Time)
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("GetComptetion")]
        public BaseResponse GetComptetion(string gender = "", long? ageid = -1)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                List<AllCompetitionModel> competitionModels = new List<AllCompetitionModel>();
                var competitions = db.Competitions.ToList();

                if (ageid != -1) //filter by Age
                {
                    competitions = (from c in competitions
                                    join ca in db.CompetitionAges on c.ID equals ca.CompetitionID
                                    join a in db.Ages on ca.AgesID equals a.ID
                                    where ca.AgesID == ageid
                                    select c).ToList();
                }

                if (!String.IsNullOrEmpty(gender)) //Filter by Gender
                {
                    gender = gender.ToLower();
                    competitions = competitions.Where(c => c.Gender != null && c.Gender.ToLower() == gender).ToList();
                }

                foreach (var item in competitions)
                {
                    var c = item;
                    AllCompetitionModel cm = new AllCompetitionModel
                    {
                        ID = c.ID,
                        NameA = c.NameA,
                        NameE = c.NameE,
                        Location = c.Location,
                        RegisterLink = c.RegisterLink
                    };
                    DateTime date = (DateTime)c.Date;
                    var shortDate = date.Date;
                    cm.Date = shortDate.ToString("yyyy-MM-dd");
                    competitionModels.Add(cm);
                }

                response.Response = new
                {
                    AllCompetitions = competitionModels
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        //[HttpGet, Route("GetComptetionHistory")]
        //public BaseResponse GetComptetionHistory(long? seasonId = -1, long? ageId = -1, string gender = "")
        //{
        //    BaseResponse response = new BaseResponse();
        //    try
        //    {
        //        var comp = db.Competitions.ToList();
        //        var compAges = db.CompetitionAges.Where(a => a.AgesID == ageId).ToList();

        //        comp = seasonId != -1 ? comp.Where(s => s.SeasonId == seasonId).ToList() : comp;
        //        comp = ageId != -1 ? (from c in comp join ca in compAges on c.ID equals ca.CompetitionID select c).ToList() : comp;
        //        comp = !String.IsNullOrEmpty(gender) ? comp.Where(s => s.Gender != null && s.Gender.ToLower() == gender.ToLower()).ToList() : comp;

        //        List<CompetitionTable> competitionTables = new List<CompetitionTable>();
        //        var compTables = db.CompetitionTables.ToList();

        //        for (int i = 0; i < comp.Count; i++)
        //        {
        //            for (int y = 0; y < compTables.Count; y++)
        //            {
        //                if (comp[i].ID==compTables[y].CompetitionID)
        //                {
        //                    competitionTables.Add(compTables[y]);
        //                }
        //            }
        //        }

        //        response.Response = new
        //        {
        //            CompetitionsTable = competitionTables.Select(u => new
        //            {
        //                u.ID,
        //                TeamName = u.CompetitionTeam.NameA,
        //                ComptetionNameAr = u.Competition.NameA,
        //                ComptetionNameEn = u.Competition.NameE,
        //                u.CompetitionTeam.Logo,
        //                u.NumberOfMatches,
        //                u.Winner,
        //                u.Lose,
        //                u.Points,
        //                INGoals = u.INGoals ?? 0,
        //                OutGoals = u.OutGoals ?? 0,
        //            }).OrderByDescending(u => u.Points).ToList()
        //        };
        //        response.isSuccess = true;
        //        response.errorMessage = "";
        //        return response;
        //    }
        //    catch (Exception ex)
        //    {
        //        response.isSuccess = false;
        //        response.errorMessage = "";
        //        response.Response = new
        //        {
        //            ExceptionMessage = ex.Message,
        //            ExceptionStackTrace = ex.StackTrace
        //        };
        //    }
        //    return response;
        //}
    }
}
