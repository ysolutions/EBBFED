﻿using EBBFED.Infra;
using EBBFED.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;


namespace EBBFED.API
{
    [RoutePrefix("api/UserNotifications")]
    public class UserNotificationsController : ApiController
    {
        private KoraEntities db = new KoraEntities();
        [HttpPost, Route("Subscribe")]
        public BaseResponse Subscribe([FromBody]UserSubscribe user)
        {
            BaseResponse response = new BaseResponse();
            string Email = user.Email.ToLower();
            if (String.IsNullOrEmpty(Email))
            {
                response.isSuccess = false;
                response.errorMessage = "لم يتم إدخال بريد إلكترونى";
                return response;
            }
            bool isEmail = Regex.IsMatch(Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
            if (isEmail)
            {
                bool isExist = db.UserNotifications.Count(e => e.Email == Email) > 0;
                if (isExist)
                {
                    var currentUser = db.UserNotifications.Where(x => x.Email.ToLower() == Email).FirstOrDefault();
                    currentUser.IsSubscribed = true;
                    db.SaveChanges();
                    response.isSuccess = true;
                    response.errorMessage = "تم إعاده الاشتراك بنجاح";
                    return response;
                }
                UserNotification notification = new UserNotification();
                notification.Email = Email;
                notification.IsSubscribed = true;
                db.UserNotifications.Add(notification);
                db.SaveChanges();
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            response.isSuccess = false;
            response.errorMessage = "لقد أدخلت بريد إلكترونى خاطئ";
            return response;
        }
        [HttpPost, Route("UnSubscribe")]
        public BaseResponse UnSubscribe([FromBody]UserSubscribe user)
        {
            BaseResponse response = new BaseResponse();
            string Email = user.Email;
            if (String.IsNullOrEmpty(Email))
            {
                response.isSuccess = false;
                response.errorMessage = "لم يتم إدخال بريد إلكترونى";
                return response;
            }
            bool isEmail = Regex.IsMatch(Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
            if (isEmail)
            {
                var userNotifications = db.UserNotifications.FirstOrDefault(e => e.Email.ToLower() == Email);
                if (userNotifications == null)
                {
                    response.isSuccess = false;
                    response.errorMessage = "لم يتم العثور على هذا البريد الإلكترونى";
                    return response;
                }
                userNotifications.IsSubscribed = false;
                db.SaveChanges();
                response.isSuccess = true;
                response.errorMessage = "تم إلغاء الاشتراك بنجاح";
                return response;
            }
            response.isSuccess = false;
            response.errorMessage = "لقد أدخلت بريد إلكترونى خاطئ";
            return response;
        }
    }
}
