﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EBBFED.Models;
using EBBFED.Infra;
namespace EBBFED.API
{
    [RoutePrefix("api/News")]
    public class NewsController : ApiController
    {
        private KoraEntities db = new KoraEntities();

        [HttpGet, Route("AllNews")]
        public BaseResponse GetAllNews()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    News = db.News.Select(u => new
                    {
                        u.ID,
                        u.TitleA,
                        u.TitleE,
                        u.ShortDescriptionA,
                        u.ShortDescriptionE,
                        u.ImageUrl,
                        CreationDate = (DateTime)u.CreationDate,
                        ModifiedDate = (DateTime)u.ModifiedDate,
                        Views = u.Views ?? 0,
                        u.ShareLink,
                        u.Important,
                        Clubs = db.NewsClubs.Where(c => c.NewsID == u.ID).Select(c => c.Club.NameE).ToList(),
                        PlayerID = db.PlayerNews.Where(p => p.NewsID == u.ID).Select(p => p.Player.ID).FirstOrDefault()
                    }).OrderByDescending(r => r.ID).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("GetNews")]
        public BaseResponse GetNews(long id)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var news = db.News.Find(id);
                if (news != null)
                {
                    news.Views = news.Views + 1;
                    db.SaveChanges();
                }
                response.Response = new
                {
                    News = db.News.Where(u => u.ID == id).Select(u => new
                    {
                        u.ID,
                        u.TitleA,
                        u.TitleE,
                        u.LongDescriptionA,
                        u.LongDescriptionE,
                        u.ImageUrl,
                        CreationDate = (DateTime)u.CreationDate,
                        ModifiedDate = (DateTime)u.ModifiedDate,
                        Views = u.Views ?? 0,
                        u.ShareLink,
                        u.Important,
                        Clubs = db.NewsClubs.Where(c => c.NewsID == u.ID).Select(c => c.Club.NameE).ToList(),
                        PlayerID = db.PlayerNews.Where(p => p.NewsID == u.ID).Select(p => p.Player.ID).FirstOrDefault()

                    }).OrderByDescending(r => r.ID).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("LastNews")]
        public BaseResponse GetLastNews()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    LastNews = db.News.Select(u => new
                    {
                        u.ID,
                        u.TitleA,
                        u.TitleE,
                        u.ShortDescriptionA,
                        u.ShortDescriptionE,
                        u.LongDescriptionA,
                        u.LongDescriptionE,
                        u.ImageUrl,
                        CreationDate = (DateTime)u.CreationDate,
                        ModifiedDate = (DateTime)u.ModifiedDate,
                        Views = u.Views ?? 0,
                        u.ShareLink,
                        u.Important,
                        Clubs = db.NewsClubs.Where(c => c.NewsID == u.ID).Select(c => c.Club.NameE).ToList(),
                        PlayerID = db.PlayerNews.Where(p => p.NewsID == u.ID).Select(p => p.Player.ID).FirstOrDefault()

                    }).OrderByDescending(r => r.ID).Take(3).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("ImportantNews")]
        public BaseResponse GetImportantNews()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    LastNews = db.News.Where(n => n.Important == true).Select(u => new
                    {
                        u.ID,
                        u.TitleA,
                        u.TitleE,
                        u.ShortDescriptionA,
                        u.ShortDescriptionE,
                        u.LongDescriptionA,
                        u.LongDescriptionE,
                        u.ImageUrl,
                        CreationDate = (DateTime)u.CreationDate,
                        ModifiedDate = (DateTime)u.ModifiedDate,
                        Views = u.Views ?? 0,
                        u.ShareLink,
                        Clubs = db.NewsClubs.Where(c => c.NewsID == u.ID).Select(c => c.Club.NameE).ToList(),
                        PlayerID = db.PlayerNews.Where(p => p.NewsID == u.ID).Select(p => p.Player.ID).FirstOrDefault()

                    }).OrderByDescending(r => r.ID).Take(3).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

    }
}