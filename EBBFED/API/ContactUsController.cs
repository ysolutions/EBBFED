﻿using EBBFED.Infra;
using EBBFED.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace EBBFED.API
{
    [RoutePrefix("api/ContactUs")]
    public class ContactUsController : ApiController
    {
        private KoraEntities db = new KoraEntities();

        [HttpGet, Route("ContactUs")]
        public BaseResponse GetContactUs()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    ContactUs = db.ContactUs.Select(u => new
                    {
                        u.PhoneNumber,
                        u.Address,
                        u.Email,
                        u.LocationLong,
                        u.LocationLat,
                        u.WorkingHours,
                        u.FacebookURL,
                        u.TwitterURL,
                        u.InstagramURL,
                        u.YoutubeURL
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpPost, Route("SubmitContactUs")]
        public BaseResponse SubmitContactUs(ContactUsModel model)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                if (ModelState.IsValid)
                {
                    Thread MailThread = new Thread(delegate ()
                    {
                        string body = string.Format("New Message \n Name: {0}\n Email : {1}\n. Message: {2}", model.Name, model.Email.ToString(), model.Message);
                        string mailSubject = string.Format(" New {0}", "Message");
                        string mailBody = body;
                        MailSender mailSender = new MailSender();
                        mailSender.SendMail(ConfigurationManager.AppSettings["SendMail.From"].ToString(), mailSubject, mailBody, false);
                    });
                    MailThread.SetApartmentState(ApartmentState.STA); // needs to be STA or throws exception
                    MailThread.Start();
                    response.isSuccess = true;
                    response.errorMessage = "";
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }
    }
}
