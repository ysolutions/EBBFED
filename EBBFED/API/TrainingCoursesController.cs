﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EBBFED.Infra;
using EBBFED.Models;    
namespace EBBFED.API
{
    [RoutePrefix("api/Courses")]
    public class TrainingCoursesController : ApiController
    {
        private KoraEntities db = new KoraEntities();

        [HttpGet, Route("AllCourses")]
        public BaseResponse GetAllCourses()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Courses = db.TrainingCourses.Select(u => new
                    {
                        u.ID,
                        u.TitleA,
                        u.TitleE,
                        u.ShortDescriptionA,
                        u.ShortDescriptionE,
                        u.LongDescriptionA,
                        u.LongDescriptionE,
                        u.Location,
                        u.Date,
                        u.CourseType,
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("Course")]
        public BaseResponse GetCourse(long id)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Course = db.TrainingCourses.Where(tc => tc.ID == id).Select(u => new
                    {
                        u.ID,
                        u.TitleA,
                        u.TitleE,
                        u.ShortDescriptionA,
                        u.ShortDescriptionE,
                        u.LongDescriptionA,
                        u.LongDescriptionE,
                        u.Location,
                        u.Date,
                        u.CourseType,
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }
    }
}
