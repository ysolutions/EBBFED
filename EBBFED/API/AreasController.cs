﻿using EBBFED.Infra;
using EBBFED.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EBBFED.API
{
    [RoutePrefix("api/Areas")]
    public class AreasController : ApiController
    {
        private KoraEntities db = new KoraEntities();

        [HttpGet, Route("AllAreas")]
        public BaseResponse GetAllAreas()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Areas = db.Areas.Select(u => new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE,
                        //u.Clubs,
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }
    }
}
