﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EBBFED.Models;
using EBBFED.Infra;
namespace EBBFED.API
{
    [RoutePrefix("api/Judges")]
    public class JudgesController : ApiController
    {
        private KoraEntities db = new KoraEntities();

        [HttpGet, Route("AllJudges")]
        public BaseResponse GetAllJudges()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Judges = db.Judges.Select(u => new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE,
                        u.DescriptionA,
                        u.DescriptionE,
                        u.ImageUrl,
                        u.Category,
                        Area = db.Areas.Where(a => a.ID == u.AreaID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault()
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("GetJudges")]
        public BaseResponse GetJudges(long id)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Judge = db.Judges.Where(j=>j.ID==id).Select(u => new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE,
                        u.DescriptionA,
                        u.DescriptionE,
                        u.ImageUrl,
                        u.Category,
                        Area = db.Areas.Where(a => a.ID == u.AreaID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault()

                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }
    }
}
