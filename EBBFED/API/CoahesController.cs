﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EBBFED.Models;
using EBBFED.Infra;
namespace EBBFED.API
{
    [RoutePrefix("api/Coahes")]
    public class CoahesController : ApiController
    {
        private KoraEntities db = new KoraEntities();

        [HttpGet, Route("AllCoahes")]
        public BaseResponse GetAllCoahes()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Coahes = db.Coahes.Select(u => new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE,
                        u.TitleA,
                        u.TitleE,
                        u.DescriptionA,
                        u.DescriptionE,
                        u.ImageUrl,
                        u.Category,
                        u.Age,
                        ClubName = u.Club.NameA
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("GetCoahes")]
        public BaseResponse GetGetCoahes(long id)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Coahe = db.Coahes.Where(c=>c.ID==id).Select(u => new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE,
                        u.TitleA,
                        u.TitleE,
                        u.DescriptionA,
                        u.DescriptionE,
                        u.ImageUrl,
                        u.Category,
                        u.Age,
                        ClubName = u.Club.NameA
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

    }
}
