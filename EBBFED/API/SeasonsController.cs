﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EBBFED.Infra;
using EBBFED.Models;

namespace EBBFED.API
{
    [RoutePrefix("api/Seasons")]
    public class SeasonsController : ApiController
    {
        private KoraEntities db = new KoraEntities();

        [HttpGet, Route("GetSeasons")]
        public BaseResponse GetSeasons()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var seasons = db.Seasons.ToList();
                response.Response = new
                {
                    Seasons = seasons.Select(x => new { ID=x.ID, Season=x.Name }).OrderByDescending(x=>x.Season).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }


        [HttpGet, Route("GetSeasonHistory")]
        public BaseResponse GetSeasonHistory(long? seasonId = -1, long? ageId = -1, string gender = "")
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var seasons = db.Seasons.ToList();
                
                seasons = seasonId != -1 ? seasons.Where(a => a.ID == seasonId).ToList() : seasons;
                seasons = ageId != -1 ? seasons.Where(a => a.AgeId == ageId).ToList() : seasons;
                seasons = !String.IsNullOrEmpty(gender) ? seasons.Where(s => s.Gender != null && s.Gender.ToLower() == gender.ToLower()).ToList() : seasons;

                List<SeasonTeam> teams = new List<SeasonTeam>();
                var seasonteams = db.SeasonTeams.ToList();

                for (int i = 0; i < seasons.Count; i++)
                {
                    for (int y = 0; y < seasonteams.Count; y++)
                    {
                        if (seasons[i].ID == seasonteams[y].SeasonId)
                        {
                            teams.Add(seasonteams[y]);
                        }
                    }
                }
                if (seasonId != -1) //list season teams
                {
                    response.Response = new
                    {
                        SeasonTable = seasons.Select(s => new
                        {
                            Season = s.Name,
                            Teams = teams.Select(u => new
                            {
                                u.ID,
                                u.Rank,
                                Season = u.Season.Name,
                                TeamNameAr = u.Team.NameA,
                                TeamNameEn = u.Team.NameE,
                                AgeAr = u.Season.Age.NameA,
                                AgeEn = u.Season.Age.NameE,
                                u.Season.Gender
                            }).OrderBy(u => u.Rank).ToList()
                        })
                    };
                    }
                else //list first 5 teams of each season
                {
                    response.Response = new
                    {
                        SeasonTable = seasons.Select(s => new
                        {
                            Season = s.Name,
                            Teams = s.SeasonTeams.Select(u => new
                            {
                                u.ID,
                                u.Rank,
                                Season = u.Season.Name,
                                TeamNameAr = u.Team.NameA,
                                TeamNameEn = u.Team.NameE,
                                AgeAr = u.Season.Age.NameA,
                                AgeEn = u.Season.Age.NameE,
                                u.Season.Gender
                            }).OrderBy(u => u.Rank).Take(5).ToList()
                        }).OrderByDescending(s => s.Season)
                    };
                }
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}