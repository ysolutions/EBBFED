﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EBBFED.Models;
using EBBFED.Infra;
namespace EBBFED.API
{
    [RoutePrefix("api/Settings")]
    public class SettingController : ApiController
    {
        private KoraEntities db = new KoraEntities();

        [HttpGet, Route("AllArticles")]
        public BaseResponse GetAllArticles()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Articles = db.Articles.Select(u => new
                    {
                        u.ID,
                        u.TitleA,
                        u.TitleE,
                        u.ShortDescriptionA,
                        u.ShortDescriptionE,
                        u.ImageUrl,
                        u.Date,
                        u.LongDescriptionA,
                        u.LongDescriptionE
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("GetArticle")]
        public BaseResponse GetArticle(long id)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Article = db.Articles.Where(a => a.ID == id).Select(u => new
                    {
                        u.ID,
                        u.TitleA,
                        u.TitleE,
                        u.ShortDescriptionA,
                        u.ShortDescriptionE,
                        u.ImageUrl,
                        u.Date,
                        u.LongDescriptionA,
                        u.LongDescriptionE
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("AllAges")]
        public BaseResponse GetAllAges()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Ages = db.Ages.Select(u => new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE,

                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("AllDivisions")]
        public BaseResponse GetAllDivisions()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Divisions = db.Divisions.Select(u => new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("GetSettings")]
        public BaseResponse GetSettings()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Settings = db.Settings.Select(u => new
                    {
                        u.CoachPDFFile
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }
    }
}
