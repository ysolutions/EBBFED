﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EBBFED.Infra;
using EBBFED.Models;

namespace EBBFED.API
{
    [RoutePrefix("api/Clubs")]
    public class ClubsController : ApiController
    {
        private KoraEntities db = new KoraEntities();

        [HttpGet, Route("AllGallery")]
        public BaseResponse GetAllGallery()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Gallery = db.Galleries.Select(u => new
                    {
                        u.ID,
                        u.ImageUrl,
                        u.VideoUrl,
                        u.DescriptionA,
                        u.DescriptionE,
                        u.Date,
                        Clubs = u.GalleryClubs.Select(gc => new { gc.Club.ID, gc.Club.NameA, gc.Club.NameE }),
                        Players = u.GalleryPlayers.Select(gp => new { gp.Player.ID, gp.Player.NameA, gp.Player.NameE }),
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("FilteredAllGallery")]
        public BaseResponse GetFilteredAllGallery(long ClubID = 0, long PlayerID = 0)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var GalleryList = db.Galleries.Select(u => new
                {
                    u.ID,
                    u.ImageUrl,
                    u.VideoUrl,
                    u.DescriptionA,
                    u.DescriptionE,
                    u.Date,
                    Clubs = u.GalleryClubs.Select(gc => new { gc.Club.ID, gc.Club.NameA, gc.Club.NameE }),
                    Players = u.GalleryPlayers.Select(gp => new { gp.Player.ID, gp.Player.NameA, gp.Player.NameE }),
                }).ToList();

                if ((ClubID != 0))
                {
                    GalleryList = GalleryList.Where(m => m.Clubs.Where(c => c.ID == ClubID).Count() > 0).ToList();
                }

                if ((PlayerID != 0))
                {
                    GalleryList = GalleryList.Where(m => m.Players.Where(p => p.ID == PlayerID).Count() > 0).ToList();
                }

                response.Response = new
                {
                    Gallery = GalleryList,
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("AllClubs")]
        public BaseResponse GetAllClubs()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Players = db.Clubs.Select(u => new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE,
                        u.DescriptionA,
                        u.DescriptionE,
                        u.EstablishedYear,
                        u.TitlesWon,
                        Division = u.Division.NameA,
                        u.Logo,
                        Area = db.Areas.Where(a => a.ID == u.AreaID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault()
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("GetClub")]
        public BaseResponse GetPlayer(long id)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Players = db.Clubs.Where(w => w.ID == id).Select(u => new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE,
                        u.DescriptionA,
                        u.DescriptionE,
                        u.EstablishedYear,
                        u.TitlesWon,
                        Division = u.Division.NameA,
                        u.Logo,
                        Area = db.Areas.Where(a => a.ID == u.AreaID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault()
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

    }
}
