﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EBBFED.Infra;
using EBBFED.Models;

namespace EBBFED.API
{
    [RoutePrefix("api/Statisticians")]
    public class StatisticiansController : ApiController
    {
        private KoraEntities db = new KoraEntities();

        [HttpGet, Route("AllStatisticians")]
        public BaseResponse GetAllStatisticians()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Statisticians = db.Statisticians.Select(u => new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE,
                        Clubs = db.Clubs.Where(s => s.ID == u.ClubId).Select(x => new
                        {
                            x.ID,
                            x.NameA,
                            x.NameE,
                            x.DescriptionA,
                            x.DescriptionE,
                            x.EstablishedYear,
                            x.TitlesWon,
                            Division = x.Division.NameA,
                            x.Logo,
                            Area = db.Areas.Where(a => a.ID == x.AreaID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault()
                        }),
                        Branches = db.Branches.Where(s => s.ID == u.BranchId).Select(x => new
                        {
                            x.ID,
                            x.NameA,
                            x.NameE,
                            x.Phone1,
                            x.Phone2,
                            x.Fax,
                            x.Email,
                            x.AddressA,
                            x.AddressE,
                        })
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }
        [HttpGet, Route("GetStatistical")]
        public BaseResponse Getstatistical(long id)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Statistical = db.Statisticians.Where(i => i.ID == id).Select(u => new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE,
                        Clubs = db.Clubs.Where(s => s.ID == u.ClubId).Select(x => new
                        {
                            x.ID,
                            x.NameA,
                            x.NameE,
                            x.DescriptionA,
                            x.DescriptionE,
                            x.EstablishedYear,
                            x.TitlesWon,
                            Division = x.Division.NameA,
                            x.Logo,
                            Area = db.Areas.Where(a => a.ID == x.AreaID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault()
                        }),
                        Branches = db.Branches.Where(s => s.ID == u.BranchId).Select(x => new
                        {
                            x.ID,
                            x.NameA,
                            x.NameE,
                            x.Phone1,
                            x.Phone2,
                            x.Fax,
                            x.Email,
                            x.AddressA,
                            x.AddressE,
                        })
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
