﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EBBFED.Models;
using EBBFED.Infra;
namespace EBBFED.API
{
    [RoutePrefix("api/Rules")]
    public class RulesController : ApiController
    {
        private KoraEntities db = new KoraEntities();

        [HttpGet, Route("AllRules")]

        public BaseResponse AllRules()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Rules = db.RulesAndLaws.Select(R => new
                    {
                        R.NameA,
                        R.NameE,
                        R.ShortDescriptionA,
                        R.ShortDescriptionE,
                        R.PdfName,
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }
    }
}
