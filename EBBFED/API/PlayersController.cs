﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EBBFED.Infra;
using EBBFED.Models;
namespace EBBFED.API
{
    [RoutePrefix("api/Players")]
    public class PlayersController : ApiController
    {
        private KoraEntities db = new KoraEntities();
        [HttpGet, Route("FamousPlayers")]
        public BaseResponse GetAllFamousPlayers()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    FamousPlayers = db.Players.Select(u => new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE,
                        u.ImageUrl,
                        u.Age,
                        u.ClubID,
                        u.PlayerNumber,
                        ClubName = u.Club.NameA,
                        Position = u.Position.NameA,
                        u.Height,
                        u.Weight,
                        u.PlayerType
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("AllPlayers")]
        public BaseResponse GetAllPlayers()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Players = db.Players.Select(u => new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE,
                        u.Age,
                        Position = u.Position.NameA,
                        u.Height,
                        u.Weight,
                        u.InternationalMatches,
                        u.PlayerType,
                        u.ClubID,
                        ClubName = u.Club.NameA,
                        u.ImageUrl,
                        u.PlayerNumber,
                        Transfar = u.Transfars.Select(t => new
                        {
                            t.TransfarFrom,
                            t.TransfarTo,
                            t.Year
                        }).ToList(),
                        PreviousClubs = u.PreviousClubs.Select(p => new
                        {
                            p.ClubName,
                            p.YearFrom,
                            p.YearTo
                        }).ToList()
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("GetPlayer")]
        public BaseResponse GetPlayer(long id)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var player = db.Players.Find(id);
                response.Response = new
                {
                    Players = db.Players.Where(w => w.ID == id).Select(u => new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE,
                        u.Age,
                        Position = u.Position.NameA,
                        u.Height,
                        u.Weight,
                        u.InternationalMatches,
                        u.PlayerType,
                        u.ClubID,
                        ClubName = u.Club.NameA,
                        u.ImageUrl,
                        u.PlayerNumber,
                        Transfar = u.Transfars.Select(t => new
                        {
                            t.TransfarFrom,
                            t.TransfarTo,
                            t.Year
                        }).ToList(),
                        PreviousClubs = u.PreviousClubs.Select(p => new
                        {
                            p.ClubName,
                            p.YearFrom,
                            p.YearTo
                        }).ToList()
                    }).ToList(),
                    RelatedPlayers = db.Players.Where(w => w.PlayerType == player.PlayerType).Select(u => new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE,
                        u.Age,
                        ClubName = u.Club.NameA,
                        u.ImageUrl,
                        u.PlayerNumber
                    }).Take(4).ToList(),
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("GetPlayersBySearch")]
        public BaseResponse GetPlayersBySearch(long? clubid = -1, long? minAge = -1,long? maxAge=-1, long? minHeight = -1, long? maxHeight = -1, long? minWeight = -1, long? maxWeight = -1)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var players = db.Players.ToList();

                players = clubid != -1 ? players.Where(c => c.ClubID == clubid).ToList() : players;
                players = minAge != -1 ? players.Where(c => c.Age >= minAge).ToList() : players;
                players = maxAge != -1 ? players.Where(c => c.Age <= maxAge).ToList() : players;
                players = minHeight != -1 ? players.Where(c => c.Height >= minHeight).ToList() : players;
                players = maxHeight != -1 ? players.Where(c => c.Height <= minHeight).ToList() : players;
                players = minWeight != -1 ? players.Where(c => c.Weight >= minWeight).ToList() : players;
                players = maxWeight != -1 ? players.Where(c => c.Weight <= maxWeight).ToList() : players;

                response.Response = new
                {
                    Players = players.Select(u => new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE,
                        u.Age,
                        Position = u.Position.NameA,
                        u.Height,
                        u.Weight,
                        u.InternationalMatches,
                        u.PlayerType,
                        u.ClubID,
                        ClubName = u.Club.NameA,
                        u.ImageUrl,
                        u.PlayerNumber,
                        Transfar = u.Transfars.Select(t => new
                        {
                            t.TransfarFrom,
                            t.TransfarTo,
                            t.Year
                        }).ToList(),
                        PreviousClubs = u.PreviousClubs.Select(p => new
                        {
                            p.ClubName,
                            p.YearFrom,
                            p.YearTo
                        }).ToList()
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }
    }
}
