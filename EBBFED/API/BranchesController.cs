﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EBBFED.Infra;
using EBBFED.Models;

namespace EBBFED.API
{
    [RoutePrefix("api/Branches")]
    public class BranchesController : ApiController
    {
        private KoraEntities db = new KoraEntities();

        // GET: api/Branches
        [HttpGet, Route("AllBranches")]
        public BaseResponse GetAllBranches()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Branches = db.Branches.Select(u => new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE,
                        u.AddressA,
                        u.AddressE,
                        u.Phone1,
                        u.Phone2,
                        u.Email,
                        u.Fax,
                        Directors = u.BranchesPresidents.Select(x => new
                        {
                            x.ID,
                            x.ImageUrl,
                            x.NameA,
                            x.NameE,
                            x.TitleA,
                            x.TitleE,
                            x.BranchId,
                            x.BriefA,
                            x.BriefE
                        }).ToList()
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        // GET: api/Branches/5
        [HttpGet, Route("GetBranch")]
        public BaseResponse GetBranch(long id)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Branches = db.Branches.Where(i=>i.ID==id).Select(u => new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE,
                        u.AddressA,
                        u.AddressE,
                        u.Phone1,
                        u.Phone2,
                        u.Email,
                        u.Fax,
                        Directors = u.BranchesPresidents.Select(x => new
                        {
                            x.ID,
                            x.ImageUrl,
                            x.NameA,
                            x.NameE,
                            x.TitleA,
                            x.TitleE,
                            x.BranchId,
                            x.BriefA,
                            x.BriefE
                        }).ToList()
                    }).FirstOrDefault()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}