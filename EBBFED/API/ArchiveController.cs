﻿using EBBFED.Infra;
using EBBFED.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EBBFED.API
{
    [RoutePrefix("api/Archive")]
    public class ArchiveController : ApiController
    {
        private KoraEntities db = new KoraEntities();

        [HttpGet, Route("ArchiveCategories")]
        public BaseResponse GetAllArchiveCategories()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    ArchiveCategories = db.ArchiveCategories.Select(u => new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE,
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("ArchiveCategoryFiles")]
        public BaseResponse GetArchiveCategoryFiles(long id)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                ArchiveCategory archiveCategory = db.ArchiveCategories.Find(id);

                if (archiveCategory != null)
                {
                    response.Response = new
                    {
                        ArchiveCategory = db.ArchiveCategories.Where(ac => ac.ID == id).Select(u => new
                        {
                            u.ID,
                            u.NameA,
                            u.NameE,
                        }).ToList(),
                        Files = archiveCategory.ArchiveFiles.Select(u => new
                        {
                            u.ID,
                            u.NameA,
                            u.NameE,
                            u.Path
                        }).ToList(),
                    };
                    response.isSuccess = true;
                    response.errorMessage = "";
                }
                else
                {
                    response.Response = null;
                    response.isSuccess = false;
                    response.errorMessage = "No ArchiveCategory with this ID.";
                }
                
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }
    }
}
