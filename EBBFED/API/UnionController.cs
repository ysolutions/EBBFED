﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EBBFED.Models;
using EBBFED.Infra;
namespace EBBFED.API
{
    [RoutePrefix("api/Union")]
    public class UnionController : ApiController
    {
        private KoraEntities db = new KoraEntities();

        [HttpGet, Route("HistoryUnion")]
        public BaseResponse GetAllHistoryUnion()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    HistoryUnion = db.HistoryUnions.Select(u => new
                    {
                        u.ID,
                        u.EstablishedYearA,
                        u.EstablishedYearE,
                        u.AchievementA,
                        u.AchievementE

                    }).ToList(),
                    PersonUnion = db.UnionPersons.Select(u => new
                    {
                        u.ID,
                        u.ImageUrl,
                        u.NameA,
                        u.NameE,
                        u.BriefA,
                        u.BriefE,
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("CouncilPresident")]
        public BaseResponse GetAllCouncilPresidents()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    CouncilPresident = db.CouncilPresidents.Select(u => new
                    {
                        u.ID,
                        u.ImageUrl,
                        u.NameA,
                        u.NameE,
                        u.BriefA,
                        u.BriefE,
                        u.TitleA,
                        u.TitleE
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("Decisions")]
        public BaseResponse GetAllDecisions()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Decision = db.Decisions.Select(u => new
                    {
                        u.ID,
                        u.ShortDescriptionA,
                        u.ShortDescriptionE,
                        u.CreationDate,
                        u.TitleA,
                        u.TitleE,
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("GetDecision")]
        public BaseResponse GetDecision(long id)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    Decision = db.Decisions.Where(d => d.ID == id).Select(u => new
                    {
                        u.ID,
                        u.ShortDescriptionA,
                        u.ShortDescriptionE,
                        u.CreationDate,
                        u.TitleA,
                        u.TitleE,
                        u.LongDescriptionA,
                        u.LongDescriptionE
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("UnoinCommitte")]
        public BaseResponse GetUnoinCommitte()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    UnoinCommittes = db.UnoinCommittes.Select(u => new
                    {
                        u.ID,
                        u.ImageUrl,
                        u.NameA,
                        u.NameE
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("GetUnoinCommitteDetails")]
        public BaseResponse GetUnoinCommitteDetails(long id)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var Committemembers = db.ComitteMembers.Where(c => c.CommitteID == id).ToList();
                var unoin = db.UnoinCommittes.Where(c => c.ID == id).FirstOrDefault();
                response.Response = new
                {
                    CommitteMember = Committemembers.Select(u=>new
                    {
                        u.ID,
                        u.NameA,
                        u.NameE
                    }).ToList(),
                    unoin.ObjectivesA,
                    unoin.ObjectivesE

                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

    }
}
