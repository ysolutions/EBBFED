﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EBBFED.Models;
using EBBFED.Infra;
namespace EBBFED.API
{
    [RoutePrefix("api/Matches")]
    public class MatchController : ApiController
    {
        private KoraEntities db = new KoraEntities();

        [HttpGet, Route("AllMatches")]
        public BaseResponse GetAllMatches()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    AllMatchesMonth = db.Matches.Where(m => m.Date.Value.Month == DateTime.Now.Month).Select(m => new
                    {
                        m.ClubA,
                        m.ClubB,
                        m.Date,
                        m.ScoreA,
                        m.ScoreB,
                        m.LiveLink,
                        AgeID = m.Ages,
                        AgeName = m.Age.NameA,
                        m.Place,
                        m.Gender,
                        Division = db.Divisions.Where(a => a.ID == m.Division.ID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                        Round = db.Rounds.Where(a => a.ID == m.Round.ID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                        Age = db.Ages.Where(a => a.ID == m.Age.ID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                        LogoA = m.Club.Logo,
                        LogoB = m.Club1.Logo
                    }).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("FilteredAllMatches")]
        public BaseResponse GetFilteredAllMatches(long AgeID = 0, long DivisionID = 0, long RoundID = 0)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var AllMatchesMonthList = db.Matches.Where(m => m.Date.Value.Month == DateTime.Now.Month).Select(m => new
                {
                    m.ClubA,
                    m.ClubB,
                    m.Date,
                    m.ScoreA,
                    m.ScoreB,
                    m.LiveLink,
                    AgeID = m.Ages,
                    AgeName = m.Age.NameA,
                    m.Place,
                    m.Gender,
                    Division = db.Divisions.Where(a => a.ID == m.Division.ID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                    Round = db.Rounds.Where(a => a.ID == m.Round.ID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                    Age = db.Ages.Where(a => a.ID == m.Age.ID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                    LogoA = m.Club.Logo,
                    LogoB = m.Club1.Logo
                }).ToList();

                if ((AgeID != 0))
                {
                    AllMatchesMonthList = AllMatchesMonthList.Where(m => m.AgeID == AgeID).ToList();
                }
                if ((DivisionID != 0))
                {
                    AllMatchesMonthList = AllMatchesMonthList.Where(m => m.Division.ID == DivisionID).ToList();
                }
                if ((RoundID != 0))
                {
                    AllMatchesMonthList = AllMatchesMonthList.Where(m => m.Round.ID == RoundID).ToList();
                }

                response.Response = new
                {
                    AllMatchesMonth = AllMatchesMonthList
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("NextMatche")]
        public BaseResponse GetNextMatche()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {
                    NextMatche = (from m in db.Matches
                                  where m.Date > DateTime.Now && (m.ScoreA != null && m.ScoreB != null)
                                  select new
                                  {
                                      ClubA = m.Club.NameA,
                                      ClubB = m.Club1.NameA,
                                      m.Date,
                                      m.LiveLink,
                                      AgeID = m.Age.ID,
                                      AgeName = m.Age.NameA,
                                      m.Place,
                                      m.Gender,
                                      Division = db.Divisions.Where(a => a.ID == m.Division.ID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                                      Round = db.Rounds.Where(a => a.ID == m.Round.ID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                                      Age = db.Ages.Where(a => a.ID == m.Age.ID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                                      LogoA = m.Club.Logo,
                                      LogoB = m.Club1.Logo
                                  }).OrderBy(m => m.Date).Take(4).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("MatcheScore")]
        public BaseResponse GetMatchesScore()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response.Response = new
                {

                    MatcheScore =
                    db.Matches.Where(m => m.ScoreA != null && m.ScoreB != null).Select(m => new
                    {
                        m.ID,
                        ClubA = m.Club.NameA,
                        ClubB = m.Club1.NameA,
                        m.Date,
                        m.ScoreA,
                        m.ScoreB,
                        m.LiveLink,
                        AgeID = m.Age.ID,
                        AgeName = m.Age.NameA,
                        m.Place,
                        m.Gender,
                        Division = db.Divisions.Where(a => a.ID == m.Division.ID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                        Round = db.Rounds.Where(a => a.ID == m.Round.ID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                        Age = db.Ages.Where(a => a.ID == m.Age.ID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                        LogoA = m.Club.Logo,
                        LogoB = m.Club1.Logo
                    }).OrderByDescending(m => m.Date).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("FilteredMatcheScore")]
        public BaseResponse GetFilteredMatcheScore(string gender = "", long? age = -1)
        {
            BaseResponse response = new BaseResponse();
            var matches = db.Matches.ToList();
            if (!string.IsNullOrEmpty(gender))
            {
                matches = matches.Where(g => g.Gender.ToLower() == gender.ToLower()).ToList();
            }
            if (age != -1)
            {
                long ageId = Convert.ToInt64(age);
                matches = matches.Where(a => a.Ages == ageId).ToList();
            }
            try
            {
                response.Response = new
                {

                    MatcheScore =
                    matches.Where(m => m.ScoreA != null && m.ScoreB != null).Select(m => new
                    {
                        m.ID,
                        ClubA = m.Club.NameA,
                        ClubB = m.Club1.NameA,
                        m.Date,
                        m.ScoreA,
                        m.ScoreB,
                        m.LiveLink,
                        AgeID = m.Age.ID,
                        AgeName = m.Age.NameA,
                        m.Place,
                        m.Gender,
                        Division = db.Divisions.Where(a => a.ID == m.Division.ID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                        Round = db.Rounds.Where(a => a.ID == m.Round.ID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                        Age = db.Ages.Where(a => a.ID == m.Age.ID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                        LogoA = m.Club.Logo,
                        LogoB = m.Club1.Logo
                    }).OrderByDescending(m => m.Date).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("TableMatches")]
        public BaseResponse GetAllTableMatches()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                List<MatchesTableModel> matchesResults = new List<MatchesTableModel>();
                List<Match> allMatches = db.Matches.ToList();

                foreach (Match match in allMatches)
                {
                    if (matchesResults.Where(mr => mr.ClubA.ID == match.ClubA && mr.DivisionID == match.DivisionID && mr.AgeID == match.Ages).FirstOrDefault() == null)
                    {
                        matchesResults.Add(new MatchesTableModel()
                        {
                            ClubA = db.Clubs.Where(c => c.ID == match.ClubA).FirstOrDefault(),
                            ClubB = db.Clubs.Where(c => c.ID == match.ClubB).FirstOrDefault(),
                            DivisionID = (int)match.DivisionID,
                            AgeID = (int)match.Ages,
                            RoundID = (match.RoundID != null) ? (int)match.RoundID : 0,
                            Gender = match.Gender,
                            Division = db.Divisions.Where(a => a.ID == match.DivisionID).FirstOrDefault(),
                            Round = db.Rounds.Where(a => a.ID == match.RoundID).FirstOrDefault(),
                            Age = db.Ages.Where(a => a.ID == match.Ages).FirstOrDefault(),
                        });
                    }
                }

                foreach (Match match in allMatches)
                {
                    if (matchesResults.Where(mr => mr.ClubA.ID == match.ClubB && mr.DivisionID == match.DivisionID && mr.AgeID == match.Ages).FirstOrDefault() == null)
                    {
                        matchesResults.Add(new MatchesTableModel()
                        {
                            ClubA = db.Clubs.Where(c => c.ID == match.ClubB).FirstOrDefault(),
                            ClubB = db.Clubs.Where(c => c.ID == match.ClubA).FirstOrDefault(),
                            DivisionID = (int)match.DivisionID,
                            AgeID = (int)match.Ages,
                            RoundID = (match.RoundID != null) ? (int)match.RoundID : 0,
                            Gender = match.Gender,
                            Division = db.Divisions.Where(a => a.ID == match.DivisionID).FirstOrDefault(),
                            Round = db.Rounds.Where(a => a.ID == match.RoundID).FirstOrDefault(),
                            Age = db.Ages.Where(a => a.ID == match.Ages).FirstOrDefault(),
                        });
                    }
                }

                foreach (MatchesTableModel matchResult in matchesResults)
                {
                    var matchPoints = db.MatchPoints.ToList();
                    long? WinnerPoints = 0, DrawPoints = 0, LoserPoints = 0;
                    for (int i = 0; i < matchPoints.Count; i++)
                    {
                        if (matchResult.DivisionID == matchPoints[i].DivisionId && matchResult.AgeID == matchPoints[i].AgeId)
                        {
                            WinnerPoints = matchPoints[i].WinnerPoints;
                            DrawPoints = matchPoints[i].DrawPoints;
                            LoserPoints = matchPoints[i].WinnerPoints;
                        }
                    }
                    List<Match> matches = db.Matches.Where(m => m.DivisionID == matchResult.DivisionID && m.Ages == matchResult.AgeID).ToList();

                    if ((matches != null) || (matches.Count != 0))
                    {
                        matchResult.NumberOfMatches = matches.Where(m => (m.ClubA == matchResult.ClubA.ID || m.ClubB == matchResult.ClubA.ID)).Count();
                        matchResult.Winner = matches.Where(m => (m.ClubA == matchResult.ClubA.ID && m.ScoreA > m.ScoreB) || (m.ClubB == matchResult.ClubA.ID && m.ScoreB > m.ScoreA)).Count();
                        matchResult.Lose = matches.Where(m => (m.ClubA == matchResult.ClubA.ID && m.ScoreA < m.ScoreB) || (m.ClubB == matchResult.ClubA.ID && m.ScoreB < m.ScoreA)).Count();
                        matchResult.Draw = matches.Where(m => (m.ClubA == matchResult.ClubA.ID && m.ScoreA == m.ScoreB) || (m.ClubB == matchResult.ClubA.ID && m.ScoreB == m.ScoreA)).Count();
                        matchResult.Withdraw = matches.Where(m => (m.ClubA == matchResult.ClubA.ID && m.ScoreA == 0) || (m.ClubB == matchResult.ClubA.ID && m.ScoreB == 0)).Count() > 0 ? 1 : 0;
                        matchResult.Points =
                    (matches.Where(m =>
                    (m.ClubA == matchResult.ClubA.ID && m.ScoreA > m.ScoreB) ||
                    (m.ClubB == matchResult.ClubA.ID && m.ScoreB > m.ScoreA)).Count() * (int)WinnerPoints)
                    +
                    (matches.Where(m =>
                    (m.ClubA == matchResult.ClubA.ID && m.ScoreA < m.ScoreB) ||
                    (m.ClubB == matchResult.ClubA.ID && m.ScoreB < m.ScoreA)).Count() * (int)LoserPoints)
                    +
                    (matches.Where(m =>
                    (m.ClubA == matchResult.ClubA.ID && m.ScoreA == m.ScoreB) ||
                    (m.ClubB == matchResult.ClubA.ID && m.ScoreB == m.ScoreA)).Count() * (int)DrawPoints);
                        matchResult.INGoals = matches.Where(m => (m.ClubA == matchResult.ClubA.ID)).Sum(s => s.ScoreB).Value + matches.Where(m => (m.ClubB == matchResult.ClubA.ID)).Sum(s => s.ScoreA).Value;
                        matchResult.OutGoals = matches.Where(m => (m.ClubA == matchResult.ClubA.ID)).Sum(s => s.ScoreA).Value + matches.Where(m => (m.ClubB == matchResult.ClubA.ID)).Sum(s => s.ScoreB).Value;
                    }
                }

                response.Response = new
                {
                    TableMatches = matchesResults.Select(u => new
                    {
                        ID = 0,
                        ClubA = db.Clubs.Where(c => c.ID == u.ClubA.ID).Select(c => new
                        {
                            c.ID,
                            c.NameA,
                            c.Logo,
                        }).FirstOrDefault(),
                        ClubB = db.Clubs.Where(c => c.ID == u.ClubB.ID).Select(c => new
                        {
                            c.ID,
                            c.NameA,
                            c.Logo,
                        }).FirstOrDefault(),
                        u.NumberOfMatches,
                        u.Winner,
                        u.Lose,
                        u.Points,
                        u.INGoals,
                        u.OutGoals,
                        u.DivisionID,
                        u.AgeID,
                        u.Gender,
                        u.RoundID,
                        Division = db.Divisions.Where(a => a.ID == u.DivisionID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                        Round = db.Rounds.Where(a => a.ID == u.RoundID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                        Age = db.Ages.Where(a => a.ID == u.AgeID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                    }).OrderByDescending(u => u.OutGoals - u.INGoals).OrderByDescending(u => u.Points).ToList()
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }

        [HttpGet, Route("FilteredTableMatches")]
        public BaseResponse GetFilteredAllTableMatches(long DivisionID = 0, long AgeID = 0, string Gender = "Male", long RoundID = 0)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                List<MatchesTableModel> matchesResults = new List<MatchesTableModel>();
                List<Match> allMatches = db.Matches.ToList();

                foreach (Match match in allMatches)
                {
                    if (matchesResults.Where(mr => mr.ClubA.ID == match.ClubA && mr.DivisionID == match.DivisionID && mr.AgeID == match.Ages).FirstOrDefault() == null)
                    {
                        matchesResults.Add(new MatchesTableModel()
                        {
                            ClubA = db.Clubs.Where(c => c.ID == match.ClubA).FirstOrDefault(),
                            ClubB = db.Clubs.Where(c => c.ID == match.ClubB).FirstOrDefault(),
                            DivisionID = (int)match.DivisionID,
                            AgeID = (int)match.Ages,
                            RoundID = (match.RoundID != null) ? (int)match.RoundID : 0,
                            Gender = match.Gender,
                            Division = db.Divisions.Where(a => a.ID == match.DivisionID).FirstOrDefault(),
                            Round = db.Rounds.Where(a => a.ID == match.RoundID).FirstOrDefault(),
                            Age = db.Ages.Where(a => a.ID == match.Ages).FirstOrDefault(),
                        });
                    }
                }

                foreach (Match match in allMatches)
                {
                    if (matchesResults.Where(mr => mr.ClubA.ID == match.ClubB && mr.DivisionID == match.DivisionID && mr.AgeID == match.Ages).FirstOrDefault() == null)
                    {
                        matchesResults.Add(new MatchesTableModel()
                        {
                            ClubA = db.Clubs.Where(c => c.ID == match.ClubB).FirstOrDefault(),
                            ClubB = db.Clubs.Where(c => c.ID == match.ClubA).FirstOrDefault(),
                            DivisionID = (int)match.DivisionID,
                            AgeID = (int)match.Ages,
                            RoundID = (match.RoundID != null) ? (int)match.RoundID : 0,
                            Gender = match.Gender,
                            Division = db.Divisions.Where(a => a.ID == match.DivisionID).FirstOrDefault(),
                            Round = db.Rounds.Where(a => a.ID == match.RoundID).FirstOrDefault(),
                            Age = db.Ages.Where(a => a.ID == match.Ages).FirstOrDefault(),
                        });
                    }
                }

                foreach (MatchesTableModel matchResult in matchesResults)
                {
                    var matchPoints = db.MatchPoints.ToList();
                    long? WinnerPoints = 0, DrawPoints = 0, LoserPoints = 0;
                    for (int i = 0; i < matchPoints.Count; i++)
                    {
                        if (matchResult.DivisionID == matchPoints[i].DivisionId && matchResult.AgeID == matchPoints[i].AgeId)
                        {
                            WinnerPoints = matchPoints[i].WinnerPoints;
                            DrawPoints = matchPoints[i].DrawPoints;
                            LoserPoints = matchPoints[i].WinnerPoints;
                        }
                    }
                    List<Match> matches = db.Matches.Where(m => m.DivisionID == matchResult.DivisionID && m.Ages == matchResult.AgeID).ToList();

                    if ((matches != null) || (matches.Count != 0))
                    {
                        matchResult.NumberOfMatches = matches.Where(m => (m.ClubA == matchResult.ClubA.ID || m.ClubB == matchResult.ClubA.ID)).Count();
                        matchResult.Winner = matches.Where(m => (m.ClubA == matchResult.ClubA.ID && m.ScoreA > m.ScoreB) || (m.ClubB == matchResult.ClubA.ID && m.ScoreB > m.ScoreA)).Count();
                        matchResult.Lose = matches.Where(m => (m.ClubA == matchResult.ClubA.ID && m.ScoreA < m.ScoreB) || (m.ClubB == matchResult.ClubA.ID && m.ScoreB < m.ScoreA)).Count();
                        matchResult.Draw = matches.Where(m => (m.ClubA == matchResult.ClubA.ID && m.ScoreA == m.ScoreB) || (m.ClubB == matchResult.ClubA.ID && m.ScoreB == m.ScoreA)).Count();
                        matchResult.Withdraw = matches.Where(m => (m.ClubA == matchResult.ClubA.ID && m.ScoreA == 0) || (m.ClubB == matchResult.ClubA.ID && m.ScoreB == 0)).Count() > 0 ? 1 : 0;
                        //matchResult.Points = (matches.Where(m => (m.ClubA == matchResult.ClubA.ID && m.ScoreA > m.ScoreB) || (m.ClubB == matchResult.ClubA.ID && m.ScoreB > m.ScoreA)).Count() * 2) + (matches.Where(m => (m.ClubA == matchResult.ClubA.ID && m.ScoreA < m.ScoreB) || (m.ClubB == matchResult.ClubA.ID && m.ScoreB < m.ScoreA)).Count());
                        matchResult.Points =
                    (matches.Where(m =>
                    (m.ClubA == matchResult.ClubA.ID && m.ScoreA > m.ScoreB) ||
                    (m.ClubB == matchResult.ClubA.ID && m.ScoreB > m.ScoreA)).Count() * (int)WinnerPoints)
                    +
                    (matches.Where(m =>
                    (m.ClubA == matchResult.ClubA.ID && m.ScoreA < m.ScoreB) ||
                    (m.ClubB == matchResult.ClubA.ID && m.ScoreB < m.ScoreA)).Count() * (int)LoserPoints)
                    +
                    (matches.Where(m =>
                    (m.ClubA == matchResult.ClubA.ID && m.ScoreA == m.ScoreB) ||
                    (m.ClubB == matchResult.ClubA.ID && m.ScoreB == m.ScoreA)).Count() * (int)DrawPoints);
                        matchResult.INGoals = matches.Where(m => (m.ClubA == matchResult.ClubA.ID)).Sum(s => s.ScoreB).Value + matches.Where(m => (m.ClubB == matchResult.ClubA.ID)).Sum(s => s.ScoreA).Value;
                        matchResult.OutGoals = matches.Where(m => (m.ClubA == matchResult.ClubA.ID)).Sum(s => s.ScoreA).Value + matches.Where(m => (m.ClubB == matchResult.ClubA.ID)).Sum(s => s.ScoreB).Value;
                    }
                }

                var TableMatchesList = matchesResults.Select(u => new
                {
                    ID = 0,
                    ClubA = db.Clubs.Where(c => c.ID == u.ClubA.ID).Select(c => new
                    {
                        c.ID,
                        c.NameA,
                        c.Logo,
                    }).FirstOrDefault(),
                    ClubB = db.Clubs.Where(c => c.ID == u.ClubB.ID).Select(c => new
                    {
                        c.ID,
                        c.NameA,
                        c.Logo,
                    }).FirstOrDefault(),
                    u.NumberOfMatches,
                    u.Winner,
                    u.Lose,
                    u.Points,
                    u.INGoals,
                    u.OutGoals,
                    u.DivisionID,
                    u.AgeID,
                    u.Gender,
                    u.RoundID,
                    Division = db.Divisions.Where(a => a.ID == u.DivisionID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                    Round = db.Rounds.Where(a => a.ID == u.RoundID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                    Age = db.Ages.Where(a => a.ID == u.AgeID).Select(a => new { a.ID, a.NameA, a.NameE }).FirstOrDefault(),
                }).OrderByDescending(u => u.OutGoals - u.INGoals).OrderByDescending(u => u.Points).ToList();

                if ((AgeID != 0))
                {
                    TableMatchesList = TableMatchesList.Where(t => t.AgeID == AgeID).ToList();
                }
                if ((DivisionID != 0))
                {
                    TableMatchesList = TableMatchesList.Where(t => t.DivisionID == DivisionID).ToList();
                }
                if ((RoundID != 0))
                {
                    TableMatchesList = TableMatchesList.Where(t => t.RoundID == RoundID).ToList();
                }
                if ((Gender == "Male" || Gender == "Female"))
                {
                    TableMatchesList = TableMatchesList.Where(t => t.Gender == Gender).ToList();
                }

                response.Response = new
                {
                    TableMatches = TableMatchesList
                };
                response.isSuccess = true;
                response.errorMessage = "";
                return response;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.errorMessage = "";
                response.Response = new
                {
                    ExceptionMessage = ex.Message,
                    ExceptionStackTrace = ex.StackTrace
                };
            }
            return response;
        }
    }
}
