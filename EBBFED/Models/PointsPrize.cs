//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EBBFED.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PointsPrize
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PointsPrize()
        {
            this.CompetitionPoints = new HashSet<CompetitionPoint>();
        }
    
        public long ID { get; set; }
        public string NameA { get; set; }
        public string NameE { get; set; }
        public Nullable<long> Points { get; set; }
        public long CompetitionID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompetitionPoint> CompetitionPoints { get; set; }
    }
}
