//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EBBFED.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class GalleryClub
    {
        public long ID { get; set; }
        public long ClubID { get; set; }
        public long GalleryID { get; set; }
    
        public virtual Gallery Gallery { get; set; }
        public virtual Club Club { get; set; }
    }
}
