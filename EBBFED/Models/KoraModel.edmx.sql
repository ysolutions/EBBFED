
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 02/07/2019 17:57:24
-- Generated from EDMX file: D:\Yackeen Solutions\Projects\EBBFED\EBBFED\EBBFED\Models\KoraModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Kora];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_ArchiveArchiveCategory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ArchiveCategories] DROP CONSTRAINT [FK_ArchiveArchiveCategory];
GO
IF OBJECT_ID(N'[dbo].[FK_ArchiveCategoryArchiveFile]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ArchiveFiles] DROP CONSTRAINT [FK_ArchiveCategoryArchiveFile];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Archive]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Archive];
GO
IF OBJECT_ID(N'[dbo].[ArchiveCategories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ArchiveCategories];
GO
IF OBJECT_ID(N'[dbo].[ArchiveFiles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ArchiveFiles];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'NewsClubs'
CREATE TABLE [dbo].[NewsClubs] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [ClubID] bigint  NOT NULL,
    [NewsID] bigint  NOT NULL
);
GO

-- Creating table 'Ages'
CREATE TABLE [dbo].[Ages] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [NameA] nvarchar(max)  NULL,
    [NameE] nvarchar(max)  NULL
);
GO

-- Creating table 'Divisions'
CREATE TABLE [dbo].[Divisions] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [NameA] nvarchar(max)  NULL,
    [NameE] nvarchar(max)  NULL
);
GO

-- Creating table 'PlayerClubs'
CREATE TABLE [dbo].[PlayerClubs] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [ClubID] bigint  NOT NULL,
    [PlayerID] bigint  NOT NULL
);
GO

-- Creating table 'News'
CREATE TABLE [dbo].[News] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [TitleE] nvarchar(max)  NULL,
    [TitleA] nvarchar(max)  NULL,
    [ShortDescriptionE] nvarchar(max)  NULL,
    [ShortDescriptionA] nvarchar(max)  NULL,
    [CreationDate] datetime  NULL,
    [ModifiedDate] datetime  NULL,
    [ImageUrl] nvarchar(max)  NULL,
    [Views] bigint  NULL,
    [ShareLink] nvarchar(max)  NULL,
    [LongDescriptionE] nvarchar(max)  NULL,
    [LongDescriptionA] nvarchar(max)  NULL,
    [Important] bit  NULL
);
GO

-- Creating table 'Galleries'
CREATE TABLE [dbo].[Galleries] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [ImageUrl] nvarchar(max)  NULL,
    [VideoUrl] nvarchar(max)  NULL,
    [DescriptionA] nvarchar(max)  NULL,
    [DescriptionE] nvarchar(max)  NULL,
    [Date] datetime  NULL
);
GO

-- Creating table 'MatchesTables'
CREATE TABLE [dbo].[MatchesTables] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [ClubA_ID] bigint  NOT NULL,
    [ClubB_ID] bigint  NOT NULL,
    [NumberOfMatches] int  NULL,
    [Winner] int  NULL,
    [Lose] int  NULL,
    [Points] int  NULL,
    [INGoals] int  NULL,
    [OutGoals] int  NULL,
    [Draw] int  NULL,
    [DivisionID] bigint  NULL,
    [AgeID] bigint  NULL,
    [RoundID] bigint  NULL,
    [Gender] nvarchar(max)  NULL
);
GO

-- Creating table 'GalleryClubs'
CREATE TABLE [dbo].[GalleryClubs] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [ClubID] bigint  NOT NULL,
    [GalleryID] bigint  NOT NULL
);
GO

-- Creating table 'GalleryPlayers'
CREATE TABLE [dbo].[GalleryPlayers] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [PlayerID] bigint  NOT NULL,
    [GalleryID] bigint  NOT NULL
);
GO

-- Creating table 'Decisions'
CREATE TABLE [dbo].[Decisions] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [TitleA] nvarchar(max)  NULL,
    [TitleE] nvarchar(max)  NULL,
    [ShortDescriptionA] nvarchar(max)  NULL,
    [ShortDescriptionE] nvarchar(max)  NULL,
    [LongDescriptionE] nvarchar(max)  NULL,
    [LongDescriptionA] nvarchar(max)  NULL,
    [CreationDate] datetime  NULL,
    [ModifiedDate] datetime  NULL
);
GO

-- Creating table 'UnionPersons'
CREATE TABLE [dbo].[UnionPersons] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [ImageUrl] nvarchar(max)  NULL,
    [NameA] nvarchar(max)  NULL,
    [BriefA] nvarchar(max)  NULL,
    [NameE] nvarchar(max)  NULL,
    [BriefE] nvarchar(max)  NULL
);
GO

-- Creating table 'UnoinCommittes'
CREATE TABLE [dbo].[UnoinCommittes] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [ImageUrl] nvarchar(max)  NULL,
    [NameA] nvarchar(max)  NULL,
    [NameE] nvarchar(max)  NULL,
    [ObjectivesA] nvarchar(max)  NULL,
    [ObjectivesE] nvarchar(max)  NULL
);
GO

-- Creating table 'CouncilPresidents'
CREATE TABLE [dbo].[CouncilPresidents] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [ImageUrl] nvarchar(max)  NULL,
    [NameA] nvarchar(max)  NULL,
    [NameE] nvarchar(max)  NULL,
    [BriefA] nvarchar(max)  NULL,
    [BriefE] nvarchar(max)  NULL,
    [TitleA] nvarchar(max)  NULL,
    [TitleE] nvarchar(max)  NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'NationalTeams'
CREATE TABLE [dbo].[NationalTeams] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [NameA] nvarchar(max)  NULL,
    [NameE] nvarchar(max)  NULL,
    [ImageUrl] nvarchar(max)  NULL
);
GO

-- Creating table 'NationalNews'
CREATE TABLE [dbo].[NationalNews] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [NewsID] bigint  NOT NULL,
    [NationalID] bigint  NOT NULL
);
GO

-- Creating table 'Judges'
CREATE TABLE [dbo].[Judges] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [NameA] nvarchar(max)  NULL,
    [NameE] nvarchar(max)  NULL,
    [ImageUrl] nvarchar(max)  NULL,
    [Category] nvarchar(max)  NULL,
    [DescriptionA] nvarchar(max)  NULL,
    [DescriptionE] nvarchar(max)  NULL,
    [AreaID] int  NULL
);
GO

-- Creating table 'Transfars'
CREATE TABLE [dbo].[Transfars] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [TransfarFrom] nvarchar(max)  NOT NULL,
    [TransfarTo] nvarchar(max)  NOT NULL,
    [Year] int  NOT NULL,
    [PLayerID] bigint  NOT NULL
);
GO

-- Creating table 'Articles'
CREATE TABLE [dbo].[Articles] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [TitleA] nvarchar(max)  NULL,
    [TitleE] nvarchar(max)  NULL,
    [ShortDescriptionA] nvarchar(max)  NULL,
    [ShortDescriptionE] nvarchar(max)  NULL,
    [LongDescriptionE] nvarchar(max)  NULL,
    [LongDescriptionA] nvarchar(max)  NULL,
    [Date] datetime  NULL,
    [ImageUrl] nvarchar(max)  NULL
);
GO

-- Creating table 'CompetitionPoints'
CREATE TABLE [dbo].[CompetitionPoints] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [PointsID] bigint  NOT NULL,
    [CompetitionID] bigint  NOT NULL
);
GO

-- Creating table 'PointsPrizes'
CREATE TABLE [dbo].[PointsPrizes] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [NameA] nvarchar(max)  NULL,
    [NameE] nvarchar(max)  NULL,
    [Points] bigint  NULL,
    [CompetitionID] bigint  NOT NULL
);
GO

-- Creating table 'PlayerNews'
CREATE TABLE [dbo].[PlayerNews] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [PlayerID] bigint  NOT NULL,
    [NewsID] bigint  NOT NULL
);
GO

-- Creating table 'Coahes'
CREATE TABLE [dbo].[Coahes] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [NameA] nvarchar(max)  NULL,
    [NameE] nvarchar(max)  NULL,
    [ImageUrl] nvarchar(max)  NULL,
    [Age] int  NULL,
    [ClubID] bigint  NOT NULL,
    [Category] nvarchar(max)  NULL,
    [DescriptionA] nvarchar(max)  NULL,
    [DescriptionE] nvarchar(max)  NULL,
    [TitleA] nvarchar(max)  NULL,
    [TitleE] nvarchar(max)  NULL
);
GO

-- Creating table 'TrainingCourses'
CREATE TABLE [dbo].[TrainingCourses] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [TitleA] nvarchar(max)  NULL,
    [TitleE] nvarchar(max)  NULL,
    [ShortDescriptionA] nvarchar(max)  NULL,
    [ShortDescriptionE] nvarchar(max)  NULL,
    [LongDescriptionA] nvarchar(max)  NULL,
    [LongDescriptionE] nvarchar(max)  NULL,
    [Location] nvarchar(max)  NULL,
    [Date] datetime  NULL,
    [CourseType] nvarchar(max)  NULL
);
GO

-- Creating table 'NationalMatches'
CREATE TABLE [dbo].[NationalMatches] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [NationalA] bigint  NOT NULL,
    [NationalB] bigint  NOT NULL,
    [Date] datetime  NULL,
    [Time] time  NOT NULL,
    [Place] nvarchar(max)  NULL,
    [LiveLink] nvarchar(max)  NULL,
    [ScoreA] int  NULL,
    [ScoreB] int  NULL,
    [CompetitionID] bigint  NOT NULL
);
GO

-- Creating table 'RulesAndLaws'
CREATE TABLE [dbo].[RulesAndLaws] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [NameA] nvarchar(max)  NULL,
    [PdfName] nvarchar(max)  NULL,
    [NameE] nvarchar(max)  NULL,
    [ShortDescriptionA] nvarchar(max)  NULL,
    [ShortDescriptionE] nvarchar(max)  NULL
);
GO

-- Creating table 'Players'
CREATE TABLE [dbo].[Players] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [ImageUrl] nvarchar(max)  NULL,
    [Age] int  NULL,
    [PositionID] bigint  NOT NULL,
    [Height] int  NULL,
    [Weight] int  NULL,
    [InternationalMatches] int  NULL,
    [ClubID] bigint  NOT NULL,
    [PlayerType] nvarchar(max)  NULL,
    [NameE] nvarchar(max)  NULL,
    [NameA] nvarchar(max)  NULL,
    [PlayerNumber] int  NULL
);
GO

-- Creating table 'Positions'
CREATE TABLE [dbo].[Positions] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [NameA] nvarchar(max)  NULL,
    [NameE] nvarchar(max)  NULL
);
GO

-- Creating table 'Clubs'
CREATE TABLE [dbo].[Clubs] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [NameE] nvarchar(max)  NULL,
    [NameA] nvarchar(max)  NULL,
    [DivisionID] bigint  NOT NULL,
    [Logo] nvarchar(max)  NULL,
    [EstablishedYear] int  NULL,
    [TitlesWon] nvarchar(max)  NULL,
    [DescriptionA] nvarchar(max)  NULL,
    [DescriptionE] nvarchar(max)  NULL,
    [AreaID] int  NULL
);
GO

-- Creating table 'HistoryUnions'
CREATE TABLE [dbo].[HistoryUnions] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [EstablishedYearA] nvarchar(max)  NULL,
    [EstablishedYearE] nvarchar(max)  NULL,
    [AchievementA] nvarchar(max)  NULL,
    [AchievementE] nvarchar(max)  NULL
);
GO

-- Creating table 'ComitteMembers'
CREATE TABLE [dbo].[ComitteMembers] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [NameA] nvarchar(max)  NULL,
    [NameE] nvarchar(max)  NULL,
    [CommitteID] bigint  NOT NULL
);
GO

-- Creating table 'CompetitionAges'
CREATE TABLE [dbo].[CompetitionAges] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [AgesID] bigint  NOT NULL,
    [CompetitionID] bigint  NOT NULL
);
GO

-- Creating table 'Matches'
CREATE TABLE [dbo].[Matches] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [ClubA] bigint  NOT NULL,
    [ClubB] bigint  NOT NULL,
    [Gender] nvarchar(max)  NULL,
    [Date] datetime  NULL,
    [Place] nvarchar(max)  NULL,
    [LiveLink] nvarchar(max)  NULL,
    [Ages] bigint  NOT NULL,
    [DivisionID] bigint  NOT NULL,
    [ScoreA] int  NULL,
    [ScoreB] int  NULL,
    [Time] time  NULL,
    [RoundID] int  NULL
);
GO

-- Creating table 'Competitions'
CREATE TABLE [dbo].[Competitions] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [NameA] nvarchar(max)  NULL,
    [NameE] nvarchar(max)  NULL,
    [Date] datetime  NULL,
    [Location] nvarchar(max)  NULL,
    [RegisterLink] nvarchar(max)  NULL,
    [Gender] nvarchar(max)  NULL
);
GO

-- Creating table 'CompetitionMatches'
CREATE TABLE [dbo].[CompetitionMatches] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [TeamA] bigint  NOT NULL,
    [TeamB] bigint  NOT NULL,
    [CompetitionID] bigint  NOT NULL,
    [AgesID] bigint  NOT NULL,
    [ScoreA] int  NULL,
    [ScoreB] int  NULL
);
GO

-- Creating table 'CompetitionTables'
CREATE TABLE [dbo].[CompetitionTables] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [Team] bigint  NOT NULL,
    [NumberOfMatches] int  NULL,
    [Winner] int  NULL,
    [Lose] int  NULL,
    [Points] int  NULL,
    [INGoals] int  NULL,
    [OutGoals] int  NULL,
    [Draw] int  NULL,
    [CompetitionID] bigint  NOT NULL
);
GO

-- Creating table 'CompetitionTeams'
CREATE TABLE [dbo].[CompetitionTeams] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [NameA] nvarchar(max)  NULL,
    [NameE] nvarchar(max)  NULL,
    [Logo] nvarchar(max)  NULL
);
GO

-- Creating table 'NationalAges'
CREATE TABLE [dbo].[NationalAges] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [AgesID] bigint  NOT NULL,
    [NationaCompetitionID] bigint  NOT NULL
);
GO

-- Creating table 'NationalCompetitions'
CREATE TABLE [dbo].[NationalCompetitions] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [NameA] nvarchar(max)  NULL,
    [NameE] nvarchar(max)  NULL,
    [Gender] nvarchar(max)  NULL,
    [Date] datetime  NULL
);
GO

-- Creating table 'Areas'
CREATE TABLE [dbo].[Areas] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [NameA] nvarchar(max)  NOT NULL,
    [NameE] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Settings'
CREATE TABLE [dbo].[Settings] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [CoachPDFFile] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Rounds'
CREATE TABLE [dbo].[Rounds] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [NameA] nvarchar(max)  NOT NULL,
    [NameE] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PreviousClubs'
CREATE TABLE [dbo].[PreviousClubs] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [ClubName] nvarchar(max)  NOT NULL,
    [YearFrom] nvarchar(max)  NOT NULL,
    [YearTo] nvarchar(max)  NOT NULL,
    [PlayerID] bigint  NULL
);
GO

-- Creating table 'ContactUs'
CREATE TABLE [dbo].[ContactUs] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [PhoneNumber] nvarchar(max)  NULL,
    [Address] nvarchar(max)  NULL,
    [Email] nvarchar(max)  NULL,
    [LocationLong] nvarchar(max)  NULL,
    [LocationLat] nvarchar(max)  NULL,
    [WorkingHours] nvarchar(max)  NULL,
    [FacebookURL] nvarchar(max)  NULL,
    [TwitterURL] nvarchar(max)  NULL,
    [InstagramURL] nvarchar(max)  NULL,
    [YoutubeURL] nvarchar(max)  NULL
);
GO

-- Creating table 'Archive'
CREATE TABLE [dbo].[Archive] (
    [ID] int IDENTITY(1,1) NOT NULL
);
GO

-- Creating table 'ArchiveCategories'
CREATE TABLE [dbo].[ArchiveCategories] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [NameA] nvarchar(max)  NULL,
    [NameE] nvarchar(max)  NULL,
    [ArchiveID] int  NOT NULL
);
GO

-- Creating table 'ArchiveFiles'
CREATE TABLE [dbo].[ArchiveFiles] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [NameA] nvarchar(max)  NULL,
    [NameE] nvarchar(max)  NULL,
    [Path] nvarchar(max)  NOT NULL,
    [ArchiveCategoryID] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID] in table 'NewsClubs'
ALTER TABLE [dbo].[NewsClubs]
ADD CONSTRAINT [PK_NewsClubs]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Ages'
ALTER TABLE [dbo].[Ages]
ADD CONSTRAINT [PK_Ages]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Divisions'
ALTER TABLE [dbo].[Divisions]
ADD CONSTRAINT [PK_Divisions]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'PlayerClubs'
ALTER TABLE [dbo].[PlayerClubs]
ADD CONSTRAINT [PK_PlayerClubs]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'News'
ALTER TABLE [dbo].[News]
ADD CONSTRAINT [PK_News]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Galleries'
ALTER TABLE [dbo].[Galleries]
ADD CONSTRAINT [PK_Galleries]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'MatchesTables'
ALTER TABLE [dbo].[MatchesTables]
ADD CONSTRAINT [PK_MatchesTables]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'GalleryClubs'
ALTER TABLE [dbo].[GalleryClubs]
ADD CONSTRAINT [PK_GalleryClubs]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'GalleryPlayers'
ALTER TABLE [dbo].[GalleryPlayers]
ADD CONSTRAINT [PK_GalleryPlayers]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Decisions'
ALTER TABLE [dbo].[Decisions]
ADD CONSTRAINT [PK_Decisions]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'UnionPersons'
ALTER TABLE [dbo].[UnionPersons]
ADD CONSTRAINT [PK_UnionPersons]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'UnoinCommittes'
ALTER TABLE [dbo].[UnoinCommittes]
ADD CONSTRAINT [PK_UnoinCommittes]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'CouncilPresidents'
ALTER TABLE [dbo].[CouncilPresidents]
ADD CONSTRAINT [PK_CouncilPresidents]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [ID] in table 'NationalTeams'
ALTER TABLE [dbo].[NationalTeams]
ADD CONSTRAINT [PK_NationalTeams]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'NationalNews'
ALTER TABLE [dbo].[NationalNews]
ADD CONSTRAINT [PK_NationalNews]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Judges'
ALTER TABLE [dbo].[Judges]
ADD CONSTRAINT [PK_Judges]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Transfars'
ALTER TABLE [dbo].[Transfars]
ADD CONSTRAINT [PK_Transfars]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Articles'
ALTER TABLE [dbo].[Articles]
ADD CONSTRAINT [PK_Articles]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'CompetitionPoints'
ALTER TABLE [dbo].[CompetitionPoints]
ADD CONSTRAINT [PK_CompetitionPoints]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'PointsPrizes'
ALTER TABLE [dbo].[PointsPrizes]
ADD CONSTRAINT [PK_PointsPrizes]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'PlayerNews'
ALTER TABLE [dbo].[PlayerNews]
ADD CONSTRAINT [PK_PlayerNews]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Coahes'
ALTER TABLE [dbo].[Coahes]
ADD CONSTRAINT [PK_Coahes]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'TrainingCourses'
ALTER TABLE [dbo].[TrainingCourses]
ADD CONSTRAINT [PK_TrainingCourses]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'NationalMatches'
ALTER TABLE [dbo].[NationalMatches]
ADD CONSTRAINT [PK_NationalMatches]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'RulesAndLaws'
ALTER TABLE [dbo].[RulesAndLaws]
ADD CONSTRAINT [PK_RulesAndLaws]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Players'
ALTER TABLE [dbo].[Players]
ADD CONSTRAINT [PK_Players]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Positions'
ALTER TABLE [dbo].[Positions]
ADD CONSTRAINT [PK_Positions]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Clubs'
ALTER TABLE [dbo].[Clubs]
ADD CONSTRAINT [PK_Clubs]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'HistoryUnions'
ALTER TABLE [dbo].[HistoryUnions]
ADD CONSTRAINT [PK_HistoryUnions]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'ComitteMembers'
ALTER TABLE [dbo].[ComitteMembers]
ADD CONSTRAINT [PK_ComitteMembers]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'CompetitionAges'
ALTER TABLE [dbo].[CompetitionAges]
ADD CONSTRAINT [PK_CompetitionAges]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Matches'
ALTER TABLE [dbo].[Matches]
ADD CONSTRAINT [PK_Matches]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Competitions'
ALTER TABLE [dbo].[Competitions]
ADD CONSTRAINT [PK_Competitions]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'CompetitionMatches'
ALTER TABLE [dbo].[CompetitionMatches]
ADD CONSTRAINT [PK_CompetitionMatches]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'CompetitionTables'
ALTER TABLE [dbo].[CompetitionTables]
ADD CONSTRAINT [PK_CompetitionTables]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'CompetitionTeams'
ALTER TABLE [dbo].[CompetitionTeams]
ADD CONSTRAINT [PK_CompetitionTeams]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'NationalAges'
ALTER TABLE [dbo].[NationalAges]
ADD CONSTRAINT [PK_NationalAges]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'NationalCompetitions'
ALTER TABLE [dbo].[NationalCompetitions]
ADD CONSTRAINT [PK_NationalCompetitions]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Areas'
ALTER TABLE [dbo].[Areas]
ADD CONSTRAINT [PK_Areas]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Settings'
ALTER TABLE [dbo].[Settings]
ADD CONSTRAINT [PK_Settings]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Rounds'
ALTER TABLE [dbo].[Rounds]
ADD CONSTRAINT [PK_Rounds]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'PreviousClubs'
ALTER TABLE [dbo].[PreviousClubs]
ADD CONSTRAINT [PK_PreviousClubs]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'ContactUs'
ALTER TABLE [dbo].[ContactUs]
ADD CONSTRAINT [PK_ContactUs]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Archive'
ALTER TABLE [dbo].[Archive]
ADD CONSTRAINT [PK_Archive]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'ArchiveCategories'
ALTER TABLE [dbo].[ArchiveCategories]
ADD CONSTRAINT [PK_ArchiveCategories]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'ArchiveFiles'
ALTER TABLE [dbo].[ArchiveFiles]
ADD CONSTRAINT [PK_ArchiveFiles]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [NewsID] in table 'NewsClubs'
ALTER TABLE [dbo].[NewsClubs]
ADD CONSTRAINT [FK_NewsClub_News]
    FOREIGN KEY ([NewsID])
    REFERENCES [dbo].[News]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_NewsClub_News'
CREATE INDEX [IX_FK_NewsClub_News]
ON [dbo].[NewsClubs]
    ([NewsID]);
GO

-- Creating foreign key on [GalleryID] in table 'GalleryClubs'
ALTER TABLE [dbo].[GalleryClubs]
ADD CONSTRAINT [FK_GalleryClubPlayer_Gallery]
    FOREIGN KEY ([GalleryID])
    REFERENCES [dbo].[Galleries]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GalleryClubPlayer_Gallery'
CREATE INDEX [IX_FK_GalleryClubPlayer_Gallery]
ON [dbo].[GalleryClubs]
    ([GalleryID]);
GO

-- Creating foreign key on [GalleryID] in table 'GalleryPlayers'
ALTER TABLE [dbo].[GalleryPlayers]
ADD CONSTRAINT [FK_GalleryPlayer_Gallery]
    FOREIGN KEY ([GalleryID])
    REFERENCES [dbo].[Galleries]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GalleryPlayer_Gallery'
CREATE INDEX [IX_FK_GalleryPlayer_Gallery]
ON [dbo].[GalleryPlayers]
    ([GalleryID]);
GO

-- Creating foreign key on [NationalID] in table 'NationalNews'
ALTER TABLE [dbo].[NationalNews]
ADD CONSTRAINT [FK_NationalNews_NationalTeam]
    FOREIGN KEY ([NationalID])
    REFERENCES [dbo].[NationalTeams]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_NationalNews_NationalTeam'
CREATE INDEX [IX_FK_NationalNews_NationalTeam]
ON [dbo].[NationalNews]
    ([NationalID]);
GO

-- Creating foreign key on [NewsID] in table 'NationalNews'
ALTER TABLE [dbo].[NationalNews]
ADD CONSTRAINT [FK_NationalNews_News]
    FOREIGN KEY ([NewsID])
    REFERENCES [dbo].[News]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_NationalNews_News'
CREATE INDEX [IX_FK_NationalNews_News]
ON [dbo].[NationalNews]
    ([NewsID]);
GO

-- Creating foreign key on [PointsID] in table 'CompetitionPoints'
ALTER TABLE [dbo].[CompetitionPoints]
ADD CONSTRAINT [FK_CompetitionPoints_PointsPrize]
    FOREIGN KEY ([PointsID])
    REFERENCES [dbo].[PointsPrizes]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CompetitionPoints_PointsPrize'
CREATE INDEX [IX_FK_CompetitionPoints_PointsPrize]
ON [dbo].[CompetitionPoints]
    ([PointsID]);
GO

-- Creating foreign key on [NewsID] in table 'PlayerNews'
ALTER TABLE [dbo].[PlayerNews]
ADD CONSTRAINT [FK_PlayerNews_News]
    FOREIGN KEY ([NewsID])
    REFERENCES [dbo].[News]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PlayerNews_News'
CREATE INDEX [IX_FK_PlayerNews_News]
ON [dbo].[PlayerNews]
    ([NewsID]);
GO

-- Creating foreign key on [NationalA] in table 'NationalMatches'
ALTER TABLE [dbo].[NationalMatches]
ADD CONSTRAINT [FK_NationalMatches_NationalTeam]
    FOREIGN KEY ([NationalA])
    REFERENCES [dbo].[NationalTeams]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_NationalMatches_NationalTeam'
CREATE INDEX [IX_FK_NationalMatches_NationalTeam]
ON [dbo].[NationalMatches]
    ([NationalA]);
GO

-- Creating foreign key on [NationalB] in table 'NationalMatches'
ALTER TABLE [dbo].[NationalMatches]
ADD CONSTRAINT [FK_NationalMatches_NationalTeam1]
    FOREIGN KEY ([NationalB])
    REFERENCES [dbo].[NationalTeams]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_NationalMatches_NationalTeam1'
CREATE INDEX [IX_FK_NationalMatches_NationalTeam1]
ON [dbo].[NationalMatches]
    ([NationalB]);
GO

-- Creating foreign key on [PlayerID] in table 'GalleryPlayers'
ALTER TABLE [dbo].[GalleryPlayers]
ADD CONSTRAINT [FK_GalleryPlayer_Players]
    FOREIGN KEY ([PlayerID])
    REFERENCES [dbo].[Players]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GalleryPlayer_Players'
CREATE INDEX [IX_FK_GalleryPlayer_Players]
ON [dbo].[GalleryPlayers]
    ([PlayerID]);
GO

-- Creating foreign key on [PlayerID] in table 'PlayerClubs'
ALTER TABLE [dbo].[PlayerClubs]
ADD CONSTRAINT [FK_PlayerClubs_Players]
    FOREIGN KEY ([PlayerID])
    REFERENCES [dbo].[Players]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PlayerClubs_Players'
CREATE INDEX [IX_FK_PlayerClubs_Players]
ON [dbo].[PlayerClubs]
    ([PlayerID]);
GO

-- Creating foreign key on [PlayerID] in table 'PlayerNews'
ALTER TABLE [dbo].[PlayerNews]
ADD CONSTRAINT [FK_PlayerNews_Players]
    FOREIGN KEY ([PlayerID])
    REFERENCES [dbo].[Players]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PlayerNews_Players'
CREATE INDEX [IX_FK_PlayerNews_Players]
ON [dbo].[PlayerNews]
    ([PlayerID]);
GO

-- Creating foreign key on [PositionID] in table 'Players'
ALTER TABLE [dbo].[Players]
ADD CONSTRAINT [FK_Players_Positions]
    FOREIGN KEY ([PositionID])
    REFERENCES [dbo].[Positions]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Players_Positions'
CREATE INDEX [IX_FK_Players_Positions]
ON [dbo].[Players]
    ([PositionID]);
GO

-- Creating foreign key on [PLayerID] in table 'Transfars'
ALTER TABLE [dbo].[Transfars]
ADD CONSTRAINT [FK_Transfar_Players]
    FOREIGN KEY ([PLayerID])
    REFERENCES [dbo].[Players]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Transfar_Players'
CREATE INDEX [IX_FK_Transfar_Players]
ON [dbo].[Transfars]
    ([PLayerID]);
GO

-- Creating foreign key on [DivisionID] in table 'Clubs'
ALTER TABLE [dbo].[Clubs]
ADD CONSTRAINT [FK_Club_Division]
    FOREIGN KEY ([DivisionID])
    REFERENCES [dbo].[Divisions]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Club_Division'
CREATE INDEX [IX_FK_Club_Division]
ON [dbo].[Clubs]
    ([DivisionID]);
GO

-- Creating foreign key on [ClubID] in table 'Coahes'
ALTER TABLE [dbo].[Coahes]
ADD CONSTRAINT [FK_Coahes_Club]
    FOREIGN KEY ([ClubID])
    REFERENCES [dbo].[Clubs]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Coahes_Club'
CREATE INDEX [IX_FK_Coahes_Club]
ON [dbo].[Coahes]
    ([ClubID]);
GO

-- Creating foreign key on [ClubID] in table 'GalleryClubs'
ALTER TABLE [dbo].[GalleryClubs]
ADD CONSTRAINT [FK_GalleryClub_Club]
    FOREIGN KEY ([ClubID])
    REFERENCES [dbo].[Clubs]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GalleryClub_Club'
CREATE INDEX [IX_FK_GalleryClub_Club]
ON [dbo].[GalleryClubs]
    ([ClubID]);
GO

-- Creating foreign key on [ClubA_ID] in table 'MatchesTables'
ALTER TABLE [dbo].[MatchesTables]
ADD CONSTRAINT [FK_MatchesTable_Club]
    FOREIGN KEY ([ClubA_ID])
    REFERENCES [dbo].[Clubs]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MatchesTable_Club'
CREATE INDEX [IX_FK_MatchesTable_Club]
ON [dbo].[MatchesTables]
    ([ClubA_ID]);
GO

-- Creating foreign key on [ClubID] in table 'NewsClubs'
ALTER TABLE [dbo].[NewsClubs]
ADD CONSTRAINT [FK_NewsClub_Club]
    FOREIGN KEY ([ClubID])
    REFERENCES [dbo].[Clubs]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_NewsClub_Club'
CREATE INDEX [IX_FK_NewsClub_Club]
ON [dbo].[NewsClubs]
    ([ClubID]);
GO

-- Creating foreign key on [ClubID] in table 'PlayerClubs'
ALTER TABLE [dbo].[PlayerClubs]
ADD CONSTRAINT [FK_PlayerClubs_Club]
    FOREIGN KEY ([ClubID])
    REFERENCES [dbo].[Clubs]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PlayerClubs_Club'
CREATE INDEX [IX_FK_PlayerClubs_Club]
ON [dbo].[PlayerClubs]
    ([ClubID]);
GO

-- Creating foreign key on [ClubID] in table 'Players'
ALTER TABLE [dbo].[Players]
ADD CONSTRAINT [FK_Players_Club]
    FOREIGN KEY ([ClubID])
    REFERENCES [dbo].[Clubs]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Players_Club'
CREATE INDEX [IX_FK_Players_Club]
ON [dbo].[Players]
    ([ClubID]);
GO

-- Creating foreign key on [CommitteID] in table 'ComitteMembers'
ALTER TABLE [dbo].[ComitteMembers]
ADD CONSTRAINT [FK_ComitteMember_UnoinCommitte]
    FOREIGN KEY ([CommitteID])
    REFERENCES [dbo].[UnoinCommittes]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ComitteMember_UnoinCommitte'
CREATE INDEX [IX_FK_ComitteMember_UnoinCommitte]
ON [dbo].[ComitteMembers]
    ([CommitteID]);
GO

-- Creating foreign key on [AgesID] in table 'CompetitionAges'
ALTER TABLE [dbo].[CompetitionAges]
ADD CONSTRAINT [FK_CompetitionAges_Ages]
    FOREIGN KEY ([AgesID])
    REFERENCES [dbo].[Ages]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CompetitionAges_Ages'
CREATE INDEX [IX_FK_CompetitionAges_Ages]
ON [dbo].[CompetitionAges]
    ([AgesID]);
GO

-- Creating foreign key on [Ages] in table 'Matches'
ALTER TABLE [dbo].[Matches]
ADD CONSTRAINT [FK_Matches_Ages]
    FOREIGN KEY ([Ages])
    REFERENCES [dbo].[Ages]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Matches_Ages'
CREATE INDEX [IX_FK_Matches_Ages]
ON [dbo].[Matches]
    ([Ages]);
GO

-- Creating foreign key on [ClubA] in table 'Matches'
ALTER TABLE [dbo].[Matches]
ADD CONSTRAINT [FK_Matches_Club]
    FOREIGN KEY ([ClubA])
    REFERENCES [dbo].[Clubs]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Matches_Club'
CREATE INDEX [IX_FK_Matches_Club]
ON [dbo].[Matches]
    ([ClubA]);
GO

-- Creating foreign key on [ClubB] in table 'Matches'
ALTER TABLE [dbo].[Matches]
ADD CONSTRAINT [FK_Matches_Club1]
    FOREIGN KEY ([ClubB])
    REFERENCES [dbo].[Clubs]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Matches_Club1'
CREATE INDEX [IX_FK_Matches_Club1]
ON [dbo].[Matches]
    ([ClubB]);
GO

-- Creating foreign key on [DivisionID] in table 'Matches'
ALTER TABLE [dbo].[Matches]
ADD CONSTRAINT [FK_Matches_Division]
    FOREIGN KEY ([DivisionID])
    REFERENCES [dbo].[Divisions]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Matches_Division'
CREATE INDEX [IX_FK_Matches_Division]
ON [dbo].[Matches]
    ([DivisionID]);
GO

-- Creating foreign key on [CompetitionID] in table 'CompetitionAges'
ALTER TABLE [dbo].[CompetitionAges]
ADD CONSTRAINT [FK_CompetitionAges_Competition]
    FOREIGN KEY ([CompetitionID])
    REFERENCES [dbo].[Competitions]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CompetitionAges_Competition'
CREATE INDEX [IX_FK_CompetitionAges_Competition]
ON [dbo].[CompetitionAges]
    ([CompetitionID]);
GO

-- Creating foreign key on [CompetitionID] in table 'CompetitionMatches'
ALTER TABLE [dbo].[CompetitionMatches]
ADD CONSTRAINT [FK_CompetitionMatches_Competition]
    FOREIGN KEY ([CompetitionID])
    REFERENCES [dbo].[Competitions]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CompetitionMatches_Competition'
CREATE INDEX [IX_FK_CompetitionMatches_Competition]
ON [dbo].[CompetitionMatches]
    ([CompetitionID]);
GO

-- Creating foreign key on [CompetitionID] in table 'CompetitionPoints'
ALTER TABLE [dbo].[CompetitionPoints]
ADD CONSTRAINT [FK_CompetitionPoints_Competition]
    FOREIGN KEY ([CompetitionID])
    REFERENCES [dbo].[Competitions]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CompetitionPoints_Competition'
CREATE INDEX [IX_FK_CompetitionPoints_Competition]
ON [dbo].[CompetitionPoints]
    ([CompetitionID]);
GO

-- Creating foreign key on [TeamA] in table 'CompetitionMatches'
ALTER TABLE [dbo].[CompetitionMatches]
ADD CONSTRAINT [FK_CompetitionMatches_CompetitionTeams]
    FOREIGN KEY ([TeamA])
    REFERENCES [dbo].[CompetitionTeams]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CompetitionMatches_CompetitionTeams'
CREATE INDEX [IX_FK_CompetitionMatches_CompetitionTeams]
ON [dbo].[CompetitionMatches]
    ([TeamA]);
GO

-- Creating foreign key on [TeamB] in table 'CompetitionMatches'
ALTER TABLE [dbo].[CompetitionMatches]
ADD CONSTRAINT [FK_CompetitionMatches_CompetitionTeams1]
    FOREIGN KEY ([TeamB])
    REFERENCES [dbo].[CompetitionTeams]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CompetitionMatches_CompetitionTeams1'
CREATE INDEX [IX_FK_CompetitionMatches_CompetitionTeams1]
ON [dbo].[CompetitionMatches]
    ([TeamB]);
GO

-- Creating foreign key on [Team] in table 'CompetitionTables'
ALTER TABLE [dbo].[CompetitionTables]
ADD CONSTRAINT [FK_CompetitionTables_CompetitionTeams]
    FOREIGN KEY ([Team])
    REFERENCES [dbo].[CompetitionTeams]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CompetitionTables_CompetitionTeams'
CREATE INDEX [IX_FK_CompetitionTables_CompetitionTeams]
ON [dbo].[CompetitionTables]
    ([Team]);
GO

-- Creating foreign key on [AgesID] in table 'NationalAges'
ALTER TABLE [dbo].[NationalAges]
ADD CONSTRAINT [FK_NationalAges_Ages]
    FOREIGN KEY ([AgesID])
    REFERENCES [dbo].[Ages]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_NationalAges_Ages'
CREATE INDEX [IX_FK_NationalAges_Ages]
ON [dbo].[NationalAges]
    ([AgesID]);
GO

-- Creating foreign key on [NationaCompetitionID] in table 'NationalAges'
ALTER TABLE [dbo].[NationalAges]
ADD CONSTRAINT [FK_NationalAges_NationalCompetition]
    FOREIGN KEY ([NationaCompetitionID])
    REFERENCES [dbo].[NationalCompetitions]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_NationalAges_NationalCompetition'
CREATE INDEX [IX_FK_NationalAges_NationalCompetition]
ON [dbo].[NationalAges]
    ([NationaCompetitionID]);
GO

-- Creating foreign key on [CompetitionID] in table 'NationalMatches'
ALTER TABLE [dbo].[NationalMatches]
ADD CONSTRAINT [FK_NationalMatches_NationalCompetition]
    FOREIGN KEY ([CompetitionID])
    REFERENCES [dbo].[NationalCompetitions]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_NationalMatches_NationalCompetition'
CREATE INDEX [IX_FK_NationalMatches_NationalCompetition]
ON [dbo].[NationalMatches]
    ([CompetitionID]);
GO

-- Creating foreign key on [AreaID] in table 'Judges'
ALTER TABLE [dbo].[Judges]
ADD CONSTRAINT [FK_JudgeArea]
    FOREIGN KEY ([AreaID])
    REFERENCES [dbo].[Areas]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JudgeArea'
CREATE INDEX [IX_FK_JudgeArea]
ON [dbo].[Judges]
    ([AreaID]);
GO

-- Creating foreign key on [AreaID] in table 'Clubs'
ALTER TABLE [dbo].[Clubs]
ADD CONSTRAINT [FK_ClubArea]
    FOREIGN KEY ([AreaID])
    REFERENCES [dbo].[Areas]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ClubArea'
CREATE INDEX [IX_FK_ClubArea]
ON [dbo].[Clubs]
    ([AreaID]);
GO

-- Creating foreign key on [RoundID] in table 'Matches'
ALTER TABLE [dbo].[Matches]
ADD CONSTRAINT [FK_MatchRound]
    FOREIGN KEY ([RoundID])
    REFERENCES [dbo].[Rounds]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MatchRound'
CREATE INDEX [IX_FK_MatchRound]
ON [dbo].[Matches]
    ([RoundID]);
GO

-- Creating foreign key on [PlayerID] in table 'PreviousClubs'
ALTER TABLE [dbo].[PreviousClubs]
ADD CONSTRAINT [FK_PlayerPreviousClub]
    FOREIGN KEY ([PlayerID])
    REFERENCES [dbo].[Players]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PlayerPreviousClub'
CREATE INDEX [IX_FK_PlayerPreviousClub]
ON [dbo].[PreviousClubs]
    ([PlayerID]);
GO

-- Creating foreign key on [ArchiveID] in table 'ArchiveCategories'
ALTER TABLE [dbo].[ArchiveCategories]
ADD CONSTRAINT [FK_ArchiveArchiveCategory]
    FOREIGN KEY ([ArchiveID])
    REFERENCES [dbo].[Archive]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ArchiveArchiveCategory'
CREATE INDEX [IX_FK_ArchiveArchiveCategory]
ON [dbo].[ArchiveCategories]
    ([ArchiveID]);
GO

-- Creating foreign key on [ArchiveCategoryID] in table 'ArchiveFiles'
ALTER TABLE [dbo].[ArchiveFiles]
ADD CONSTRAINT [FK_ArchiveCategoryArchiveFile]
    FOREIGN KEY ([ArchiveCategoryID])
    REFERENCES [dbo].[ArchiveCategories]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ArchiveCategoryArchiveFile'
CREATE INDEX [IX_FK_ArchiveCategoryArchiveFile]
ON [dbo].[ArchiveFiles]
    ([ArchiveCategoryID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------