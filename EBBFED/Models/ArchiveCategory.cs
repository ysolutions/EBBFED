//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EBBFED.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ArchiveCategory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ArchiveCategory()
        {
            this.ArchiveFiles = new HashSet<ArchiveFile>();
        }
    
        public int ID { get; set; }
        public string NameA { get; set; }
        public string NameE { get; set; }
        public int ArchiveID { get; set; }
    
        public virtual Archive Archive { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ArchiveFile> ArchiveFiles { get; set; }
    }
}
