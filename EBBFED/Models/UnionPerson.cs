//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EBBFED.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class UnionPerson
    {
        public long ID { get; set; }
        public string ImageUrl { get; set; }
        public string NameA { get; set; }
        public string BriefA { get; set; }
        public string NameE { get; set; }
        public string BriefE { get; set; }
    }
}
