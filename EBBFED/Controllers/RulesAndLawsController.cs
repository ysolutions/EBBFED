﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;

using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class RulesAndLawsController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: RulesAndLaws
        public ActionResult Index()
        {
            return View(db.RulesAndLaws.OrderByDescending(r => r.ID).ToList());
        }

        // GET: RulesAndLaws/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RulesAndLaw rulesAndLaw = db.RulesAndLaws.Find(id);
            if (rulesAndLaw == null)
            {
                return HttpNotFound();
            }
            return View(rulesAndLaw);
        }

        // GET: RulesAndLaws/Create
        public ActionResult Create()
        {
            return View(new RulesAndLawsModel());
        }

        // POST: RulesAndLaws/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RulesAndLawsModel rulesAndLaw, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                string fileName = Path.Combine(Server.MapPath("~/Content/Pdfs"), upload.FileName);
                upload.SaveAs(fileName);
                rulesAndLaw.PdfName = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                RulesAndLaw ral = new RulesAndLaw()
                {
                    NameA = rulesAndLaw.NameA,
                    NameE = rulesAndLaw.NameE,
                    ShortDescriptionA = rulesAndLaw.ShortDescriptionA,
                    ShortDescriptionE = rulesAndLaw.ShortDescriptionE,
                    PdfName = rulesAndLaw.PdfName
                };
                db.RulesAndLaws.Add(ral);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rulesAndLaw);
        }

        // GET: RulesAndLaws/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RulesAndLaw rulesAndLaw = db.RulesAndLaws.Find(id);
            if (rulesAndLaw == null)
            {
                return HttpNotFound();
            }
            RulesAndLawsModel ral = new RulesAndLawsModel()
            {
                ID = rulesAndLaw.ID,
                NameA = rulesAndLaw.NameA,
                NameE = rulesAndLaw.NameE,
                ShortDescriptionA = rulesAndLaw.ShortDescriptionA,
                ShortDescriptionE = rulesAndLaw.ShortDescriptionE,
                PdfName = rulesAndLaw.PdfName
            };
            return View(ral);
        }

        // POST: RulesAndLaws/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RulesAndLawsModel rulesAndLaw, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                string fileName = Path.Combine(Server.MapPath("~/Content/Pdfs"), upload.FileName);
                upload.SaveAs(fileName);
                rulesAndLaw.PdfName = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                RulesAndLaw ral = new RulesAndLaw()
                {
                    ID = rulesAndLaw.ID,
                    NameA = rulesAndLaw.NameA,
                    NameE = rulesAndLaw.NameE,
                    ShortDescriptionA = rulesAndLaw.ShortDescriptionA,
                    ShortDescriptionE = rulesAndLaw.ShortDescriptionE,
                    PdfName = rulesAndLaw.PdfName
                };
                db.Entry(ral).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(rulesAndLaw);
        }

        // GET: RulesAndLaws/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RulesAndLaw rulesAndLaw = db.RulesAndLaws.Find(id);
            if (rulesAndLaw == null)
            {
                return HttpNotFound();
            }
            return View(rulesAndLaw);
        }

        // POST: RulesAndLaws/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            RulesAndLaw rulesAndLaw = db.RulesAndLaws.Find(id);
            db.RulesAndLaws.Remove(rulesAndLaw);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
