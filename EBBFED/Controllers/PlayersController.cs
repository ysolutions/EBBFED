﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;

using static EBBFED.FilterConfig;
using OfficeOpenXml;
using System.Drawing;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class PlayersController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: Players
        public ActionResult Index()
        {
            var players = db.Players;
            return View(players.OrderByDescending(r => r.ID).OrderByDescending(r => r.ID).ToList());
        }

        // GET: Players/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Player player = db.Players.Find(id);
            if (player == null)
            {
                return HttpNotFound();
            }
            return View(player);
        }

        // GET: Players/Create
        public ActionResult Create()
        {
            var Clubs = db.Clubs.Select(c => new SelectListItem() { Text = c.NameA, Value = c.ID.ToString() }).ToList();
            ViewBag.Clubs = Clubs;
            ViewBag.Positions = new SelectList(db.Positions, "ID", "NameA");
            List<SelectListItem> PlayerType = new List<SelectListItem> {
                                             new SelectListItem{ Text="محلى ",   Value = "1"},
                                             new SelectListItem{ Text="دولى ", Value = "2"}
                                             };
            ViewBag.PlayerTypes = PlayerType;
            return View(new PlayerModel());
        }

        // POST: Players/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PlayerModel player, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                player.ImageUrl = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                Player newPlayer = new Player()
                {
                    Age = player.Age,
                    PositionID = player.Position,
                    ClubID = player.Club,
                    Height = player.Height,
                    Weight = player.Weight,
                    ImageUrl = player.ImageUrl,
                    InternationalMatches = player.InternationalMatches,
                    NameA = player.NameA,
                    NameE = player.NameE,
                    PlayerType = player.PlayerType == "1" ? "محلى " : "دولى ",
                    PlayerNumber = player.PlayerNumber
                };
                db.Players.Add(newPlayer);
                for (int i = 0; i < player.TransfarFrom.Count; i++)
                {
                    if (player.TransfarFrom[i].Length == 0 || player.TransfarTo[i].Length == 0 || player.Year[i] == 0)
                        continue;
                    Transfar trans = new Transfar()
                    {
                        TransfarFrom = player.TransfarFrom[i],
                        TransfarTo = player.TransfarTo[i],
                        Year = player.Year[i],
                        PLayerID = newPlayer.ID
                    };
                    db.Transfars.Add(trans);
                }
                for (int i = 0; i < player.ClubName.Count; i++)
                {
                    if (player.ClubName[i].Length == 0 || player.YearFrom[i].Length == 0 || player.YearTo[i].Length == 0)
                        continue;
                    PreviousClub preclub = new PreviousClub()
                    {
                        ClubName = player.ClubName[i],
                        YearFrom = player.YearFrom[i],
                        YearTo = player.YearTo[i],
                        PlayerID = newPlayer.ID
                    };
                    db.PreviousClubs.Add(preclub);
                }
                db.SaveChanges();

                //********* Start Email Notification *********//
                SendEmail email = new SendEmail();
                string clubName = db.Clubs.Where(x => x.ID == player.Club).FirstOrDefault().NameA;
                string EmailSubject = "لاعب جديد";
                string Category = "اللاعبين";
                string imgUrl = "http://yaken.cloudapp.net/EBBFEDSiteTest/assets/images/logo-1.png";
                string shortBrief = $"استحداث اللاعب {player.NameA} الى نادى {clubName}";
                string LongDescription = "Long Description Here";
                email.Send(EmailSubject, Category, EmailSubject, imgUrl, "", shortBrief, LongDescription);
                //********* End Email Notification *********//

                return RedirectToAction("Index");
            }

            return View(player);
        }

        // GET: Players/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Player player = db.Players.Find(id);
            if (player == null)
            {
                return HttpNotFound();
            }
            var Clubs = db.Clubs.Select(c => new SelectListItem() { Text = c.NameA, Value = c.ID.ToString() }).ToList();
            ViewBag.Clubs = Clubs;
            List<string> TransfarFroms = db.Transfars.Where(t => t.PLayerID == id).Select(t => t.TransfarFrom).ToList();
            List<string> TransfarTos = db.Transfars.Where(t => t.PLayerID == id).Select(t => t.TransfarTo).ToList();
            List<int> Years = db.Transfars.Where(t => t.PLayerID == id).Select(t => t.Year).ToList();

            List<string> ClubNames = db.PreviousClubs.Where(t => t.PlayerID == id).Select(t => t.ClubName).ToList();
            List<string> YearFroms = db.PreviousClubs.Where(t => t.PlayerID == id).Select(t => t.YearFrom).ToList();
            List<string> YearTos = db.PreviousClubs.Where(t => t.PlayerID == id).Select(t => t.YearTo).ToList();

            ViewBag.Positions = new SelectList(db.Positions, "ID", "NameA");
            List<SelectListItem> PlayerType = new List<SelectListItem> {
                                             new SelectListItem{ Text="محلى ",   Value = "1"},
                                             new SelectListItem{ Text="دولى ", Value = "2"}
                                             };
            ViewBag.PlayerTypes = PlayerType;
            PlayerModel playerModel = new PlayerModel()
            {
                ID = player.ID,
                Age = player.Age ?? 0,
                Position = player.PositionID,
                Club = player.ClubID,
                Height = player.Height ?? 0,
                Weight = player.Weight ?? 0,
                ImageUrl = player.ImageUrl,
                InternationalMatches = player.InternationalMatches ?? 0,
                NameA = player.NameA,
                NameE = player.NameE,
                PlayerType = player.PlayerType== "محلى "?"1":"2",
                TransfarFrom = TransfarFroms,
                TransfarTo = TransfarTos,
                ClubName = ClubNames,
                YearFrom = YearFroms,
                YearTo = YearTos,
                Year = Years,

                PlayerNumber=player.PlayerNumber??0
            };
            return View(playerModel);
        }

        // POST: Players/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PlayerModel player, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                player.ImageUrl = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                Player newPlayer = new Player()
                {
                    ID = player.ID,
                    Age = player.Age,
                    PositionID = player.Position,
                    ClubID = player.Club,
                    Height = player.Height,
                    Weight = player.Weight,
                    ImageUrl = player.ImageUrl,
                    InternationalMatches = player.InternationalMatches,
                    NameA = player.NameA,
                    NameE = player.NameE,
                    PlayerType = player.PlayerType == "1" ? "محلى " : "دولى ",
                    PlayerNumber=player.PlayerNumber
                };
                List<long> TransformationIDs = db.Transfars.Where(t => t.PLayerID == newPlayer.ID).Select(t => t.ID).ToList();
                for (int i = 0; i < player.TransfarFrom.Count; i++)
                {
                    var trans = db.Transfars.Find(TransformationIDs[i]);
                    trans.TransfarFrom = player.TransfarFrom[i];
                    trans.TransfarTo = player.TransfarTo[i];
                    trans.Year = player.Year[i];
                    db.Entry(trans).State = EntityState.Modified;
                }
                List<int> PreClubsIDs = db.PreviousClubs.Where(t => t.PlayerID == newPlayer.ID).Select(t => t.ID).ToList();
                for (int i = 0; i < player.ClubName.Count; i++)
                {
                    var preclub = db.PreviousClubs.Find(PreClubsIDs[i]);
                    preclub.ClubName = player.ClubName[i];
                    preclub.YearFrom = player.YearFrom[i];
                    preclub.YearTo = player.YearTo[i];
                    db.Entry(preclub).State = EntityState.Modified;
                }
                for (int i = 0; i < player.TransfarFrome.Count; i++)
                {
                    if (player.TransfarFrome[i].Length == 0 || player.TransfarToe[i].Length == 0 || player.Yeare[i] == 0)
                        continue;
                    Transfar newtrans = new Transfar()
                    {
                        TransfarFrom = player.TransfarFrome[i],
                        TransfarTo = player.TransfarToe[i],
                        Year = player.Yeare[i],
                        PLayerID = newPlayer.ID
                    };
                    db.Transfars.Add(newtrans);
                }
                for (int i = 0; i < player.ClubNameE.Count; i++)
                {
                    if (player.ClubNameE[i].Length == 0 || player.YearFromE[i].Length == 0 || player.YearToE[i].Length == 0)
                        continue;
                    PreviousClub newpreclub = new PreviousClub()
                    {
                        ClubName = player.ClubNameE[i],
                        YearFrom = player.YearFromE[i],
                        YearTo = player.YearToE[i],
                        PlayerID = newPlayer.ID
                    };
                    db.PreviousClubs.Add(newpreclub);
                }
                db.Entry(newPlayer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var Clubs = db.Clubs.Select(c => new SelectListItem() { Text = c.NameA, Value = c.ID.ToString() }).ToList();
            ViewBag.Clubs = Clubs;
            ViewBag.Positions = new SelectList(db.Positions, "ID", "NameA");
            List<SelectListItem> PlayerType = new List<SelectListItem> {
                                             new SelectListItem{ Text="محلى ",   Value = "1"},
                                             new SelectListItem{ Text="دولى ", Value = "2"}
                                             };
            ViewBag.PlayerTypes = PlayerType;
            return View(player);
        }

        // GET: Players/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Player player = db.Players.Find(id);
            if (player == null)
            {
                return HttpNotFound();
            }
            return View(player);
        }

        // POST: Players/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Player player = db.Players.Find(id);
            db.Players.Remove(player);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //Import Excel into DB
        public ActionResult Upload(FormCollection formCollection)
        {
            var players = new List<Player>();
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["UploadedFile"];
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                    using (var package = new ExcelPackage(file.InputStream))
                    {
                        var currentSheet = package.Workbook.Worksheets;
                        var workSheet = currentSheet.First();
                        var noOfCol = workSheet.Dimension.End.Column;
                        var noOfRow = workSheet.Dimension.End.Row;
                        var clubs = db.Clubs.ToList();
                        var positions = db.Positions.ToList();

                        for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                        {
                            var player = new Player();

                            player.NameA = workSheet.Cells[rowIterator, 1].Value == null ? "" : workSheet.Cells[rowIterator, 1].Value.ToString();
                            player.NameE = workSheet.Cells[rowIterator, 2].Value == null ? "" : workSheet.Cells[rowIterator, 2].Value.ToString();
                            player.ImageUrl = workSheet.Cells[rowIterator, 3].Value == null ? "" : workSheet.Cells[rowIterator, 3].Value.ToString();

                            if (workSheet.Cells[rowIterator, 4].Value != null)
                            {
                                player.Age = Convert.ToInt32(workSheet.Cells[rowIterator, 4].Value);
                            }

                            if (workSheet.Cells[rowIterator, 5].Value != null)
                            {
                                player.Height = Convert.ToInt32(workSheet.Cells[rowIterator, 5].Value);
                            }

                            if (workSheet.Cells[rowIterator, 6].Value != null)
                            {
                                player.Weight = Convert.ToInt32(workSheet.Cells[rowIterator, 6].Value);
                            }

                            if (workSheet.Cells[rowIterator, 7].Value != null)
                            {
                                player.InternationalMatches = Convert.ToInt32(workSheet.Cells[rowIterator, 7].Value);
                            }

                            if (workSheet.Cells[rowIterator, 8].Value != null)
                            {
                                for (int i = 0; i < positions.Count(); i++)
                                {
                                    var positionID = positions.FirstOrDefault(a => (a.NameA != null && a.NameA.ToLower() == workSheet.Cells[rowIterator, 8].Value.ToString().ToLower()) || (a.NameE != null && a.NameE.ToLower() == workSheet.Cells[rowIterator, 8].Value.ToString().ToLower()));
                                    if (positionID != null)
                                    {
                                        player.PositionID = positionID.ID;
                                    }
                                }
                            }

                            player.PlayerType = workSheet.Cells[rowIterator, 9].Value == null ? "" : workSheet.Cells[rowIterator, 9].Value.ToString();

                            if (workSheet.Cells[rowIterator, 10].Value != null)
                            {
                                for (int i = 0; i < clubs.Count(); i++)
                                {
                                    var clubID = clubs.FirstOrDefault(a => (a.NameA != null && a.NameA.ToLower() == workSheet.Cells[rowIterator, 10].Value.ToString().ToLower()) || (a.NameE != null && a.NameE.ToLower() == workSheet.Cells[rowIterator, 10].Value.ToString().ToLower()));
                                    if (clubID != null)
                                    {
                                        player.ClubID = clubID.ID;
                                    }
                                }
                            }

                            if (workSheet.Cells[rowIterator, 11].Value != null)
                            {
                                player.PlayerNumber = Convert.ToInt32(workSheet.Cells[rowIterator, 11].Value);
                            }

                            players.Add(player);
                        }
                    }
                }
            }
            using (db)
            {
                foreach (var item in players)
                {
                    db.Players.Add(item);
                }
                db.SaveChanges();
                TempData["success"] = true;
                return RedirectToAction("Index", db.Players.OrderByDescending(r => r.ID).ToList());
            }
        }

        //Export Excel into DB
        [HttpPost]
        public void Download()
        {
            var players = db.Players.ToList();
            var clubs = db.Clubs.ToList();
            var positions = db.Positions.ToList();

            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
            Sheet.Row(1).Height = 30;
            Sheet.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            Sheet.Row(1).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

            Sheet.Cells["A1"].Value = "NameA";
            Sheet.Cells["B1"].Value = "NameE";
            Sheet.Cells["C1"].Value = "ImageUrl";
            Sheet.Cells["D1"].Value = "Age";
            Sheet.Cells["E1"].Value = "Height";
            Sheet.Cells["F1"].Value = "Weight";
            Sheet.Cells["G1"].Value = "InternationalMatches";
            Sheet.Cells["H1"].Value = "PositionID";
            Sheet.Cells["I1"].Value = "PlayerType";
            Sheet.Cells["J1"].Value = "Club";
            Sheet.Cells["K1"].Value = "PlayerNumber";

            int row = 2;
            foreach (var item in players)
            {
                Sheet.Cells[string.Format("A{0}", row)].Value = item.NameA;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.NameE;
                Sheet.Cells[string.Format("C{0}", row)].Value = item.ImageUrl;
                Sheet.Cells[string.Format("D{0}", row)].Value = item.Age;
                Sheet.Cells[string.Format("E{0}", row)].Value = item.Height;
                Sheet.Cells[string.Format("F{0}", row)].Value = item.Weight;
                Sheet.Cells[string.Format("G{0}", row)].Value = item.InternationalMatches;

                for (int x = 0; x < positions.Count(); x++)
                {
                    if (item.PositionID == positions[x].ID)
                    {
                        Sheet.Cells[string.Format("H{0}", row)].Value = positions[x].NameA;
                    }
                }

                Sheet.Cells[string.Format("I{0}", row)].Value = item.PlayerType;

                for (int x = 0; x < clubs.Count(); x++)
                {
                    if (item.ClubID == clubs[x].ID)
                    {
                        Sheet.Cells[string.Format("J{0}", row)].Value = clubs[x].NameA;
                    }
                }

                Sheet.Cells[string.Format("K{0}", row)].Value = item.PlayerNumber;
                row++;
            }
            var headerCells = Sheet.Cells[1, 1, 1, 11];
            var headerFont = headerCells.Style.Font;
            headerFont.SetFromFont(new Font("Arial", 11, FontStyle.Bold));
            Sheet.Cells["A:AZ"].AutoFitColumns();

            string excelName = "Players";
            using (var memoryStream = new MemoryStream())
            {
                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
                Ep.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
        }
    }
}
