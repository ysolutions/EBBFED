﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;
using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class BranchesController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: Branches
        public ActionResult Index()
        {
            return View(db.Branches.OrderByDescending(r => r.ID).ToList());
        }

        // GET: Branches/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Branch branch = db.Branches.Find(id);
            if (branch == null)
            {
                return HttpNotFound();
            }
            return View(branch);
        }

        // GET: Branches/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Branches/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BranchModel branch)
        {
            if (ModelState.IsValid)
            {
                Branch branches = new Branch()
                {
                    AddressA = branch.AddressA,
                    AddressE = branch.AddressE,
                    Email = branch.Email,
                    Fax = branch.Fax,
                    NameA = branch.NameA,
                    NameE = branch.NameE,
                    Phone1 = branch.Phone1,
                    Phone2 = branch.Phone2
                };
                db.Branches.Add(branches);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(branch);
        }

        // GET: Branches/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Branch branch = db.Branches.Find(id);
            if (branch == null)
            {
                return HttpNotFound();
            }
            BranchModel branches = new BranchModel()
            {
                AddressA = branch.AddressA,
                AddressE = branch.AddressE,
                Email = branch.Email,
                Fax = branch.Fax,
                NameA = branch.NameA,
                NameE = branch.NameE,
                Phone1 = branch.Phone1,
                Phone2 = branch.Phone2,
                ID=branch.ID
            };
            return View(branches);
        }

        // POST: Branches/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BranchModel branch)
        {
            if (ModelState.IsValid)
            {
                Branch branches = new Branch()
                {
                    ID = branch.ID,
                    AddressA = branch.AddressA,
                    AddressE = branch.AddressE,
                    Email = branch.Email,
                    Fax = branch.Fax,
                    NameA = branch.NameA,
                    NameE = branch.NameE,
                    Phone1 = branch.Phone1,
                    Phone2 = branch.Phone2,
                };
                db.Entry(branches).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(branch);
        }

        // GET: Branches/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Branch branch = db.Branches.Find(id);
            if (branch == null)
            {
                return HttpNotFound();
            }
            return View(branch);
        }

        // POST: Branches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Branch branch = db.Branches.Find(id);
            db.Branches.Remove(branch);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
