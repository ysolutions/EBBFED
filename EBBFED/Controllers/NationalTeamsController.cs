﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Infra;
using EBBFED.Models;
using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class NationalTeamsController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: NationalTeams
        public ActionResult Index()
        {
            var x = db.Competitions.SingleOrDefault(c => c.ID == 1);
            return View(db.NationalTeams.OrderByDescending(r => r.ID).ToList());
        }

        // GET: NationalTeams/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NationalTeam nationalTeam = db.NationalTeams.Find(id);
            if (nationalTeam == null)
            {
                return HttpNotFound();
            }
            return View(nationalTeam);
        }

        // GET: NationalTeams/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: NationalTeams/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NationalTeam nationalTeam, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                nationalTeam.ImageUrl = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                db.NationalTeams.Add(nationalTeam);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(nationalTeam);
        }

        // GET: NationalTeams/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NationalTeam nationalTeam = db.NationalTeams.Find(id);
            if (nationalTeam == null)
            {
                return HttpNotFound();
            }
            return View(nationalTeam);
        }

        // POST: NationalTeams/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(NationalTeam nationalTeam, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                nationalTeam.ImageUrl = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                db.Entry(nationalTeam).State = EntityState.Modified;
                db.SaveChanges();
            }
            return View(nationalTeam);
        }

        // GET: NationalTeams/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NationalTeam nationalTeam = db.NationalTeams.Find(id);
            if (nationalTeam == null)
            {
                return HttpNotFound();
            }
            return View(nationalTeam);
        }

        // POST: NationalTeams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            NationalTeam nationalTeam = db.NationalTeams.Find(id);
            db.NationalTeams.Remove(nationalTeam);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
