﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;


using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class CouncilPresidentsController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: CouncilPresidents
        public ActionResult Index()
        {
            return View(db.CouncilPresidents.OrderByDescending(r => r.ID).ToList());
        }

        // GET: CouncilPresidents/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CouncilPresident councilPresident = db.CouncilPresidents.Find(id);
            if (councilPresident == null)
            {
                return HttpNotFound();
            }
            return View(councilPresident);
        }

        // GET: CouncilPresidents/Create
        public ActionResult Create()
        {
            return View(new CouncilPresidentsModel());
        }

        // POST: CouncilPresidents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CouncilPresidentsModel councilPresident, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                councilPresident.ImageUrl = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                CouncilPresident cp = new CouncilPresident()
                {
                    BriefA = councilPresident.BriefA,
                    BriefE = councilPresident.BriefE,
                    NameA = councilPresident.NameA,
                    NameE = councilPresident.NameE,
                    TitleA = councilPresident.TitleA,
                    TitleE = councilPresident.TitleE,
                    ImageUrl = councilPresident.ImageUrl
                };
                db.CouncilPresidents.Add(cp);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(councilPresident);
        }

        // GET: CouncilPresidents/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CouncilPresident councilPresident = db.CouncilPresidents.Find(id);
            if (councilPresident == null)
            {
                return HttpNotFound();
            }
            CouncilPresidentsModel cp = new CouncilPresidentsModel()
            {
                ID = councilPresident.ID,
                BriefA = councilPresident.BriefA,
                BriefE = councilPresident.BriefE,
                NameA = councilPresident.NameA,
                NameE = councilPresident.NameE,
                TitleA = councilPresident.TitleA,
                TitleE = councilPresident.TitleE,
                ImageUrl = councilPresident.ImageUrl
            };
            return View(cp);
        }

        // POST: CouncilPresidents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CouncilPresidentsModel councilPresident, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                councilPresident.ImageUrl = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                CouncilPresident cp = new CouncilPresident()
                {
                    ID = councilPresident.ID,
                    BriefA = councilPresident.BriefA,
                    BriefE = councilPresident.BriefE,
                    NameA = councilPresident.NameA,
                    NameE = councilPresident.NameE,
                    TitleA = councilPresident.TitleA,
                    TitleE = councilPresident.TitleE,
                    ImageUrl = councilPresident.ImageUrl
                };
                db.Entry(cp).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(councilPresident);
        }

        // GET: CouncilPresidents/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CouncilPresident councilPresident = db.CouncilPresidents.Find(id);
            if (councilPresident == null)
            {
                return HttpNotFound();
            }
            return View(councilPresident);
        }

        // POST: CouncilPresidents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            CouncilPresident councilPresident = db.CouncilPresidents.Find(id);
            db.CouncilPresidents.Remove(councilPresident);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
