﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;
using System.IO;

using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class GalleriesController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: Galleries
        public ActionResult Index()
        {
            return View(db.Galleries.OrderByDescending(r => r.ID).ToList());
        }

        // GET: Galleries/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gallery gallery = db.Galleries.Find(id);
            if (gallery == null)
            {
                return HttpNotFound();
            }
            return View(gallery);
        }

        // GET: Galleries/Create
        public ActionResult Create()
        {
            GalleryModel NM = new GalleryModel();
            var Clubs = db.Clubs.Select(c => new SelectListItem() { Text = c.NameA, Value = c.ID.ToString() }).ToList();
            ViewBag.Clubs = Clubs;
            var Players = db.Players.Select(c => new SelectListItem() { Text = c.NameA, Value = c.ID.ToString() }).ToList();
            ViewBag.Players = Players;
            return View(new GalleryModel());
        }

        // POST: Galleries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GalleryModel gallerymodel, HttpPostedFileBase image, HttpPostedFileBase video)
        {
            if (image != null && image.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), image.FileName);
                image.SaveAs(fileName);
                gallerymodel.ImageUrl = image.FileName;
            }
            if (video != null && video.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), video.FileName);
                video.SaveAs(fileName);
                gallerymodel.VideoUrl = video.FileName;
            }
            if (ModelState.IsValid)
            {
                Gallery gallery = new Gallery()
                {
                    Date=DateTime.Now,
                    DescriptionA=gallerymodel.DescriptionA,
                    DescriptionE = gallerymodel.DescriptionE,
                    ImageUrl=gallerymodel.ImageUrl,
                    VideoUrl=gallerymodel.ImageUrl
                };
                db.Galleries.Add(gallery);
                db.SaveChanges();

                //********* Start Email Notification *********//
                SendEmail email = new SendEmail();
                string EmailSubject = "جديد: اضافه البوم جديد";
                string Category = "البوم الصور";
                //string imgUrl = "http://yaken.cloudapp.net/EBBFEDSiteTest/assets/images/logo-1.png";
                string shortBrief = "";
                string LongDescription = gallery.DescriptionA;
                email.Send(EmailSubject, Category, EmailSubject, gallery.ImageUrl, "", shortBrief, LongDescription);
                //********* End Email Notification *********//

                for (int i = 0; i < gallerymodel.Clubs.Count; i++)
                {
                    GalleryClub club = new GalleryClub();
                    club.ClubID = gallerymodel.Clubs[i];
                    club.GalleryID = gallery.ID;
                    db.GalleryClubs.Add(club);
                }
                for (int i = 0; i < gallerymodel.Players.Count; i++)
                {
                    GalleryPlayer player = new GalleryPlayer();
                    player.PlayerID = gallerymodel.Players[i];
                    player.GalleryID = gallery.ID;
                    db.GalleryPlayers.Add(player);
                }
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(gallerymodel);
        }

        // GET: Galleries/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gallery gallery = db.Galleries.Find(id);
            if (gallery == null)
            {
                return HttpNotFound();
            }
            var Clubs = db.Clubs.Select(c => new SelectListItem() { Text = c.NameA, Value = c.ID.ToString() }).ToList();
            ViewBag.AllClubs = Clubs;
            var Players = db.Players.Select(c => new SelectListItem() { Text = c.NameA, Value = c.ID.ToString() }).ToList();
            ViewBag.AllPlayers = Players;
            List<long> AllClubs = db.GalleryClubs.Where(c => c.GalleryID == gallery.ID).Select(c => c.ClubID).ToList();
            List<long> AllPlayers= db.GalleryPlayers.Where(c => c.GalleryID == gallery.ID).Select(c => c.PlayerID).ToList();

            GalleryModel galleryNew = new GalleryModel()
            {
                ID = gallery.ID,
                DescriptionA = gallery.DescriptionA,
                DescriptionE = gallery.DescriptionE,
                ImageUrl = gallery.ImageUrl,
                VideoUrl = gallery.VideoUrl,
                Clubs=AllClubs,
                Players=AllPlayers
            };
            return View(galleryNew);
        }

        // POST: Galleries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(GalleryModel gallery, HttpPostedFileBase image, HttpPostedFileBase video)
        {
            if (image != null && image.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), image.FileName);
                image.SaveAs(fileName);
                gallery.ImageUrl = image.FileName;
            }
            if (video != null && video.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), video.FileName);
                video.SaveAs(fileName);
                gallery.VideoUrl = video.FileName;
            }
            if (ModelState.IsValid)
            {
                gallery.Date = DateTime.Now;

                Gallery galleryNew = new Gallery()
                {
                    ID = gallery.ID,
                    Date = gallery.Date,
                    DescriptionA = gallery.DescriptionA,
                    DescriptionE = gallery.DescriptionE,
                    ImageUrl = gallery.ImageUrl,
                    VideoUrl = gallery.VideoUrl
                };
                db.Entry(galleryNew).State = EntityState.Modified;
                db.SaveChanges();
                var newsClubs = db.GalleryClubs.Where(nc => nc.GalleryID == galleryNew.ID).ToList();
                foreach (var item in newsClubs)
                {
                    db.GalleryClubs.Remove(item);
                }
                var newsPlayers = db.GalleryPlayers.Where(nc => nc.GalleryID == galleryNew.ID).ToList();
                foreach (var item in newsPlayers)
                {
                    db.GalleryPlayers.Remove(item);
                }
                for (int i = 0; i < gallery.Clubs.Count; i++)
                {
                    GalleryClub club = new GalleryClub();
                    club.ClubID = gallery.Clubs[i];
                    club.GalleryID = gallery.ID;
                    db.GalleryClubs.Add(club);
                }
                for (int i = 0; i < gallery.Players.Count; i++)
                {
                    GalleryPlayer player = new GalleryPlayer();
                    player.PlayerID = gallery.Players[i];
                    player.GalleryID = gallery.ID;
                    db.GalleryPlayers.Add(player);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(gallery);
        }

        // GET: Galleries/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gallery gallery = db.Galleries.Find(id);
            if (gallery == null)
            {
                return HttpNotFound();
            }
            return View(gallery);
        }

        // POST: Galleries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Gallery gallery = db.Galleries.Find(id);
            var GalleryClubs = db.GalleryClubs.Where(gc => gc.GalleryID == id).ToList();
            var GalleryPlayers = db.GalleryPlayers.Where(gc => gc.GalleryID == id).ToList();

            foreach(var club in GalleryClubs)
            {
                db.GalleryClubs.Remove(db.GalleryClubs.Find(club.ID));
            }

            foreach (var player in GalleryPlayers)
            {
                db.GalleryPlayers.Remove(db.GalleryPlayers.Find(player.ID));
            }

            db.Galleries.Remove(gallery);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
