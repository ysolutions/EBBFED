﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;

using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class PointsPrizesController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: PointsPrizes
        public ActionResult Index()
        {
            return View(db.PointsPrizes.OrderByDescending(r => r.ID).ToList());
        }

        // GET: PointsPrizes/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PointsPrize pointsPrize = db.PointsPrizes.Find(id);
            if (pointsPrize == null)
            {
                return HttpNotFound();
            }
            return View(pointsPrize);
        }

        // GET: PointsPrizes/Create
        public ActionResult Create()
        {
            ViewBag.Competitions = db.Competitions.Select(c => new SelectListItem { Text = c.NameA, Value = c.ID.ToString() }).ToList();
            return View();
        }

        // POST: PointsPrizes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PointsPrize pointsPrize)
        {
            if (ModelState.IsValid)
            {
                db.PointsPrizes.Add(pointsPrize);
                db.SaveChanges();
                CompetitionPoint cp = new CompetitionPoint()
                {
                    PointsID = pointsPrize.ID,
                    CompetitionID = pointsPrize.CompetitionID
                };
                db.CompetitionPoints.Add(cp);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pointsPrize);
        }

        // GET: PointsPrizes/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PointsPrize pointsPrize = db.PointsPrizes.Find(id);
            if (pointsPrize == null)
            {
                return HttpNotFound();
            }
            ViewBag.Competitions = db.Competitions.Select(c => new SelectListItem { Text = c.NameA, Value = c.ID.ToString() }).ToList();

            return View(pointsPrize);
        }

        // POST: PointsPrizes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PointsPrize pointsPrize)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pointsPrize).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Competitions = db.Competitions.Select(c => new SelectListItem { Text = c.NameA, Value = c.ID.ToString() }).ToList();

            return View(pointsPrize);
        }

        // GET: PointsPrizes/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PointsPrize pointsPrize = db.PointsPrizes.Find(id);
            if (pointsPrize == null)
            {
                return HttpNotFound();
            }
            return View(pointsPrize);
        }

        // POST: PointsPrizes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            PointsPrize pointsPrize = db.PointsPrizes.Find(id);
            db.PointsPrizes.Remove(pointsPrize);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
