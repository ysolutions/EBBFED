﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Infra;
using EBBFED.Models;
using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles ="Admin")]
    public class BranchesPresidentsController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: BranchesPresidents
        public ActionResult Index()
        {
            var branchesPresidents = db.BranchesPresidents.Include(b => b.Branch);
            return View(branchesPresidents.OrderByDescending(r => r.ID).ToList());
        }

        // GET: BranchesPresidents/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BranchesPresident branchesPresident = db.BranchesPresidents.Find(id);
            if (branchesPresident == null)
            {
                return HttpNotFound();
            }
            return View(branchesPresident);
        }

        // GET: BranchesPresidents/Create
        public ActionResult Create()
        {
            ViewBag.BranchId = new SelectList(db.Branches, "ID", "NameE");
            return View();
        }

        // POST: BranchesPresidents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BranchesPresidentsModel branchesPresident,HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string name = Guid.NewGuid() + "_" + upload.FileName;
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), name);
                upload.SaveAs(fileName);
                branchesPresident.ImageUrl = name;
            }
            if (ModelState.IsValid)
            {
                BranchesPresident president = new BranchesPresident()
                {
                    BranchId=(long)branchesPresident.BranchId,
                    BriefA= branchesPresident.BriefA,
                    BriefE= branchesPresident.BriefE,
                    ImageUrl= branchesPresident.ImageUrl,
                    NameA= branchesPresident.NameA,
                    NameE= branchesPresident.NameE,
                    TitleA= branchesPresident.TitleA,
                    TitleE= branchesPresident.TitleE,
                    IsChairman=branchesPresident.IsChairman
                };
                db.BranchesPresidents.Add(president);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BranchId = new SelectList(db.Branches, "ID", "NameE", branchesPresident.BranchId);
            return View(branchesPresident);
        }

        // GET: BranchesPresidents/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BranchesPresident branchesPresident = db.BranchesPresidents.Find(id);
            if (branchesPresident == null)
            {
                return HttpNotFound();
            }
            BranchesPresidentsModel President = new BranchesPresidentsModel()
            {
                BranchId = (long)branchesPresident.BranchId,
                BriefA = branchesPresident.BriefA,
                BriefE = branchesPresident.BriefE,
                ImageUrl = branchesPresident.ImageUrl,
                NameA = branchesPresident.NameA,
                NameE = branchesPresident.NameE,
                TitleA = branchesPresident.TitleA,
                TitleE = branchesPresident.TitleE,
                ID = branchesPresident.ID,
                IsChairman = branchesPresident.IsChairman

            };
            ViewBag.BranchId = new SelectList(db.Branches, "ID", "NameE", branchesPresident.BranchId);
            return View(President);
        }

        // POST: BranchesPresidents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BranchesPresidentsModel branchesPresident, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();

                //Old Image
                string oldName = branchesPresident.ImageUrl;
                string oldPath = Path.Combine(Server.MapPath("~/Content/Images"), oldName);
                if (System.IO.File.Exists(oldPath))
                {
                    System.IO.File.Delete(oldPath);
                }
                string name = Guid.NewGuid() + "_" + upload.FileName;
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), name);
                upload.SaveAs(fileName);
                branchesPresident.ImageUrl = name;
            }
            if (ModelState.IsValid)
            {
                BranchesPresident president = new BranchesPresident()
                {
                    BranchId = (long)branchesPresident.BranchId,
                    BriefA = branchesPresident.BriefA,
                    BriefE = branchesPresident.BriefE,
                    ImageUrl = branchesPresident.ImageUrl,
                    NameA = branchesPresident.NameA,
                    NameE = branchesPresident.NameE,
                    TitleA = branchesPresident.TitleA,
                    TitleE = branchesPresident.TitleE,
                    ID = branchesPresident.ID,
                    IsChairman = branchesPresident.IsChairman

                };
                db.Entry(president).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BranchId = new SelectList(db.Branches, "ID", "NameE", branchesPresident.BranchId);
            return View(branchesPresident);
        }

        // GET: BranchesPresidents/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BranchesPresident branchesPresident = db.BranchesPresidents.Find(id);
            if (branchesPresident == null)
            {
                return HttpNotFound();
            }
            return View(branchesPresident);
        }

        // POST: BranchesPresidents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            BranchesPresident branchesPresident = db.BranchesPresidents.Find(id);
            db.BranchesPresidents.Remove(branchesPresident);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
