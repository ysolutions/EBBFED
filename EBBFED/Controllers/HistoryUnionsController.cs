﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;

using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class HistoryUnionsController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: HistoryUnions
        public ActionResult Index()
        {
            return View(db.HistoryUnions.OrderByDescending(r => r.ID).ToList());
        }

        // GET: HistoryUnions/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HistoryUnion historyUnion = db.HistoryUnions.Find(id);
            if (historyUnion == null)
            {
                return HttpNotFound();
            }
            return View(historyUnion);
        }

        // GET: HistoryUnions/Create
        public ActionResult Create()
        {
            return View(new HistoryUnionModel());
        }

        // POST: HistoryUnions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HistoryUnionModel historyUnion)
        {
            if (ModelState.IsValid)
            {
                HistoryUnion hu = new HistoryUnion()
                {
                    AchievementA = historyUnion.AchievementA,
                    AchievementE = historyUnion.AchievementE,
                    EstablishedYearA = historyUnion.EstablishedYearA,
                    EstablishedYearE = historyUnion.EstablishedYearE
                };
                db.HistoryUnions.Add(hu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(historyUnion);
        }

        // GET: HistoryUnions/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HistoryUnion historyUnion = db.HistoryUnions.Find(id);
            if (historyUnion == null)
            {
                return HttpNotFound();
            }
            HistoryUnionModel hu = new HistoryUnionModel()
            {
                ID = historyUnion.ID,
                AchievementA = historyUnion.AchievementA,
                AchievementE = historyUnion.AchievementE,
                EstablishedYearA = historyUnion.EstablishedYearA,
                EstablishedYearE = historyUnion.EstablishedYearE
            };
            return View(hu);
        }

        // POST: HistoryUnions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HistoryUnionModel historyUnion)
        {
            if (ModelState.IsValid)
            {
                HistoryUnion hu = new HistoryUnion()
                {
                    ID = historyUnion.ID,
                    AchievementA = historyUnion.AchievementA,
                    AchievementE = historyUnion.AchievementE,
                    EstablishedYearA = historyUnion.EstablishedYearA,
                    EstablishedYearE = historyUnion.EstablishedYearE
                };
                db.Entry(hu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(historyUnion);
        }

        // GET: HistoryUnions/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HistoryUnion historyUnion = db.HistoryUnions.Find(id);
            if (historyUnion == null)
            {
                return HttpNotFound();
            }
            return View(historyUnion);
        }

        // POST: HistoryUnions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            HistoryUnion historyUnion = db.HistoryUnions.Find(id);
            db.HistoryUnions.Remove(historyUnion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
