﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Infra;
using EBBFED.Models;


using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class CompetitionTablesController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: CompetitionTables
        public ActionResult Index()
        {
            return View(db.CompetitionTables.OrderByDescending(ct=>ct.Points).ToList());
        }

        // GET: CompetitionTables/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompetitionTable competitionTable = db.CompetitionTables.Find(id);
            if (competitionTable == null)
            {
                return HttpNotFound();
            }
            return View(competitionTable);
        }

        // GET: CompetitionTables/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CompetitionTables/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Team,NumberOfMatches,Winner,Lose,Points,INGoals,OutGoals,Draw")] CompetitionTable competitionTable)
        {
            if (ModelState.IsValid)
            {
                db.CompetitionTables.Add(competitionTable);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(competitionTable);
        }

        // GET: CompetitionTables/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompetitionTable competitionTable = db.CompetitionTables.Find(id);
            if (competitionTable == null)
            {
                return HttpNotFound();
            }
            ViewBag.CompID = competitionTable.CompetitionID;
            return View(competitionTable);
        }

        // POST: CompetitionTables/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Team,NumberOfMatches,Winner,Lose,Points,INGoals,OutGoals,Draw,CompetitionID")] CompetitionTable competitionTable)
        {
            if (ModelState.IsValid)
            {
                db.Entry(competitionTable).State = EntityState.Modified;
                db.SaveChanges();

                //********* Start Email Notification *********//
                string TeamName = db.CompetitionTeams.Where(x => x.ID == competitionTable.Team).FirstOrDefault().NameA;
                SendEmail email = new SendEmail();
                string EmailSubject = "جديد: نتائج فريق "+ TeamName;
                string Category = "نتائج الفرق";
                string imgUrl = "http://yaken.cloudapp.net/EBBFEDSiteTest/assets/images/logo-1.png";
                string shortBrief = $"اصبحت نقاط الفريق {TeamName} {competitionTable.Points} نقاط بعد اخر مباراه له";
                string LongDescription = $"لعب {TeamName} مباراه ، بواقع {competitionTable.Winner} فوز ، {competitionTable.Draw} تعادل و {competitionTable.Lose} خساره ، وبهذا اصبحت نقاط الفريق  {competitionTable.Points} نقطه بعد اخر مباراه له ";
                email.Send(EmailSubject, Category, EmailSubject, imgUrl, "", shortBrief, LongDescription);
                //********* End Email Notification *********//

                return RedirectToAction("Index");
            }
            return View(competitionTable);
        }

        // GET: CompetitionTables/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompetitionTable competitionTable = db.CompetitionTables.Find(id);
            if (competitionTable == null)
            {
                return HttpNotFound();
            }
            return View(competitionTable);
        }

        // POST: CompetitionTables/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            CompetitionTable competitionTable = db.CompetitionTables.Find(id);
            db.CompetitionTables.Remove(competitionTable);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
