﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;
using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class NationalMatchesController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: NationalMatches
        public ActionResult Index()
        {
            var nationalMatches = db.NationalMatches.Include(n => n.NationalTeam).Include(n => n.NationalTeam1);
            return View(nationalMatches.OrderByDescending(r => r.ID).ToList());
        }

        // GET: NationalMatches/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NationalMatch nationalMatch = db.NationalMatches.Find(id);
            if (nationalMatch == null)
            {
                return HttpNotFound();
            }
            return View(nationalMatch);
        }

        // GET: NationalMatches/Create
        public ActionResult Create()
        {
            ViewBag.NationalAs = new SelectList(db.NationalTeams, "ID", "NameA");
            ViewBag.NationalBs = new SelectList(db.NationalTeams, "ID", "NameA");
            ViewBag.NationalCompetition = new SelectList(db.NationalCompetitions, "ID", "NameA");
            return View(new NationalMatcheModel());
        }

        // POST: NationalMatches/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NationalMatcheModel nationalMatch)
        {
            if (ModelState.IsValid)
            {
                NationalMatch NM = new NationalMatch()
                {
                    NationalA = nationalMatch.NationalA,
                    NationalB = nationalMatch.NationalB,
                    Place = nationalMatch.Place,
                    LiveLink = nationalMatch.LiveLink,
                    ScoreA = nationalMatch.ScoreA,
                    ScoreB = nationalMatch.ScoreB,
                    Date = nationalMatch.Date,
                    Time = nationalMatch.Time,
                    CompetitionID = nationalMatch.Competition
                };

                db.NationalMatches.Add(NM);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.NationalAs = new SelectList(db.NationalTeams, "ID", "NameA");
            ViewBag.NationalBs = new SelectList(db.NationalTeams, "ID", "NameA");
            ViewBag.NationalCompetition = new SelectList(db.NationalCompetitions, "ID", "NameA");
            return View(nationalMatch);
        }

        // GET: NationalMatches/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NationalMatch nationalMatch = db.NationalMatches.Find(id);
            if (nationalMatch == null)
            {
                return HttpNotFound();
            }
            ViewBag.NationalA = new SelectList(db.NationalTeams, "ID", "NameA", nationalMatch.NationalA);
            ViewBag.NationalB = new SelectList(db.NationalTeams, "ID", "NameA", nationalMatch.NationalB);
            ViewBag.NationalCompetition = new SelectList(db.NationalCompetitions, "ID", "NameA");
            NationalMatcheModel NM = new NationalMatcheModel()
            {
                ID = nationalMatch.ID,
                NationalA = nationalMatch.NationalA,
                NationalB = nationalMatch.NationalB,
                Place = nationalMatch.Place,
                LiveLink = nationalMatch.LiveLink,
                ScoreA = nationalMatch.ScoreA ?? 0,
                ScoreB = nationalMatch.ScoreB ?? 0,
                Date = (DateTime)nationalMatch.Date,
                Time = nationalMatch.Time,
                CompetitionID = nationalMatch.CompetitionID
            };
            return View(NM);
        }

        // POST: NationalMatches/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(NationalMatch nationalMatch)
        {
            if (ModelState.IsValid)
            {
                NationalMatch NM = new NationalMatch()
                {
                    ID = nationalMatch.ID,
                    NationalA = nationalMatch.NationalA,
                    NationalB = nationalMatch.NationalB,
                    Place = nationalMatch.Place,
                    LiveLink = nationalMatch.LiveLink,
                    ScoreA = nationalMatch.ScoreA ?? 0,
                    ScoreB = nationalMatch.ScoreB ?? 0,
                    Date = (DateTime)nationalMatch.Date,
                    Time = nationalMatch.Time,
                    CompetitionID = nationalMatch.CompetitionID
                };
                db.Entry(NM).State = EntityState.Modified;
                db.SaveChanges();

                //********* Start Email Notification *********//
                SendEmail email = new SendEmail();
                string TeamAName = db.NationalTeams.Where(x => x.ID == NM.NationalA).FirstOrDefault().NameA;
                string TeamBName = db.NationalTeams.Where(x => x.ID == NM.NationalB).FirstOrDefault().NameA;
                string CompName = db.NationalCompetitions.Where(x => x.ID == NM.CompetitionID).FirstOrDefault().NameA;

                string EmailSubject = "عاجل: نتيجه مباراه " + TeamAName + " و " + TeamBName;
                string Category = "اخبار المنتخبات";
                string imgUrl = "http://yaken.cloudapp.net/EBBFEDSiteTest/assets/images/logo-1.png";
                string shortBrief = "عاجل: نتيجه مباراه " + TeamAName  + " و " + TeamBName + " فى " + CompName;
                string LongDescription = $"انتهت منذ قليل المباراه المقامه بين منتخبى {NM.NationalA} و {NM.NationalB} المقامه فى {NM.Place} فى اطار {CompName} <br> وكانت المباراه قد انتهت بنتيجه {NM.ScoreA} مقابل {NM.ScoreB} بين {NM.NationalA} و {NM.NationalB} ";
                email.Send(EmailSubject, Category,EmailSubject, imgUrl,"", shortBrief, LongDescription);
                //********* End Email Notification *********//

                return RedirectToAction("Index");
            }
            ViewBag.NationalA = new SelectList(db.NationalTeams, "ID", "NameA", nationalMatch.NationalA);
            ViewBag.NationalB = new SelectList(db.NationalTeams, "ID", "NameA", nationalMatch.NationalB);
            ViewBag.NationalCompetition = new SelectList(db.NationalCompetitions, "ID", "NameA");
            return View(nationalMatch);
        }

        // GET: NationalMatches/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NationalMatch nationalMatch = db.NationalMatches.Find(id);
            if (nationalMatch == null)
            {
                return HttpNotFound();
            }
            return View(nationalMatch);
        }

        // POST: NationalMatches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            NationalMatch nationalMatch = db.NationalMatches.Find(id);
            db.NationalMatches.Remove(nationalMatch);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
