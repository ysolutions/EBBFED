﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Infra;
using EBBFED.Models;
using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class MatchPointsController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: MatchPoints
        public ActionResult Index()
        {
            var matchPoints = db.MatchPoints.Include(m => m.Age).Include(m => m.Division);
            return View(matchPoints.OrderByDescending(r => r.ID).ToList());
        }

        // GET: MatchPoints/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MatchPoint matchPoint = db.MatchPoints.Find(id);
            if (matchPoint == null)
            {
                return HttpNotFound();
            }
            return View(matchPoint);
        }

        // GET: MatchPoints/Create
        public ActionResult Create()
        {
            ViewBag.AgeId = new SelectList(db.Ages, "ID", "NameA");
            ViewBag.DivisionId = new SelectList(db.Divisions, "ID", "NameA");
            return View();
        }

        // POST: MatchPoints/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MatchPointModel matchPoint)
        {
            if (ModelState.IsValid)
            {
                MatchPoint matchPoints = new MatchPoint()
                {
                    AgeId = matchPoint.AgeId,
                    DivisionId = matchPoint.DivisionId,
                    DrawPoints = matchPoint.DrawPoints,
                    LoserPoints = matchPoint.LoserPoints,
                    WinnerPoints = matchPoint.WinnerPoints
                };
                db.MatchPoints.Add(matchPoints);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AgeId = new SelectList(db.Ages, "ID", "NameA", matchPoint.AgeId);
            ViewBag.DivisionId = new SelectList(db.Divisions, "ID", "NameA", matchPoint.DivisionId);
            return View(matchPoint);
        }

        // GET: MatchPoints/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MatchPoint matchPoint = db.MatchPoints.Find(id);
            if (matchPoint == null)
            {
                return HttpNotFound();
            }
            MatchPoint matchPoints = new MatchPoint()
            {
                AgeId = matchPoint.AgeId,
                DivisionId = matchPoint.DivisionId,
                DrawPoints = matchPoint.DrawPoints,
                LoserPoints = matchPoint.LoserPoints,
                WinnerPoints = matchPoint.WinnerPoints,
                ID = matchPoint.ID
            };
            ViewBag.AgeId = new SelectList(db.Ages, "ID", "NameA", matchPoint.AgeId);
            ViewBag.DivisionId = new SelectList(db.Divisions, "ID", "NameA", matchPoint.DivisionId);
            return View(matchPoint);
        }

        // POST: MatchPoints/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MatchPointModel matchPoint)
        {
            if (ModelState.IsValid)
            {
                MatchPoint matchPoints = new MatchPoint()
                {
                    AgeId = matchPoint.AgeId,
                    DivisionId = matchPoint.DivisionId,
                    DrawPoints = matchPoint.DrawPoints,
                    LoserPoints = matchPoint.LoserPoints,
                    WinnerPoints = matchPoint.WinnerPoints,
                    ID = matchPoint.ID
                };
                db.Entry(matchPoints).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AgeId = new SelectList(db.Ages, "ID", "NameA", matchPoint.AgeId);
            ViewBag.DivisionId = new SelectList(db.Divisions, "ID", "NameA", matchPoint.DivisionId);
            return View(matchPoint);
        }

        // GET: MatchPoints/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MatchPoint matchPoint = db.MatchPoints.Find(id);
            if (matchPoint == null)
            {
                return HttpNotFound();
            }
            return View(matchPoint);
        }

        // POST: MatchPoints/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            MatchPoint matchPoint = db.MatchPoints.Find(id);
            db.MatchPoints.Remove(matchPoint);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
