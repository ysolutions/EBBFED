﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class ArchiveFilesController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: ArchiveFiles
        public ActionResult Index()
        {
            var archiveFiles = db.ArchiveFiles.Include(a => a.ArchiveCategory);
            return View(archiveFiles.OrderByDescending(r => r.ID).ToList());
        }

        // GET: ArchiveFiles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ArchiveFile archiveFile = db.ArchiveFiles.Find(id);
            if (archiveFile == null)
            {
                return HttpNotFound();
            }
            return View(archiveFile);
        }

        // GET: ArchiveFiles/Create
        public ActionResult Create()
        {
            ViewBag.ArchiveCategoryID = new SelectList(db.ArchiveCategories, "ID", "NameA");
            return View();
        }

        // POST: ArchiveFiles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,NameA,NameE,Path,ArchiveCategoryID")] ArchiveFile archiveFile, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    file.SaveAs(HttpContext.Server.MapPath("~/Content/Pdfs/")
                                                          + file.FileName);
                    archiveFile.Path = Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~/Content/Pdfs/") + file.FileName;
                }

                db.ArchiveFiles.Add(archiveFile);
                db.SaveChanges();
                return RedirectToAction("Index", "Archive");
            }

            ViewBag.ArchiveCategoryID = new SelectList(db.ArchiveCategories, "ID", "NameA", archiveFile.ArchiveCategoryID);
            return View(archiveFile);
        }

        // GET: ArchiveFiles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ArchiveFile archiveFile = db.ArchiveFiles.Find(id);
            if (archiveFile == null)
            {
                return HttpNotFound();
            }
            ViewBag.ArchiveCategoryID = new SelectList(db.ArchiveCategories, "ID", "NameA", archiveFile.ArchiveCategoryID);
            return View(archiveFile);
        }

        // POST: ArchiveFiles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,NameA,NameE,Path,ArchiveCategoryID")] ArchiveFile archiveFile, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    file.SaveAs(HttpContext.Server.MapPath("~/Content/Pdfs/")
                                                          + file.FileName);
                    archiveFile.Path = Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~/Content/Pdfs/") + file.FileName;
                }

                db.Entry(archiveFile).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Archive");
            }
            ViewBag.ArchiveCategoryID = new SelectList(db.ArchiveCategories, "ID", "NameA", archiveFile.ArchiveCategoryID);
            return View(archiveFile);
        }

        // GET: ArchiveFiles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ArchiveFile archiveFile = db.ArchiveFiles.Find(id);
            if (archiveFile == null)
            {
                return HttpNotFound();
            }
            return View(archiveFile);
        }

        // POST: ArchiveFiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ArchiveFile archiveFile = db.ArchiveFiles.Find(id);
            db.ArchiveFiles.Remove(archiveFile);
            db.SaveChanges();
            return RedirectToAction("Index", "Archive");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
