﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Infra;
using EBBFED.Models;

namespace EBBFED.Controllers
{
    public class StatisticiansController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: Statisticians
        public ActionResult Index()
        {
            var statisticians = db.Statisticians.Include(s => s.Branch).Include(s => s.Club);
            return View(statisticians.OrderByDescending(r => r.ID).ToList());
        }

        // GET: Statisticians/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Statistician statistician = db.Statisticians.Find(id);
            if (statistician == null)
            {
                return HttpNotFound();
            }
            return View(statistician);
        }

        // GET: Statisticians/Create
        public ActionResult Create()
        {
            ViewBag.BranchId = new SelectList(db.Branches, "ID", "NameE");
            ViewBag.ClubId = new SelectList(db.Clubs, "ID", "NameE");
            return View();
        }

        // POST: Statisticians/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(StatisticiansModel statistician)
        {
            if (ModelState.IsValid)
            {
                Statistician statisticians = new Statistician()
                {
                    BranchId = statistician.BranchId,
                    ClubId = statistician.ClubId,
                    NameA = statistician.NameA,
                    NameE = statistician.NameE
                };
                db.Statisticians.Add(statisticians);
                db.SaveChanges();

                //********* Start Email Notification *********//
                SendEmail email = new SendEmail();
                string ClubName = db.Clubs.Where(x => x.ID == statisticians.ClubId).FirstOrDefault().NameA;
                string BranchName = db.Branches.Where(x => x.ID == statisticians.BranchId).FirstOrDefault().NameA;

                string EmailSubject = "عاجل: احصائى جديد فى الاتحاد المصرى لكره السله";
                string Category = "الاحصائيين";
                string imgUrl = "http://yaken.cloudapp.net/EBBFEDSiteTest/assets/images/logo-1.png";
                string shortBrief = $"قام الاتحاد المصرى لكره السله بتعيين احصائى جديد للمباريات ";
                string LongDescription = $"قام الاتحاد المصرى لكره السله بتعيين الاحصائى  {statisticians.NameA} فى نادى {ClubName} فى فرع {BranchName}";
                email.Send(EmailSubject, Category, EmailSubject, imgUrl, "", shortBrief, LongDescription);
                //********* End Email Notification *********//

                return RedirectToAction("Index");
            }

            ViewBag.BranchId = new SelectList(db.Branches, "ID", "NameE", statistician.BranchId);
            ViewBag.ClubId = new SelectList(db.Clubs, "ID", "NameE", statistician.ClubId);
            return View(statistician);
        }

        // GET: Statisticians/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Statistician statistician = db.Statisticians.Find(id);
            if (statistician == null)
            {
                return HttpNotFound();
            }
            StatisticiansModel statisticians = new StatisticiansModel()
            {
                BranchId = statistician.BranchId,
                ClubId = statistician.ClubId,
                NameA = statistician.NameA,
                NameE = statistician.NameE,
                ID = statistician.ID
            };
            ViewBag.BranchId = new SelectList(db.Branches, "ID", "NameE", statistician.BranchId);
            ViewBag.ClubId = new SelectList(db.Clubs, "ID", "NameE", statistician.ClubId);
            return View(statisticians);
        }

        // POST: Statisticians/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(StatisticiansModel statistician)
        {
            if (ModelState.IsValid)
            {
                Statistician statisticians = new Statistician()
                {
                    BranchId = statistician.BranchId,
                    ClubId = statistician.ClubId,
                    NameA = statistician.NameA,
                    NameE = statistician.NameE,
                    ID = statistician.ID
                };
                db.Entry(statisticians).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BranchId = new SelectList(db.Branches, "ID", "NameE", statistician.BranchId);
            ViewBag.ClubId = new SelectList(db.Clubs, "ID", "NameE", statistician.ClubId);
            return View(statistician);
        }

        // GET: Statisticians/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Statistician statistician = db.Statisticians.Find(id);
            if (statistician == null)
            {
                return HttpNotFound();
            }
            return View(statistician);
        }

        // POST: Statisticians/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Statistician statistician = db.Statisticians.Find(id);
            db.Statisticians.Remove(statistician);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
