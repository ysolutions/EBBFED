﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;

using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class MatchesController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: Matches
        public ActionResult Index()
        {
            return View(db.Matches.OrderByDescending(r => r.ID).ToList());
        }

        // GET: Matches/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Match match = db.Matches.Find(id);
            if (match == null)
            {
                return HttpNotFound();
            }
            return View(match);
        }

        // GET: Matches/Create
        public ActionResult Create()
        {
            List<Gender> genderList = new List<Gender> {
                                             new Gender{ Name="رجال",   ID = 1},
                                             new Gender{ Name="سيدات", ID = 2}
                                             };
            ViewBag.Genders = new SelectList(genderList, "ID", "Name");
            ViewBag.Divisions = new SelectList(db.Divisions, "ID", "NameA");
            ViewBag.Ages = new SelectList(db.Ages, "ID", "NameA");
            ViewBag.Clubs = new SelectList(db.Clubs, "ID", "NameA");
            ViewBag.Rounds = new SelectList(db.Rounds, "ID", "NameA");
            return View(new MatchModel());
        }

        // POST: Matches/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MatchModel newMatch)
        {
            if (!ModelState.IsValid)
            {
                List<Gender> genderList = new List<Gender> {
                                             new Gender{ Name="رجال",   ID = 1},
                                             new Gender{ Name="سيدات", ID = 2}
                                             };
                ViewBag.Genders = new SelectList(genderList, "ID", "Name");
                ViewBag.Divisions = new SelectList(db.Divisions, "ID", "NameA");
                ViewBag.Ages = new SelectList(db.Ages, "ID", "NameA");
                ViewBag.Clubs = new SelectList(db.Clubs, "ID", "NameA");
                ViewBag.Rounds = new SelectList(db.Rounds, "ID", "NameA");
            }
            if (ModelState.IsValid)
            {
                Match match = new Match()
                {
                    ClubA = newMatch.ClubA,
                    ClubB = newMatch.ClubB,
                    Gender = newMatch.Gender == 2 ? "سيدات" : "رجال",
                    Date = newMatch.Date,
                    ScoreA = newMatch.ScoreA,
                    ScoreB = newMatch.ScoreB,
                    Place = newMatch.Place,
                    LiveLink = newMatch.LiveLink,
                    Ages = newMatch.Age,
                    DivisionID = newMatch.DivisionID,
                    RoundID = (int)newMatch.RoundID,
                    Time = (TimeSpan)newMatch.Time,
                };
                db.Matches.Add(match);
                db.SaveChanges();
                // add result to match table
                if (match.ScoreA != 0 || match.ScoreB != 0)
                {
                    AddMatchResults(match);
                }

                return RedirectToAction("Index");
            }

            return View(newMatch);
        }

        // GET: Matches/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Match match = db.Matches.Find(id);
            if (match == null)
            {
                return HttpNotFound();
            }
            List<Gender> genderList = new List<Gender> {
                                             new Gender{ Name="رجال",   ID = 1},
                                             new Gender{ Name="سيدات", ID = 2}
                                             };
            ViewBag.Genders = new SelectList(genderList, "ID", "Name");
            ViewBag.Divisions = new SelectList(db.Divisions, "ID", "NameA");
            ViewBag.Ages = new SelectList(db.Ages, "ID", "NameA");
            ViewBag.Clubs = new SelectList(db.Clubs, "ID", "NameA");
            ViewBag.Rounds = new SelectList(db.Rounds, "ID", "NameA");

            MatchModel NM = new MatchModel()
            {
                ID = match.ID,
                Age = match.Ages,
                Date = (DateTime)match.Date,
                Gender = match.Gender == "رجال" ? 1 : 2,
                LiveLink = match.LiveLink,
                Place = match.Place,
                ScoreA = match.ScoreA ?? 0,
                ScoreB = match.ScoreB ?? 0,
                ClubA = match.ClubA,
                ClubB = match.ClubB,
                DivisionID = match.DivisionID,
                RoundID = match.RoundID ?? 0,
                Time = (TimeSpan)match.Time
            };

            return View(NM);
        }

        // POST: Matches/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MatchModel match)
        {
            if (ModelState.IsValid)
            {
                Match M = new Match()
                {
                    ID = match.ID,
                    Ages = match.Age,
                    Date = (DateTime)match.Date,
                    Gender = match.Gender == 1 ? "رجال" : "سيدات",
                    LiveLink = match.LiveLink,
                    Place = match.Place,
                    ScoreA = match.ScoreA,
                    ScoreB = match.ScoreB,
                    ClubA = match.ClubA,
                    ClubB = match.ClubB,
                    DivisionID = match.DivisionID,
                    RoundID = (int)match.RoundID,
                    Time = match.Time
                };
                db.Entry(M).State = EntityState.Modified;
                db.SaveChanges();

                //********* Start Email Notification *********//
                string ClubAName = db.Clubs.Where(x => x.ID == M.ClubA).FirstOrDefault().NameA;
                string ClubBName = db.Clubs.Where(x => x.ID == M.ClubB).FirstOrDefault().NameA;
                string DivisionName = db.Divisions.Where(x => x.ID == M.DivisionID).FirstOrDefault().NameA;
                string RoundName = db.Rounds.Where(x => x.ID == M.RoundID).FirstOrDefault().NameA;

                SendEmail email = new SendEmail();
                string EmailSubject = "تابع ترتيب الدورى";
                string Category = "أحدث ترتيب للدورى بعد اخر مباراه";
                string imgUrl = "http://yaken.cloudapp.net/EBBFEDSiteTest/assets/images/logo-1.png";
                string shortBrief = $"لمتابعه احدث ترتيب للدورى قم بزياره موقعنا الالكترونى www.blabla.com";
                string LongDescription = $"انتهت المباراه المقامه بين {ClubAName} و {ClubBName} بنتيجه {M.ScoreA} – {M.ScoreB} المقامه فى {M.Place} فى ال{DivisionName} فى ال{RoundName}";
                email.Send(EmailSubject, Category, EmailSubject, imgUrl, "", shortBrief, LongDescription);
                //********* End Email Notification *********//

                // add result to match table
                if (match.ScoreA != 0 || match.ScoreB != 0)
                {
                    AddMatchResults(M);
                }
                return RedirectToAction("Index");
            }
            List<Gender> genderList = new List<Gender> {
                                             new Gender{ Name="رجال",   ID = 1},
                                             new Gender{ Name="سيدات", ID = 2}
                                             };
            ViewBag.Genders = new SelectList(genderList, "ID", "Name");
            ViewBag.Divisions = new SelectList(db.Divisions, "ID", "NameA");
            ViewBag.Ages = new SelectList(db.Ages, "ID", "NameA");
            ViewBag.Clubs = new SelectList(db.Clubs, "ID", "NameA");
            ViewBag.Rounds = new SelectList(db.Rounds, "ID", "NameA");

            return View(match);
        }

        // GET: Matches/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Match match = db.Matches.Find(id);
            if (match == null)
            {
                return HttpNotFound();
            }
            return View(match);
        }

        // POST: Matches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Match match = db.Matches.Find(id);
            db.Matches.Remove(match);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public void AddMatchResults(Match match)
        {
            var club1 = db.MatchesTables.Where(
                mt => mt.ClubA_ID == match.ClubA && mt.ClubB_ID == match.ClubB && mt.DivisionID == match.DivisionID
                && mt.AgeID == match.Ages && mt.RoundID == match.RoundID && mt.Gender == match.Gender
            ).FirstOrDefault();
            var matchPoints = db.MatchPoints.ToList();
            int winnerPoints = -2, loserPoints =-2, DrawPoints = -2;
            if (club1 == null)
            {
                for (int i = 0; i < matchPoints.Count; i++)
                {
                    if (match.DivisionID == matchPoints[i].DivisionId && match.Ages == matchPoints[i].AgeId)
                    {
                        winnerPoints = Convert.ToInt32(matchPoints[i].WinnerPoints);
                        loserPoints = Convert.ToInt32(matchPoints[i].LoserPoints);
                        DrawPoints = Convert.ToInt32(matchPoints[i].DrawPoints);
                    }
                }
                MatchesTable mt = new MatchesTable()
                {
                    ClubA_ID = match.ClubA,
                    ClubB_ID = match.ClubB,
                    NumberOfMatches = 1,
                    Winner = (match.ScoreA > match.ScoreB ? 1 : 0),
                    Draw = (match.ScoreA == match.ScoreB ? 1 : 0),
                    Lose = (match.ScoreA < match.ScoreB ? 1 : 0),
                    INGoals = match.ScoreA,
                    OutGoals = match.ScoreB,
                    Points = ((match.ScoreA > match.ScoreB) ? winnerPoints : (match.ScoreA < match.ScoreB) ? loserPoints : DrawPoints),
                    AgeID = match.Ages,
                    RoundID = match.RoundID,
                    DivisionID = match.DivisionID,
                    Gender = match.Gender,
                };
                db.MatchesTables.Add(mt);
            }
            else
            {
                club1.ClubA_ID = match.ClubA;
                club1.ClubB_ID = match.ClubB;
                club1.NumberOfMatches = 1;
                club1.Winner = (match.ScoreA > match.ScoreB ? 1 : 0);
                club1.Draw = (match.ScoreA == match.ScoreB ? 1 : 0);
                club1.Lose = (match.ScoreA < match.ScoreB ? 1 : 0);
                club1.INGoals = match.ScoreA;
                club1.OutGoals = match.ScoreB;
                club1.Points = ((match.ScoreA > match.ScoreB) ? winnerPoints : (match.ScoreA < match.ScoreB) ? loserPoints : DrawPoints);
                db.Entry(club1).State = EntityState.Modified;
            }
            db.SaveChanges();
        }
    }
}