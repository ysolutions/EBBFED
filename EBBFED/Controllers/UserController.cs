﻿using EBBFED.Infra;
using EBBFED.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class UserController : Controller
    {
        private KoraEntities db = new KoraEntities();

        [AllowAnonymous]
        public ActionResult Index()
        {
            var authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                if (authTicket != null && !authTicket.Expired)
                {
                    var roles = authTicket.UserData.Split(',');
                    var User = new System.Security.Principal.GenericPrincipal(new FormsIdentity(authTicket), roles);
                    var Email = User.Identity.Name;
                    var _user = db.Users.Where(x => x.Email == Email).FirstOrDefault();
                    Session["User"] = _user.Email;

                }
            }
            return View(); 
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = db.Users.Where(x => x.Email == model.Email && x.Password == model.Password).FirstOrDefault();

            if (user != null)
            {
                Session["User"] = user.Email;
                FormsAuthentication.SetAuthCookie(model.Email, false);

                var authTicket = new FormsAuthenticationTicket(1, user.Email, DateTime.Now, DateTime.Now.AddMinutes(200000), false, user.Email);
                string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                HttpContext.Response.Cookies.Add(authCookie);
                return RedirectToAction("Index", "User");
            }

            else
            {
                ModelState.AddModelError("", "Invalid login attempt.");
                return View(model);
            }

        }

        [AllowAnonymous]
        public ActionResult LogOff()
        {
            Session["User"] = null;
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "User");
        }
    
        [AuthorizeUser(Roles = "Admin")]
        public ActionResult UpdateUser()
        {
            var admin = db.Users.Find(1);
            return View(admin);
        }

        [AuthorizeUser(Roles = "Admin")]
        [HttpPost]
        public ActionResult UpdateUser(User model)
        {
            var _User = db.Users.Find(model.ID);
            _User.Email = model.Email;
            _User.Password = model.Password;
            db.Entry(_User).State= EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Login");
        }

    }
}