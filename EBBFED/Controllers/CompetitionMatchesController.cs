﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Infra;
using EBBFED.Models;

using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class CompetitionMatchesController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: CompetitionMatches
        public ActionResult Index()
        {
            var competitionMatches = db.CompetitionMatches.Include(c => c.CompetitionTeam).Include(c => c.CompetitionTeam1);
            return View(competitionMatches.OrderByDescending(r => r.ID).ToList());
        }

        // GET: CompetitionMatches/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompetitionMatch competitionMatch = db.CompetitionMatches.Find(id);
            if (competitionMatch == null)
            {
                return HttpNotFound();
            }
            return View(competitionMatch);
        }

        // GET: CompetitionMatches/Create
        public ActionResult Create()
        {
            ViewBag.AgesID = new SelectList(db.CompetitionAges, "ID", "AgesA");
            ViewBag.TeamA = new SelectList(db.CompetitionTeams, "ID", "NameA");
            ViewBag.TeamB = new SelectList(db.CompetitionTeams, "ID", "NameA");
            ViewBag.Competitions = db.Competitions.Select(c => new SelectListItem { Text = c.NameA, Value = c.ID.ToString() }).ToList();

            return View(new CompetitionMatchModel());
        }

        // POST: CompetitionMatches/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CompetitionMatchModel competitionMatch)
        {
            if (ModelState.IsValid)
            {
                CompetitionMatch NMcompetitionMatch = new CompetitionMatch()
                {
                    AgesID = competitionMatch.AgesID,
                    ScoreA = competitionMatch.ScoreA,
                    ScoreB = competitionMatch.ScoreB,
                    CompetitionID = competitionMatch.CompetitionID,
                    Date = competitionMatch.Date,
                    Time = competitionMatch.Time,
                    TeamA = competitionMatch.TeamA,
                    TeamB = competitionMatch.TeamB
                };
                db.CompetitionMatches.Add(NMcompetitionMatch);
                db.SaveChanges();
                if (competitionMatch.ScoreA != 0 || competitionMatch.ScoreB != 0)
                {
                    AddResultMatchA(competitionMatch, new CompetitionMatch());
                    AddResultMatchB(competitionMatch, new CompetitionMatch());
                }


                return RedirectToAction("Index");
            }

            ViewBag.AgesID = new SelectList(db.CompetitionAges, "ID", "AgesA", competitionMatch.AgesID);
            ViewBag.TeamA = new SelectList(db.CompetitionTeams, "ID", "NameA", competitionMatch.TeamA);
            ViewBag.TeamB = new SelectList(db.CompetitionTeams, "ID", "NameA", competitionMatch.TeamB);
            ViewBag.Competitions = db.Competitions.Select(c => new SelectListItem { Text = c.NameA, Value = c.ID.ToString() }).ToList();

            return View(competitionMatch);
        }

        // GET: CompetitionMatches/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompetitionMatch competitionMatch = db.CompetitionMatches.Find(id);
            if (competitionMatch == null)
            {
                return HttpNotFound();
            }
            ViewBag.AgesID = new SelectList(db.CompetitionAges, "ID", "AgesA", competitionMatch.AgesID);
            ViewBag.TeamA = new SelectList(db.CompetitionTeams, "ID", "NameA", competitionMatch.TeamA);
            ViewBag.TeamB = new SelectList(db.CompetitionTeams, "ID", "NameA", competitionMatch.TeamB);
            ViewBag.Competitions = db.Competitions.Select(c => new SelectListItem { Text = c.NameA, Value = c.ID.ToString() }).ToList();
            CompetitionMatchModel NMcompetitionMatch = new CompetitionMatchModel
            {
                ID = competitionMatch.ID,
                AgesID = competitionMatch.AgesID,
                ScoreA = competitionMatch.ScoreA ?? 0,
                ScoreB = competitionMatch.ScoreB ?? 0,
                CompetitionID = competitionMatch.CompetitionID,
                Date = (DateTime)competitionMatch.Date,
                Time = (TimeSpan)competitionMatch.Time,
                TeamA = competitionMatch.TeamA,
                TeamB = competitionMatch.TeamB
            };
            return View(NMcompetitionMatch);
        }

        // POST: CompetitionMatches/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CompetitionMatchModel competitionMatch)
        {
            if (ModelState.IsValid)
            {
                var ResultLastMatch = db.CompetitionMatches.Where(cm => cm.ID == competitionMatch.ID).FirstOrDefault();
                if (competitionMatch.ScoreA != 0 || competitionMatch.ScoreB != 0)
                {
                    AddResultMatchA(competitionMatch, ResultLastMatch);
                    AddResultMatchB(competitionMatch, ResultLastMatch);
                }
                ResultLastMatch.AgesID = competitionMatch.AgesID;
                ResultLastMatch.ScoreA = competitionMatch.ScoreA;
                ResultLastMatch.ScoreB = competitionMatch.ScoreB;
                ResultLastMatch.CompetitionID = competitionMatch.CompetitionID;
                ResultLastMatch.Date = competitionMatch.Date;
                ResultLastMatch.Time = competitionMatch.Time;
                ResultLastMatch.TeamA = competitionMatch.TeamA;
                ResultLastMatch.TeamB = competitionMatch.TeamB;
                db.Entry(ResultLastMatch).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AgesID = new SelectList(db.CompetitionAges, "ID", "AgesA", competitionMatch.AgesID);
            ViewBag.TeamA = new SelectList(db.CompetitionTeams, "ID", "NameA", competitionMatch.TeamA);
            ViewBag.TeamB = new SelectList(db.CompetitionTeams, "ID", "NameA", competitionMatch.TeamB);
            ViewBag.Competitions = db.Competitions.Select(c => new SelectListItem { Text = c.NameA, Value = c.ID.ToString() }).ToList();
            return View(competitionMatch);
        }

        // GET: CompetitionMatches/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompetitionMatch competitionMatch = db.CompetitionMatches.Find(id);
            if (competitionMatch == null)
            {
                return HttpNotFound();
            }
            return View(competitionMatch);
        }

        // POST: CompetitionMatches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            CompetitionMatch competitionMatch = db.CompetitionMatches.Find(id);
            db.CompetitionMatches.Remove(competitionMatch);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public void AddResultMatchA(CompetitionMatchModel match, CompetitionMatch ResultLastMatch)
        {
            var club1 = db.CompetitionTables.Where(u => u.CompetitionID == match.CompetitionID && u.Team == match.TeamA).FirstOrDefault();
            if (club1 == null)
            {
                CompetitionTable mt = new CompetitionTable()
                {
                    CompetitionID = match.CompetitionID,
                    Team = match.TeamA,
                    NumberOfMatches = 1,
                    Winner = (match.ScoreA > match.ScoreB ? 1 : 0),
                    Draw = (match.ScoreA == match.ScoreB ? 1 : 0),
                    Lose = (match.ScoreA < match.ScoreB ? 1 : 0),
                    INGoals = match.ScoreA,
                    OutGoals = match.ScoreB,
                    Points = ((match.ScoreA > match.ScoreB) ? 3 : (match.ScoreA < match.ScoreB) ? 0 : 1)
                };
                db.CompetitionTables.Add(mt);
            }
            else
            {
                club1.CompetitionID = match.CompetitionID;
                club1.Team = match.TeamA;
                //club1.NumberOfMatches += 1;
                //removeLastReuslt
                club1.Winner -= (ResultLastMatch.ScoreA > ResultLastMatch.ScoreB ? 1 : 0);
                club1.Draw -= (ResultLastMatch.ScoreA == ResultLastMatch.ScoreB ? 1 : 0);
                club1.Lose -= (ResultLastMatch.ScoreA < ResultLastMatch.ScoreB ? 1 : 0);
                club1.INGoals -= ResultLastMatch.ScoreA;
                club1.OutGoals -= ResultLastMatch.ScoreB;
                club1.Points -= ((ResultLastMatch.ScoreA > ResultLastMatch.ScoreB) ? 3 : (ResultLastMatch.ScoreA < ResultLastMatch.ScoreB) ? 0 : 1);
                // add new result
                club1.Winner += (match.ScoreA > match.ScoreB ? 1 : 0);
                club1.Draw += (match.ScoreA == match.ScoreB ? 1 : 0);
                club1.Lose += (match.ScoreA < match.ScoreB ? 1 : 0);
                club1.INGoals += match.ScoreA;
                club1.OutGoals += match.ScoreB;
                club1.Points += ((match.ScoreA > match.ScoreB) ? 3 : (match.ScoreA < match.ScoreB) ? 0 : 1);

                db.Entry(club1).State = EntityState.Modified;
            }
            db.SaveChanges();
        }

        public void AddResultMatchB(CompetitionMatchModel match, CompetitionMatch ResultLastMatch)
        {
            var club1 = db.CompetitionTables.Where(u => u.CompetitionID == match.CompetitionID && u.Team == match.TeamB).FirstOrDefault();
            if (club1 == null)
            {
                CompetitionTable mt = new CompetitionTable()
                {
                    CompetitionID = match.CompetitionID,
                    Team = match.TeamB,
                    NumberOfMatches = 1,
                    Winner = (match.ScoreB > match.ScoreA ? 1 : 0),
                    Draw = (match.ScoreB == match.ScoreA ? 1 : 0),
                    Lose = (match.ScoreB < match.ScoreA ? 1 : 0),
                    INGoals = match.ScoreB,
                    OutGoals = match.ScoreA,
                    Points = ((match.ScoreB > match.ScoreA) ? 3 : (match.ScoreB < match.ScoreA) ? 0 : 1)
                };
                db.CompetitionTables.Add(mt);
            }
            else
            {
                club1.CompetitionID = match.CompetitionID;
                club1.Team = match.TeamB;
                //club1.NumberOfMatches += 1;
                // remove Last result
                club1.Winner -= (ResultLastMatch.ScoreB > ResultLastMatch.ScoreA ? 1 : 0);
                club1.Draw -= (ResultLastMatch.ScoreB == ResultLastMatch.ScoreA ? 1 : 0);
                club1.Lose -= (ResultLastMatch.ScoreB < ResultLastMatch.ScoreA ? 1 : 0);
                club1.INGoals -= ResultLastMatch.ScoreB;
                club1.OutGoals -= ResultLastMatch.ScoreA;
                club1.Points -= ((ResultLastMatch.ScoreB > ResultLastMatch.ScoreA) ? 3 : (ResultLastMatch.ScoreB < ResultLastMatch.ScoreA) ? 0 : 1);

                //add new result
                club1.Winner += (match.ScoreB > match.ScoreA ? 1 : 0);
                club1.Draw += (match.ScoreB == match.ScoreA ? 1 : 0);
                club1.Lose += (match.ScoreB < match.ScoreA ? 1 : 0);
                club1.INGoals += match.ScoreB;
                club1.OutGoals += match.ScoreA;
                club1.Points += ((match.ScoreB > match.ScoreA) ? 3 : (match.ScoreB < match.ScoreA) ? 0 : 1);
                db.Entry(club1).State = EntityState.Modified;
            }
            db.SaveChanges();
        }

    }
}
