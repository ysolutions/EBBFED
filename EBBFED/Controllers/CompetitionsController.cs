﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;

using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class CompetitionsController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: Competitions
        public ActionResult Index()
        {
            return View(db.Competitions.OrderByDescending(r => r.ID).ToList());
        }

        // GET: Competitions/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Competition competition = db.Competitions.Find(id);
            if (competition == null)
            {
                return HttpNotFound();
            }
            return View(competition);
        }

        // GET: Competitions/Create
        public ActionResult Create()
        {
            ViewBag.AllAges = new SelectList(db.Ages, "ID", "NameA");
            return View(new CompetitionModel());
        }

        // POST: Competitions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CompetitionModel competition)
        {
            if (ModelState.IsValid)
            {
                Competition cm = new Competition()
                {
                    Date = competition.Date,
                    Location = competition.Location,
                    NameA = competition.NameA,
                    NameE = competition.NameE,
                    RegisterLink = competition.RegisterLink
                };
                db.Competitions.Add(cm);
                db.SaveChanges();
                for (int i = 0; i < competition.Ages.Count; i++)
                {
                    CompetitionAge CA = new CompetitionAge();
                    CA.AgesID = competition.Ages[i];
                    CA.CompetitionID = cm.ID;
                    db.CompetitionAges.Add(CA);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AllAges = new SelectList(db.Ages, "ID", "NameA");
            return View(competition);
        }

        // GET: Competitions/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Competition competition = db.Competitions.Find(id);
            if (competition == null)
            {
                return HttpNotFound();
            }
            List<long>Age= db.CompetitionAges.Where(n => n.CompetitionID== id).Select(n => n.AgesID).ToList();
            CompetitionModel cm = new CompetitionModel()
            {
                ID = competition.ID,
                Date = (DateTime)competition.Date,
                Location = competition.Location,
                NameA = competition.NameA,
                NameE = competition.NameE,
                RegisterLink = competition.RegisterLink,
                Ages=Age
            };
            ViewBag.AllAges = new SelectList(db.Ages, "ID", "NameA");

            return View(cm);
        }

        // POST: Competitions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CompetitionModel competition)
        {
            if (ModelState.IsValid)
            {
                Competition cm = new Competition()
                {
                    ID = competition.ID,
                    Date = (DateTime)competition.Date,
                    Location = competition.Location,
                    NameA = competition.NameA,
                    NameE = competition.NameE,
                    RegisterLink = competition.RegisterLink
                };
                db.Entry(cm).State = EntityState.Modified;
                db.SaveChanges();

                var competitionAge = db.CompetitionAges.Where(nc => nc.CompetitionID == competition.ID).ToList();
                foreach (var item in competitionAge)
                {
                    db.CompetitionAges.Remove(item);
                }
                for (int i = 0; i < competition.Ages.Count; i++)
                {
                    CompetitionAge CA = new CompetitionAge();
                    CA.AgesID = competition.Ages[i];
                    CA.CompetitionID = cm.ID;
                    db.CompetitionAges.Add(CA);
                }
                db.SaveChanges();

                string mailSubject = $"نتيجه جديده:";
                string mailBody = $"<h2>ننتيجه جديده</h2>";
                UserNotificationModel.SendEmail(mailSubject, mailBody);

                return RedirectToAction("Index");
            }
            ViewBag.AllAges = new SelectList(db.Ages, "ID", "NameA");
            return View(competition);
        }

        // GET: Competitions/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Competition competition = db.Competitions.Find(id);
            if (competition == null)
            {
                return HttpNotFound();
            }
            return View(competition);
        }

        // POST: Competitions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Competition competition = db.Competitions.Find(id);
            db.Competitions.Remove(competition);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
