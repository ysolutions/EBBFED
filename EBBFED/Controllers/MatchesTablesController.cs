﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;

using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class MatchesTablesController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: MatchesTables
        public ActionResult Index()
        {
            List<MatchesTableModel> matchesResults = new List<MatchesTableModel>();
            List<Match> allMatches = db.Matches.ToList();

            foreach (Match match in allMatches)
            {
                if (matchesResults.Where(mr => mr.ClubA.ID == match.ClubA && mr.DivisionID == match.DivisionID && mr.AgeID == match.Ages).FirstOrDefault() == null)
                {
                    matchesResults.Add(new MatchesTableModel()
                    {
                        ClubA = db.Clubs.Where(c => c.ID == match.ClubA).FirstOrDefault(),
                        ClubB = db.Clubs.Where(c => c.ID == match.ClubB).FirstOrDefault(),
                        DivisionID = (int)match.DivisionID,
                        AgeID = (int)match.Ages,
                        RoundID = (match.RoundID != null) ? (int)match.RoundID : 0,
                        Gender = match.Gender,
                        Division = db.Divisions.Where(a => a.ID == match.DivisionID).FirstOrDefault(),
                        Round = db.Rounds.Where(a => a.ID == match.RoundID).FirstOrDefault(),
                        Age = db.Ages.Where(a => a.ID == match.Ages).FirstOrDefault(),
                    });
                }
            }

            foreach (Match match in allMatches)
            {
                if (matchesResults.Where(mr => mr.ClubA.ID == match.ClubB && mr.DivisionID == match.DivisionID && mr.AgeID == match.Ages).FirstOrDefault() == null)
                {
                    matchesResults.Add(new MatchesTableModel()
                    {
                        ClubA = db.Clubs.Where(c => c.ID == match.ClubB).FirstOrDefault(),
                        ClubB = db.Clubs.Where(c => c.ID == match.ClubA).FirstOrDefault(),
                        DivisionID = (int)match.DivisionID,
                        AgeID = (int)match.Ages,
                        RoundID = (match.RoundID != null) ? (int)match.RoundID : 0,
                        Gender = match.Gender,
                        Division = db.Divisions.Where(a => a.ID == match.DivisionID).FirstOrDefault(),
                        Round = db.Rounds.Where(a => a.ID == match.RoundID).FirstOrDefault(),
                        Age = db.Ages.Where(a => a.ID == match.Ages).FirstOrDefault(),
                    });
                }
            }

            foreach (MatchesTableModel matchResult in matchesResults)
            {
                var matchPoints = db.MatchPoints.ToList();
                long? WinnerPoints = 0, DrawPoints = 0, LoserPoints = 0;
                for (int i = 0; i < matchPoints.Count; i++)
                {
                    if (matchResult.DivisionID == matchPoints[i].DivisionId && matchResult.AgeID == matchPoints[i].AgeId)
                    {
                        WinnerPoints = matchPoints[i].WinnerPoints;
                        DrawPoints = matchPoints[i].DrawPoints;
                        LoserPoints = matchPoints[i].WinnerPoints;
                    }
                }
                List<Match> matches = db.Matches.Where(m => m.DivisionID == matchResult.DivisionID && m.Ages == matchResult.AgeID).ToList();

                if ((matches != null) || (matches.Count != 0))
                {
                    matchResult.NumberOfMatches = matches.Where(m => (m.ClubA == matchResult.ClubA.ID || m.ClubB == matchResult.ClubA.ID)).Count();
                    matchResult.Winner = matches.Where(m => (m.ClubA == matchResult.ClubA.ID && m.ScoreA > m.ScoreB) || (m.ClubB == matchResult.ClubA.ID && m.ScoreB > m.ScoreA)).Count();
                    matchResult.Lose = matches.Where(m => (m.ClubA == matchResult.ClubA.ID && m.ScoreA < m.ScoreB) || (m.ClubB == matchResult.ClubA.ID && m.ScoreB < m.ScoreA)).Count();
                    matchResult.Draw = matches.Where(m => (m.ClubA == matchResult.ClubA.ID && m.ScoreA == m.ScoreB) || (m.ClubB == matchResult.ClubA.ID && m.ScoreB == m.ScoreA)).Count();
                    matchResult.Withdraw = matches.Where(m => (m.ClubA == matchResult.ClubA.ID && m.ScoreA == 0) || (m.ClubB == matchResult.ClubA.ID && m.ScoreB == 0)).Count() > 0 ? 1 : 0;
                    //matchResult.Points = (matches.Where(m => (m.ClubA == matchResult.ClubA.ID && m.ScoreA > m.ScoreB) || (m.ClubB == matchResult.ClubA.ID && m.ScoreB > m.ScoreA)).Count() * 3) + (matches.Where(m => (m.ClubA == matchResult.ClubA.ID && m.ScoreA < m.ScoreB) || (m.ClubB == matchResult.ClubA.ID && m.ScoreB < m.ScoreA)).Count());
                    matchResult.Points = 
                    (matches.Where(m => 
                    (m.ClubA == matchResult.ClubA.ID && m.ScoreA > m.ScoreB) ||
                    (m.ClubB == matchResult.ClubA.ID && m.ScoreB > m.ScoreA)).Count() * (int)WinnerPoints) 
                    +
                    (matches.Where(m => 
                    (m.ClubA == matchResult.ClubA.ID && m.ScoreA < m.ScoreB) ||
                    (m.ClubB == matchResult.ClubA.ID && m.ScoreB < m.ScoreA)).Count() * (int)LoserPoints)
                    +
                    (matches.Where(m =>
                    (m.ClubA == matchResult.ClubA.ID && m.ScoreA == m.ScoreB) ||
                    (m.ClubB == matchResult.ClubA.ID && m.ScoreB == m.ScoreA)).Count() * (int)DrawPoints);
                    matchResult.INGoals = matches.Where(m => (m.ClubA == matchResult.ClubA.ID)).Sum(s => s.ScoreB).Value + matches.Where(m => (m.ClubB == matchResult.ClubA.ID)).Sum(s => s.ScoreA).Value;
                    matchResult.OutGoals = matches.Where(m => (m.ClubA == matchResult.ClubA.ID)).Sum(s => s.ScoreA).Value + matches.Where(m => (m.ClubB == matchResult.ClubA.ID)).Sum(s => s.ScoreB).Value;
                }
            }

            return View(matchesResults);
        }

        // GET: MatchesTables/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MatchesTable matchesTable = db.MatchesTables.Find(id);
            if (matchesTable == null)
            {
                return HttpNotFound();
            }
            return View(matchesTable);
        }

        // GET: MatchesTables/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MatchesTables/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,ClubA_ID,ClubB_ID,NumberOfMatches,Winner,Lose,Points,INGoals,OutGoals,Draw,DivisionID,AgeID,RoundID,Gender")] MatchesTable matchesTable)
        {
            if (ModelState.IsValid)
            {
                db.MatchesTables.Add(matchesTable);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(matchesTable);
        }

        // GET: MatchesTables/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MatchesTable matchesTable = db.MatchesTables.Find(id);
            if (matchesTable == null)
            {
                return HttpNotFound();
            }
            return View(matchesTable);
        }

        // POST: MatchesTables/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,ClubA_ID,ClubB_ID,NumberOfMatches,Winner,Lose,Points,INGoals,OutGoals,Draw,DivisionID,AgeID,RoundID,Gender")] MatchesTable matchesTable)
        {
            if (ModelState.IsValid)
            {
                db.Entry(matchesTable).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(matchesTable);
        }

        // GET: MatchesTables/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MatchesTable matchesTable = db.MatchesTables.Find(id);
            if (matchesTable == null)
            {
                return HttpNotFound();
            }
            return View(matchesTable);
        }

        // POST: MatchesTables/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            MatchesTable matchesTable = db.MatchesTables.Find(id);
            db.MatchesTables.Remove(matchesTable);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
