﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;
using System.Data.Entity.Validation;
using System.Diagnostics;

using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class DecisionsController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: Decisions
        public ActionResult Index()
        {
            return View(db.Decisions.OrderByDescending(r => r.ID).ToList());
        }

        // GET: Decisions/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Decision decision = db.Decisions.Find(id);
            if (decision == null)
            {
                return HttpNotFound();
            }
            return View(decision);
        }

        // GET: Decisions/Create
        public ActionResult Create()
        {
            return View(new DecisionModel());
        }

        // POST: Decisions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DecisionModel decision)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    decision.CreationDate = DateTime.Now;
                    decision.ModifiedDate = DateTime.Now;
                    Decision ds = new Decision()
                    {
                        ShortDescriptionA = decision.ShortDescriptionA,
                        ShortDescriptionE = decision.ShortDescriptionE,
                        TitleA = decision.TitleA,
                        TitleE = decision.TitleE,
                        LongDescriptionA = decision.LongDescriptionA,
                        LongDescriptionE = decision.LongDescriptionE,
                        CreationDate = decision.CreationDate,
                        ModifiedDate = decision.ModifiedDate
                    };
                    db.Decisions.Add(ds);
                    db.SaveChanges();

                    //********* Start Email Notification *********//
                    SendEmail email = new SendEmail();
                    string EmailSubject = "عاجل: قرار جديد من مجلس اداره الاتحاد المصرى لكره السله";
                    string Category = "الاتحاد المصرى لكره السله";
                    string imgUrl = "http://yaken.cloudapp.net/EBBFEDSiteTest/assets/images/logo-1.png";
                    email.Send(EmailSubject, Category, ds.TitleA, imgUrl, "", ds.ShortDescriptionA, ds.LongDescriptionA);
                    //********* End Email Notification *********//

                    return RedirectToAction("Index");
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }

            return View(decision);
        }

        // GET: Decisions/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Decision decision = db.Decisions.Find(id);
            if (decision == null)
            {
                return HttpNotFound();
            }
            DecisionModel ds = new DecisionModel()
            {
                ID = decision.ID,
                ShortDescriptionA = decision.ShortDescriptionA,
                ShortDescriptionE = decision.ShortDescriptionE,
                TitleA = decision.TitleA,
                TitleE = decision.TitleE,
                LongDescriptionA = decision.LongDescriptionA,
                LongDescriptionE = decision.LongDescriptionE,
                CreationDate = (DateTime)decision.CreationDate,
                ModifiedDate = (DateTime)decision.ModifiedDate
            };
            return View(ds);
        }

        // POST: Decisions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DecisionModel decision)
        {
            if (ModelState.IsValid)
            {
                decision.ModifiedDate = DateTime.Now;
                Decision ds = new Decision()
                {
                    ID = decision.ID,
                    ShortDescriptionA = decision.ShortDescriptionA,
                    ShortDescriptionE = decision.ShortDescriptionE,
                    TitleA = decision.TitleA,
                    TitleE = decision.TitleE,
                    LongDescriptionA = decision.LongDescriptionA,
                    LongDescriptionE = decision.LongDescriptionE,
                    //CreationDate = (DateTime)decision.CreationDate,
                    //ModifiedDate = (DateTime)decision.ModifiedDate
                };
                db.Entry(ds).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(decision);
        }

        // GET: Decisions/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Decision decision = db.Decisions.Find(id);
            if (decision == null)
            {
                return HttpNotFound();
            }
            return View(decision);
        }

        // POST: Decisions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Decision decision = db.Decisions.Find(id);
            db.Decisions.Remove(decision);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
