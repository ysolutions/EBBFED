﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;
using static EBBFED.FilterConfig;
using OfficeOpenXml;
using System.Drawing;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class CoahesController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: Coahes
        public ActionResult Index()
        {
            return View(db.Coahes.OrderByDescending(r => r.ID).ToList());
        }

        // GET: Coahes/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Coahe coahe = db.Coahes.Find(id);
            if (coahe == null)
            {
                return HttpNotFound();
            }
            return View(coahe);
        }

        // GET: Coahes/Create
        public ActionResult Create()
        {
            var Clubs = db.Clubs.Select(c => new SelectListItem() { Text = c.NameA, Value = c.ID.ToString() }).ToList();
            ViewBag.Clubs = Clubs;
            return View(new CoaheModel());
        }

        // POST: Coahes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CoaheModel coahe, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                coahe.ImageUrl = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                Coahe coahes = new Coahe()
                {
                    TitleA = coahe.TitleA,
                    TitleE = coahe.TitleE,
                    DescriptionA = coahe.DescriptionA,
                    DescriptionE = coahe.DescriptionE,
                    ImageUrl = coahe.ImageUrl,
                    Age = coahe.Age,
                    ClubID = coahe.ClubID,
                    Category = coahe.Category,
                    NameA = coahe.NameA,
                    NameE = coahe.NameE
                };
                db.Coahes.Add(coahes);
                db.SaveChanges();

                //********* Start Email Notification *********//
                SendEmail email = new SendEmail();
                string clubName = db.Clubs.Where(x => x.ID == coahe.ClubID).FirstOrDefault().NameA;
                string EmailSubject = $"عاجل: مدرب جديد لنادى {clubName}";
                string Category = "المدربين";
                string imgUrl = "http://yaken.cloudapp.net/EBBFEDSiteTest/assets/images/logo-1.png";
                string shortBrief = $"قام نادى {clubName} بتعيين المدرب {coahes.NameA}";
                string LongDescription = $"{coahes.DescriptionA}";
                email.Send(EmailSubject, Category, EmailSubject, imgUrl, "", shortBrief, LongDescription);
                //********* End Email Notification *********//

                return RedirectToAction("Index");
            }

            return View(coahe);
        }

        // GET: Coahes/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Coahe coahe = db.Coahes.Find(id);
            if (coahe == null)
            {
                return HttpNotFound();
            }
            var Clubs = db.Clubs.Select(c => new SelectListItem() { Text = c.NameA, Value = c.ID.ToString() }).ToList();
            ViewBag.Clubs = Clubs;
            CoaheModel coahes = new CoaheModel()
            {
                TitleA = coahe.TitleA,
                TitleE = coahe.TitleE,
                DescriptionA = coahe.DescriptionA,
                DescriptionE = coahe.DescriptionE,
                ImageUrl = coahe.ImageUrl,
                Age = coahe.Age ?? 0,
                ClubID = coahe.ClubID,
                Category = coahe.Category,
                NameA = coahe.NameA,
                NameE = coahe.NameE
            };
            return View(coahes);
        }

        // POST: Coahes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CoaheModel coahe, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                coahe.ImageUrl = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                Coahe coahes = new Coahe()
                {
                    ID = coahe.ID,
                    TitleA = coahe.TitleA,
                    TitleE = coahe.TitleE,
                    DescriptionA = coahe.DescriptionA,
                    DescriptionE = coahe.DescriptionE,
                    ImageUrl = coahe.ImageUrl,
                    Age = coahe.Age,
                    ClubID = coahe.ClubID,
                    Category = coahe.Category,
                    NameA = coahe.NameA,
                    NameE = coahe.NameE
                };
                db.Entry(coahes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(coahe);
        }

        // GET: Coahes/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Coahe coahe = db.Coahes.Find(id);
            if (coahe == null)
            {
                return HttpNotFound();
            }
            return View(coahe);
        }

        // POST: Coahes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Coahe coahe = db.Coahes.Find(id);
            db.Coahes.Remove(coahe);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //Import Excel into DB
        public ActionResult Upload(FormCollection formCollection)
        {
            var coaches = new List<Coahe>();
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["UploadedFile"];
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                    using (var package = new ExcelPackage(file.InputStream))
                    {
                        var currentSheet = package.Workbook.Worksheets;
                        var workSheet = currentSheet.First();
                        var noOfCol = workSheet.Dimension.End.Column;
                        var noOfRow = workSheet.Dimension.End.Row;
                        var clubs = db.Clubs.ToList();

                        for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                        {
                            var coahe = new Coahe();

                            coahe.NameA = workSheet.Cells[rowIterator, 1].Value == null ? "" : workSheet.Cells[rowIterator, 1].Value.ToString();
                            coahe.NameE = workSheet.Cells[rowIterator, 2].Value == null ? "" : workSheet.Cells[rowIterator, 2].Value.ToString();
                            coahe.ImageUrl = workSheet.Cells[rowIterator, 3].Value == null ? "" : workSheet.Cells[rowIterator, 3].Value.ToString();
                            if (workSheet.Cells[rowIterator, 4].Value != null)
                            {
                                coahe.Age = Convert.ToInt32(workSheet.Cells[rowIterator, 4].Value);
                            }
                            coahe.Category = workSheet.Cells[rowIterator, 5].Value == null ? "" : workSheet.Cells[rowIterator, 4].Value.ToString();
                            coahe.DescriptionA = workSheet.Cells[rowIterator, 6].Value == null ? "" : workSheet.Cells[rowIterator, 5].Value.ToString();
                            coahe.DescriptionE = workSheet.Cells[rowIterator, 7].Value == null ? "" : workSheet.Cells[rowIterator, 6].Value.ToString();
                            coahe.TitleA = workSheet.Cells[rowIterator, 8].Value == null ? "" : workSheet.Cells[rowIterator, 6].Value.ToString();
                            coahe.TitleE = workSheet.Cells[rowIterator, 9].Value == null ? "" : workSheet.Cells[rowIterator, 6].Value.ToString();

                            if (workSheet.Cells[rowIterator, 10].Value != null)
                            {
                                for (int i = 0; i < clubs.Count(); i++)
                                {
                                    var clubID = clubs.FirstOrDefault(a => (a.NameA != null && a.NameA.ToLower() == workSheet.Cells[rowIterator, 10].Value.ToString().ToLower()) || (a.NameE != null && a.NameE.ToLower() == workSheet.Cells[rowIterator, 10].Value.ToString().ToLower()));
                                    if (clubID != null)
                                    {
                                        coahe.ClubID = clubID.ID;
                                    }
                                }
                            }
                            coaches.Add(coahe);
                        }
                    }
                }
            }
            using (db)
            {
                foreach (var item in coaches)
                {
                    db.Coahes.Add(item);
                }
                db.SaveChanges();
                TempData["success"] = true;
                return RedirectToAction("Index", db.Coahes.ToList());
            }
        }

        //Export Excel into DB
        [HttpPost]
        public void Download()
        {
            var coahes = db.Coahes.ToList();
            var clubs = db.Clubs.ToList();

            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
            Sheet.Row(1).Height = 30;
            Sheet.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            Sheet.Row(1).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

            Sheet.Cells["A1"].Value = "NameA";
            Sheet.Cells["B1"].Value = "NameE";
            Sheet.Cells["C1"].Value = "ImageUrl";
            Sheet.Cells["D1"].Value = "Age";
            Sheet.Cells["E1"].Value = "Category";
            Sheet.Cells["F1"].Value = "DescriptionA";
            Sheet.Cells["G1"].Value = "DescriptionE";
            Sheet.Cells["H1"].Value = "TitleA";
            Sheet.Cells["I1"].Value = "TitleE";
            Sheet.Cells["J1"].Value = "Club";


            int row = 2;
            foreach (var item in coahes)
            {
                Sheet.Cells[string.Format("A{0}", row)].Value = item.NameA;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.NameE;
                Sheet.Cells[string.Format("C{0}", row)].Value = item.ImageUrl;
                Sheet.Cells[string.Format("D{0}", row)].Value = item.Age;
                Sheet.Cells[string.Format("E{0}", row)].Value = item.Category;
                Sheet.Cells[string.Format("F{0}", row)].Value = item.DescriptionA;
                Sheet.Cells[string.Format("G{0}", row)].Value = item.DescriptionE;
                Sheet.Cells[string.Format("H{0}", row)].Value = item.TitleA;
                Sheet.Cells[string.Format("I{0}", row)].Value = item.TitleE;

                for (int x = 0; x < clubs.Count(); x++)
                {
                    if (item.ClubID == clubs[x].ID)
                    {
                        Sheet.Cells[string.Format("J{0}", row)].Value = clubs[x].NameA;
                    }
                }
                row++;
            }
            var headerCells = Sheet.Cells[1, 1, 1, 10];
            var headerFont = headerCells.Style.Font;
            headerFont.SetFromFont(new Font("Arial", 11, FontStyle.Bold));
            Sheet.Cells["A:AZ"].AutoFitColumns();

            string excelName = "Coaches";
            using (var memoryStream = new MemoryStream())
            {
                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
                Ep.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
        }
    }
}
