﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class HomePagesController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: HomePages
        public ActionResult Index()
        {
            return View(db.HomePages.OrderByDescending(r => r.Id).ToList());
        }

        // GET: HomePages/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HomePages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HttpPostedFileBase upload)
        {
            if (upload!=null)
            {
                HomePage homePage = new HomePage();
                string imgName = Guid.NewGuid()+"_"+upload.FileName;
                string imgPath = Path.Combine(Server.MapPath("~/Content/Images/"), imgName);
                upload.SaveAs(imgPath);
                homePage.ImageUrl = imgName;
                db.HomePages.Add(homePage);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View();
        }

        // GET: HomePages/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HomePage homePage = db.HomePages.Find(id);
            if (homePage == null)
            {
                return HttpNotFound();
            }
            return View(homePage);
        }

        // POST: HomePages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id")] HomePage homePage,HttpPostedFileBase upload)
        {
            if (upload!=null)
            {
                HomePage home = db.HomePages.Find(homePage.Id);

                string oldImgName = home.ImageUrl;
                string oldImgPath = Path.Combine(Server.MapPath("~/Content/Images/"), oldImgName);
                if (System.IO.File.Exists(oldImgPath))
                {
                    System.IO.File.Delete(oldImgPath);
                }
                string imgName = Guid.NewGuid() + "_" + upload.FileName;
                string imgPath = Path.Combine(Server.MapPath("~/Content/Images/"), imgName);
                upload.SaveAs(imgPath);
                home.ImageUrl = imgName;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(homePage);
        }

        // GET: HomePages/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HomePage homePage = db.HomePages.Find(id);
            if (homePage == null)
            {
                return HttpNotFound();
            }
            return View(homePage);
        }

        // POST: HomePages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            HomePage homePage = db.HomePages.Find(id);
            db.HomePages.Remove(homePage);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
