﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;
using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class ClubsController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: Clubs
        public ActionResult Index()
        {
            var clubs = db.Clubs.Include(c => c.Division);
            return View(clubs.OrderByDescending(r => r.ID).ToList());
        }

        // GET: Clubs/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Club club = db.Clubs.Find(id);
            if (club == null)
            {
                return HttpNotFound();
            }
            return View(club);
        }

        // GET: Clubs/Create
        public ActionResult Create()
        {
            ViewBag.DivisionID = new SelectList(db.Divisions, "ID", "NameA");
            ViewBag.AreaID = new SelectList(db.Areas, "ID", "NameA");
            return View(new ClubsModel());
        }

        // POST: Clubs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ClubsModel club, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                club.Logo = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                Club clubs = new Club()
                {
                    Logo=club.Logo,
                    NameA=club.NameA,
                    NameE=club.NameE,
                    DescriptionA=club.DescriptionA,
                    DescriptionE=club.DescriptionE,
                    DivisionID=club.DivisionID,
                    AreaID=(int)club.AreaID,
                    EstablishedYear=club.EstablishedYear,
                    TitlesWon=club.TitlesWon
                };
                db.Clubs.Add(clubs);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DivisionID = new SelectList(db.Divisions, "ID", "NameA", club.DivisionID);
            ViewBag.AreaID = new SelectList(db.Areas, "ID", "NameA");

            return View(club);
        }

        // GET: Clubs/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Club club = db.Clubs.Find(id);
            if (club == null)
            {
                return HttpNotFound();
            }
            ViewBag.DivisionID = new SelectList(db.Divisions, "ID", "NameA", club.DivisionID);
            ViewBag.AreaID = new SelectList(db.Areas, "ID", "NameA", club.AreaID ?? 0);

            ClubsModel clubs = new ClubsModel()
            {
                Logo = club.Logo,
                NameA = club.NameA,
                NameE = club.NameE,
                DescriptionA = club.DescriptionA,
                DescriptionE = club.DescriptionE,
                DivisionID = club.DivisionID,
                AreaID = club.AreaID ?? 0,
                EstablishedYear = club.EstablishedYear??0,
                TitlesWon = club.TitlesWon
            };
            return View(clubs);
        }

        // POST: Clubs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ClubsModel club, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                club.Logo = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                Club clubs = new Club()
                {
                    ID=club.ID,
                    Logo = club.Logo,
                    NameA = club.NameA,
                    NameE = club.NameE,
                    DescriptionA = club.DescriptionA,
                    DescriptionE = club.DescriptionE,
                    DivisionID = club.DivisionID,
                    AreaID = (int)club.AreaID,
                    EstablishedYear = club.EstablishedYear,
                    TitlesWon = club.TitlesWon
                };
                db.Entry(clubs).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DivisionID = new SelectList(db.Divisions, "ID", "NameA", club.DivisionID);
            ViewBag.AreaID = new SelectList(db.Areas, "ID", "NameA", club.AreaID);
            return View(club);
        }

        // GET: Clubs/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Club club = db.Clubs.Find(id);
            if (club == null)
            {
                return HttpNotFound();
            }
            return View(club);
        }

        // POST: Clubs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Club club = db.Clubs.Find(id);
            db.Clubs.Remove(club);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
