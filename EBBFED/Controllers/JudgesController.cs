﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;

using static EBBFED.FilterConfig;
using OfficeOpenXml;
using System.Drawing;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class JudgesController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: Judges
        public ActionResult Index()
        {
            return View(db.Judges.OrderByDescending(r => r.ID).ToList());
        }

        // GET: Judges/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Judge judge = db.Judges.Find(id);
            if (judge == null)
            {
                return HttpNotFound();
            }
            return View(judge);
        }

        // GET: Judges/Create
        public ActionResult Create()
        {
            ViewBag.AreaID = new SelectList(db.Areas, "ID", "NameA");
            return View(new JudgesModel());
        }

        // POST: Judges/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(JudgesModel judge, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                judge.ImageUrl = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                Judge judges = new Judge()
                {
                    NameA = judge.NameA,
                    NameE = judge.NameE,
                    Category = judge.Category,
                    DescriptionA = judge.DescriptionA,
                    DescriptionE = judge.DescriptionE,
                    ImageUrl = judge.ImageUrl,
                    AreaID = (int)judge.AreaID
                };
                db.Judges.Add(judges);
                db.SaveChanges();

                //********* Start Email Notification *********//
                SendEmail email = new SendEmail();
                string AreaName = db.Areas.Where(x => x.ID == judges.AreaID).FirstOrDefault().NameA;
                string EmailSubject = "عاجل: حكم جديد فى الاتحاد المصرى لكره السله";
                string Category = "الحكام";
                string imgUrl = "http://yaken.cloudapp.net/EBBFEDSiteTest/assets/images/logo-1.png";
                string shortBrief = $"قام الاتحاد المصرى لكره السله بتعيين حكما جديدا للمباريات ";
                string LongDescription = $"قام الاتحاد المصرى لكره السله بتعيين الحكم ال{judges.Category} {judges.NameA} فى منطقه {AreaName} <br> وهو {judges.DescriptionA}";
                email.Send(EmailSubject, Category, EmailSubject, imgUrl, "", shortBrief, LongDescription);
                //********* End Email Notification *********//


                return RedirectToAction("Index");
            }

            ViewBag.AreaID = new SelectList(db.Areas, "ID", "NameA");

            return View(judge);
        }

        // GET: Judges/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Judge judge = db.Judges.Find(id);
            if (judge == null)
            {
                return HttpNotFound();
            }
            ViewBag.AreaID = new SelectList(db.Areas, "ID", "NameA");

            JudgesModel judges = new JudgesModel()
            {
                ID = judge.ID,
                NameA = judge.NameA,
                NameE = judge.NameE,
                Category = judge.Category,
                DescriptionA = judge.DescriptionA,
                DescriptionE = judge.DescriptionE,
                ImageUrl = judge.ImageUrl,
                AreaID = judge.AreaID ?? 0,

            };
            return View(judge);
        }

        // POST: Judges/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(JudgesModel judge, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                judge.ImageUrl = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                Judge judges = new Judge()
                {
                    ID = judge.ID,
                    NameA = judge.NameA,
                    NameE = judge.NameE,
                    Category = judge.Category,
                    DescriptionA = judge.DescriptionA,
                    DescriptionE = judge.DescriptionE,
                    ImageUrl = judge.ImageUrl,
                    AreaID = (int)judge.AreaID,

                };
                db.Entry(judges).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AreaID = new SelectList(db.Areas, "ID", "NameA");

            return View(judge);
        }

        // GET: Judges/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Judge judge = db.Judges.Find(id);
            if (judge == null)
            {
                return HttpNotFound();
            }
            return View(judge);
        }

        // POST: Judges/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Judge judge = db.Judges.Find(id);
            db.Judges.Remove(judge);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //Import Excel into DB
        public ActionResult Upload(FormCollection formCollection)
        {
            var judges = new List<Judge>();
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["UploadedFile"];
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                    using (var package = new ExcelPackage(file.InputStream))
                    {
                        var currentSheet = package.Workbook.Worksheets;
                        var workSheet = currentSheet.First();
                        var noOfCol = workSheet.Dimension.End.Column;
                        var noOfRow = workSheet.Dimension.End.Row;
                        var areas = db.Areas.ToList();
                        for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                        {
                            var judge = new Judge();

                            judge.NameA = workSheet.Cells[rowIterator, 1].Value == null ? "": workSheet.Cells[rowIterator, 1].Value.ToString();
                            judge.NameE = workSheet.Cells[rowIterator, 2].Value== null ? "" : workSheet.Cells[rowIterator, 2].Value.ToString();
                            judge.ImageUrl = workSheet.Cells[rowIterator, 3].Value== null ? "" : workSheet.Cells[rowIterator, 3].Value.ToString();
                            judge.Category = workSheet.Cells[rowIterator, 4].Value== null ? "" : workSheet.Cells[rowIterator, 4].Value.ToString();
                            judge.DescriptionA = workSheet.Cells[rowIterator, 5].Value== null ? "" : workSheet.Cells[rowIterator, 5].Value.ToString();
                            judge.DescriptionE = workSheet.Cells[rowIterator, 6].Value== null ? "" : workSheet.Cells[rowIterator, 6].Value.ToString();

                            if (workSheet.Cells[rowIterator, 7].Value!=null)
                            {
                                for (int i = 0; i < areas.Count(); i++)
                                {
                                    var areaID = areas.FirstOrDefault(a => a.NameA.ToLower() == workSheet.Cells[rowIterator, 7].Value.ToString().ToLower() || a.NameE.ToLower() == workSheet.Cells[rowIterator, 7].Value.ToString().ToLower());
                                    if (areaID!=null)
                                    {
                                        judge.AreaID = areaID.ID;
                                    }
                                }
                            }
                            judges.Add(judge);
                        }
                    }
                }
            }
            using (db)
            {
                foreach (var item in judges)
                {
                    db.Judges.Add(item);
                }
                db.SaveChanges();
                TempData["success"] = true;
                return RedirectToAction("Index", db.Judges.ToList());
            }
        }

        //Export Excel into DB
        [HttpPost]
        public void Download()
        {
            var Judges = db.Judges.ToList();
            var Areas = db.Areas.ToList();

            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
            Sheet.Row(1).Height = 30;
            Sheet.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            Sheet.Row(1).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

            Sheet.Cells["A1"].Value = "NameA";
            Sheet.Cells["B1"].Value = "NameE";
            Sheet.Cells["C1"].Value = "Category";
            Sheet.Cells["D1"].Value = "DescriptionA";
            Sheet.Cells["E1"].Value = "DescriptionE";
            Sheet.Cells["F1"].Value = "AreaID";

            int row = 2;
            foreach (var item in Judges)
            {
                Sheet.Cells[string.Format("A{0}", row)].Value = item.NameA;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.NameE;
                Sheet.Cells[string.Format("C{0}", row)].Value = item.Category;
                Sheet.Cells[string.Format("D{0}", row)].Value = item.DescriptionA;
                Sheet.Cells[string.Format("E{0}", row)].Value = item.DescriptionE;

                for (int x = 0; x < Areas.Count(); x++)
                {
                    if (item.AreaID == Areas[x].ID)
                    {
                        Sheet.Cells[string.Format("F{0}", row)].Value = Areas[x].NameA;
                    }
                }
                row++;
            }
            var headerCells = Sheet.Cells[1, 1, 1, 6];
            var headerFont = headerCells.Style.Font;
            headerFont.SetFromFont(new Font("Arial", 11, FontStyle.Bold));
            Sheet.Cells["A:AZ"].AutoFitColumns();

            string excelName = "Judges";
            using (var memoryStream = new MemoryStream())
            {
                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
                Ep.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
        }
    }
}
