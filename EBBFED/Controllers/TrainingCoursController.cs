﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;
using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class TrainingCoursController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: TrainingCours
        public ActionResult Index()
        {
            return View(db.TrainingCourses.OrderByDescending(r => r.ID).ToList());
        }

        // GET: TrainingCours/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainingCours trainingCours = db.TrainingCourses.Find(id);
            if (trainingCours == null)
            {
                return HttpNotFound();
            }
            return View(trainingCours);
        }

        // GET: TrainingCours/Create
        public ActionResult Create()
        {
            List<SelectListItem> CourseType = new List<SelectListItem> {
                                             new SelectListItem{ Text="حكام",   Value = "1"},
                                             new SelectListItem{ Text="مدربين", Value = "2"}
                                             };
            ViewBag.CourseTypes = CourseType;
            return View(new TrainingCoursModel());
        }

        // POST: TrainingCours/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TrainingCoursModel trainingCours)
        {
            if (ModelState.IsValid)
            {
                TrainingCours ts = new TrainingCours()
                {
                    TitleA=trainingCours.TitleA,
                    TitleE=trainingCours.TitleE,
                    ShortDescriptionA=trainingCours.ShortDescriptionA,
                    ShortDescriptionE=trainingCours.ShortDescriptionE,
                    LongDescriptionA=trainingCours.LongDescriptionA,
                    LongDescriptionE=trainingCours.LongDescriptionE,
                    Location=trainingCours.Location,
                    Date=trainingCours.Date,
                    CourseType=trainingCours.CourseType=="1"? "حكام" : "مدربين"
                };
                db.TrainingCourses.Add(ts);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            List<SelectListItem> CourseType = new List<SelectListItem> {
                                             new SelectListItem{ Text="حكام",   Value = "1"},
                                             new SelectListItem{ Text="مدربين", Value = "2"}
                                             };
            ViewBag.CourseTypes = CourseType;

            return View(trainingCours);
        }

        // GET: TrainingCours/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainingCours trainingCours = db.TrainingCourses.Find(id);
            if (trainingCours == null)
            {
                return HttpNotFound();
            }
            TrainingCoursModel ts = new TrainingCoursModel()
            {
                TitleA = trainingCours.TitleA,
                TitleE = trainingCours.TitleE,
                ShortDescriptionA = trainingCours.ShortDescriptionA,
                ShortDescriptionE = trainingCours.ShortDescriptionE,
                LongDescriptionA = trainingCours.LongDescriptionA,
                LongDescriptionE = trainingCours.LongDescriptionE,
                Location = trainingCours.Location,
                Date = (DateTime)trainingCours.Date,
                CourseType = trainingCours.CourseType == "حكام" ? "1" : "2"
            };
            List<SelectListItem> CourseType = new List<SelectListItem> {
                                             new SelectListItem{ Text="حكام",   Value = "1"},
                                             new SelectListItem{ Text="مدربين", Value = "2"}
                                             };
            ViewBag.CourseTypes = CourseType;
            return View(ts);
        }

        // POST: TrainingCours/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TrainingCoursModel trainingCours)
        {
            if (ModelState.IsValid)
            {
                TrainingCours ts = new TrainingCours()
                {
                    ID=trainingCours.ID,
                    TitleA = trainingCours.TitleA,
                    TitleE = trainingCours.TitleE,
                    ShortDescriptionA = trainingCours.ShortDescriptionA,
                    ShortDescriptionE = trainingCours.ShortDescriptionE,
                    LongDescriptionA = trainingCours.LongDescriptionA,
                    LongDescriptionE = trainingCours.LongDescriptionE,
                    Location = trainingCours.Location,
                    Date = trainingCours.Date,
                    CourseType = trainingCours.CourseType == "1" ? "حكام" : "مدربين"
                };
                db.Entry(ts).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            List<SelectListItem> CourseType = new List<SelectListItem> {
                                             new SelectListItem{ Text="حكام",   Value = "1"},
                                             new SelectListItem{ Text="مدربين", Value = "2"}
                                             };
            ViewBag.CourseTypes = CourseType;
            return View(trainingCours);
        }

        // GET: TrainingCours/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainingCours trainingCours = db.TrainingCourses.Find(id);
            if (trainingCours == null)
            {
                return HttpNotFound();
            }
            return View(trainingCours);
        }

        // POST: TrainingCours/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            TrainingCours trainingCours = db.TrainingCourses.Find(id);
            db.TrainingCourses.Remove(trainingCours);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
