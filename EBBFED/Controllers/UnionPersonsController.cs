﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;
using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class UnionPersonsController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: UnionPersons
        public ActionResult Index()
        {
            return View(db.UnionPersons.OrderByDescending(r => r.ID).ToList());
        }

        // GET: UnionPersons/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UnionPerson unionPerson = db.UnionPersons.Find(id);
            if (unionPerson == null)
            {
                return HttpNotFound();
            }
            return View(unionPerson);
        }

        // GET: UnionPersons/Create
        public ActionResult Create()
        {
            return View(new UnionPersonModel());
        }

        // POST: UnionPersons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UnionPersonModel unionPerson, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                unionPerson.ImageUrl = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                UnionPerson up = new UnionPerson()
                {
                    BriefA=unionPerson.BriefA,
                    BriefE=unionPerson.BriefE,
                    ImageUrl=unionPerson.ImageUrl,
                    NameA=unionPerson.NameA,
                    NameE=unionPerson.NameE
                };
                db.UnionPersons.Add(up);
                db.SaveChanges();

                //********* Start Email Notification *********//
                SendEmail email = new SendEmail();
                string EmailSubject = "عاجل: تغيير جديد فى مجلس الاتحاد المصرى لكره السله ";
                string Category = "مجلس الاتحاد المصرى لكره السله";
                string imgUrl = "http://yaken.cloudapp.net/EBBFEDSiteTest/assets/images/logo-1.png";
                string shortBrief = "قام الاتحاد المصرى لكره السله بتعديل جديد فى مجلس الاتحاد حيث قام بتعيين السيد " + up.NameA;
                string LongDescription = "قام الاتحاد المصرى لكره السله بتعديل جديد فى مجلس الاتحاد حيث قام بتعيين السيد " + up.NameA + "<br>- "+up.BriefA;
                email.Send(EmailSubject, Category,EmailSubject, imgUrl, "", shortBrief, LongDescription);
                //********* End Email Notification *********//

                return RedirectToAction("Index");
            }
            return View(unionPerson);
        }

        // GET: UnionPersons/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UnionPerson unionPerson = db.UnionPersons.Find(id);
            if (unionPerson == null)
            {
                return HttpNotFound();
            }
            UnionPersonModel up = new UnionPersonModel()
            {
                ID=unionPerson.ID,
                BriefA = unionPerson.BriefA,
                BriefE = unionPerson.BriefE,
                ImageUrl = unionPerson.ImageUrl,
                NameA = unionPerson.NameA,
                NameE = unionPerson.NameE
            };
            return View(up);
        }

        // POST: UnionPersons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UnionPersonModel unionPerson, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                unionPerson.ImageUrl = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                UnionPerson up = new UnionPerson()
                {
                    ID = unionPerson.ID,
                    BriefA = unionPerson.BriefA,
                    BriefE = unionPerson.BriefE,
                    ImageUrl = unionPerson.ImageUrl,
                    NameA = unionPerson.NameA,
                    NameE = unionPerson.NameE
                };
                db.Entry(up).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(unionPerson);
        }

        // GET: UnionPersons/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UnionPerson unionPerson = db.UnionPersons.Find(id);
            if (unionPerson == null)
            {
                return HttpNotFound();
            }
            return View(unionPerson);
        }

        // POST: UnionPersons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            UnionPerson unionPerson = db.UnionPersons.Find(id);
            db.UnionPersons.Remove(unionPerson);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
