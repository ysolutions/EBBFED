﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;

using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class CompetitionTeamsController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: CompetitionTeams
        public ActionResult Index()
        {
            var competitionTeams = db.CompetitionTeams;
            return View(competitionTeams.OrderByDescending(r => r.ID).ToList());
        }

        // GET: CompetitionTeams/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompetitionTeam competitionTeam = db.CompetitionTeams.Find(id);
            if (competitionTeam == null)
            {
                return HttpNotFound();
            }
            return View(competitionTeam);
        }

        // GET: CompetitionTeams/Create
        public ActionResult Create()
        {
            ViewBag.CompetitionID = new SelectList(db.Competitions, "ID", "NameA");
            return View();
        }

        // POST: CompetitionTeams/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CompetitionTeam competitionTeam, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                competitionTeam.Logo = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                db.CompetitionTeams.Add(competitionTeam);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CompetitionID = new SelectList(db.Competitions, "ID", "NameA");
            return View(competitionTeam);
        }

        // GET: CompetitionTeams/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompetitionTeam competitionTeam = db.CompetitionTeams.Find(id);
            if (competitionTeam == null)
            {
                return HttpNotFound();
            }
            ViewBag.CompetitionID = new SelectList(db.Competitions, "ID", "NameA");
            return View(competitionTeam);
        }

        // POST: CompetitionTeams/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CompetitionTeam competitionTeam, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                competitionTeam.Logo = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                db.Entry(competitionTeam).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CompetitionID = new SelectList(db.Competitions, "ID", "NameA");
            return View(competitionTeam);
        }

        // GET: CompetitionTeams/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompetitionTeam competitionTeam = db.CompetitionTeams.Find(id);
            if (competitionTeam == null)
            {
                return HttpNotFound();
            }
            return View(competitionTeam);
        }

        // POST: CompetitionTeams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            CompetitionTeam competitionTeam = db.CompetitionTeams.Find(id);
            db.CompetitionTeams.Remove(competitionTeam);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
