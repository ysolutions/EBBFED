﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;

using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class ContactUsController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: ContactUs
        public ActionResult Index()
        {
            ContactUs contactInfo = db.ContactUs.Find(1);
            if (contactInfo == null)
            {
                ContactUs initialContactInfo = new ContactUs()
                {
                    ID = 1,
                    PhoneNumber = "Enter PhoneNumber Here.",
                    Address = "Enter Address Here.",
                    Email = "Enter Email Here.",
                    LocationLong = "Enter LocationLong Here.",
                    LocationLat = "Enter LocationLat Here.",
                    WorkingHours = "Enter Working Hours Here.",
                    FacebookURL = "",
                    TwitterURL = "",
                    InstagramURL = "",
                    YoutubeURL = ""
                };
                db.ContactUs.Add(initialContactInfo);
                db.SaveChanges();

                contactInfo = db.ContactUs.Find(1);
            }
            return View(contactInfo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "ID,PhoneNumber,Address,Email,LocationLong,LocationLat,WorkingHours,FacebookURL,TwitterURL,InstagramURL,YoutubeURL")] ContactUs contactUs)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(contactUs).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(contactUs);
            }
            catch (Exception ex)
            {
                ViewData["ErrorDetails"] = ex;
                return View("Error");
            }
        }

        // GET: ContactUs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactUs contactUs = db.ContactUs.Find(id);
            if (contactUs == null)
            {
                return HttpNotFound();
            }
            return View(contactUs);
        }

        // GET: ContactUs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ContactUs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,PhoneNumber,Address,Email,LocationLong,LocationLat,WorkingHours")] ContactUs contactUs)
        {
            if (ModelState.IsValid)
            {
                db.ContactUs.Add(contactUs);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(contactUs);
        }

        // GET: ContactUs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactUs contactUs = db.ContactUs.Find(id);
            if (contactUs == null)
            {
                return HttpNotFound();
            }
            return View(contactUs);
        }

        // POST: ContactUs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,PhoneNumber,Address,Email,LocationLong,LocationLat,WorkingHours")] ContactUs contactUs)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contactUs).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(contactUs);
        }

        // GET: ContactUs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactUs contactUs = db.ContactUs.Find(id);
            if (contactUs == null)
            {
                return HttpNotFound();
            }
            return View(contactUs);
        }

        // POST: ContactUs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ContactUs contactUs = db.ContactUs.Find(id);
            db.ContactUs.Remove(contactUs);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
