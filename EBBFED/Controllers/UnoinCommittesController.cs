﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;
using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class UnoinCommittesController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: UnoinCommittes
        public ActionResult Index()
        {
            return View(db.UnoinCommittes.OrderByDescending(r => r.ID).ToList());
        }

        // GET: UnoinCommittes/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UnoinCommitte unoinCommitte = db.UnoinCommittes.Find(id);
            if (unoinCommitte == null)
            {
                return HttpNotFound();
            }
            return View(unoinCommitte);
        }

        // GET: UnoinCommittes/Create
        public ActionResult Create()
        {
            return View(new UnoinCommitteModel());
        }

        // POST: UnoinCommittes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UnoinCommitteModel unoinCommitte, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                unoinCommitte.ImageUrl = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                UnoinCommitte un = new UnoinCommitte()
                {
                    ImageUrl = unoinCommitte.ImageUrl,
                    NameA = unoinCommitte.NameA,
                    NameE = unoinCommitte.NameE,
                    ObjectivesA = unoinCommitte.ObjectivesA,
                    ObjectivesE = unoinCommitte.ObjectivesE,
                };
                db.UnoinCommittes.Add(un);
                for (int i = 0; i < unoinCommitte.MemberNameA.Count; i++)
                {
                    if (unoinCommitte.MemberNameA[i].Length == 0 || unoinCommitte.MemberNameE[i].Length == 0)
                        continue;
                    ComitteMember cm = new ComitteMember()
                    {
                        NameA = unoinCommitte.MemberNameA[i],
                        NameE = unoinCommitte.MemberNameE[i],
                        CommitteID = un.ID
                    };
                    db.ComitteMembers.Add(cm);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(unoinCommitte);
        }

        // GET: UnoinCommittes/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UnoinCommitte unoinCommitte = db.UnoinCommittes.Find(id);
            if (unoinCommitte == null)
            {
                return HttpNotFound();
            }
            List<string> MemberNameAs= db.ComitteMembers.Where(t => t.CommitteID == id).Select(t => t.NameA).ToList();
            List<string> MemberNameEs = db.ComitteMembers.Where(t => t.CommitteID == id).Select(t => t.NameE).ToList();
            UnoinCommitteModel un = new UnoinCommitteModel()
            {
                ID = unoinCommitte.ID,
                ImageUrl = unoinCommitte.ImageUrl,
                NameA = unoinCommitte.NameA,
                NameE = unoinCommitte.NameE,
                ObjectivesA = unoinCommitte.ObjectivesA,
                ObjectivesE = unoinCommitte.ObjectivesE,
                MemberNameA= MemberNameAs,
                MemberNameE= MemberNameEs,
            };
            return View(un);
        }

        // POST: UnoinCommittes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UnoinCommitteModel unoinCommitte, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                unoinCommitte.ImageUrl = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                UnoinCommitte un = new UnoinCommitte()
                {
                    ID = unoinCommitte.ID,
                    ImageUrl = unoinCommitte.ImageUrl,
                    NameA = unoinCommitte.NameA,
                    NameE = unoinCommitte.NameE,
                    ObjectivesA = unoinCommitte.ObjectivesA,
                    ObjectivesE = unoinCommitte.ObjectivesE,
                };
                List<long> MemberIDs = db.ComitteMembers.Where(t => t.CommitteID == unoinCommitte.ID).Select(t => t.ID).ToList();
                for (int i = 0; i < unoinCommitte.MemberNameA.Count; i++)
                {
                    var member = db.ComitteMembers.Find(MemberIDs[i]);
                    member.NameA = unoinCommitte.MemberNameA[i];
                    member.NameE = unoinCommitte.MemberNameE[i];
                    db.Entry(member).State = EntityState.Modified;
                }
                for (int i = 0; i < unoinCommitte.MemberNameAe.Count; i++)
                {
                    if (unoinCommitte.MemberNameAe[i].Length == 0 || unoinCommitte.MemberNameEe[i].Length == 0)
                        continue;
                    ComitteMember cm = new ComitteMember()
                    {
                        NameA = unoinCommitte.MemberNameAe[i],
                        NameE = unoinCommitte.MemberNameEe[i],
                        CommitteID = un.ID
                    };
                    db.ComitteMembers.Add(cm);
                }
                db.Entry(un).State = EntityState.Modified;
                db.SaveChanges();
                /////////////////////////////
                string mailSubject = "تعديل فى مجلس الاتحاد";
                string mailBody = $"<h2>{un.NameA}</h2><br /><h3>{un.ObjectivesA}</h3>";
                UserNotificationModel.SendEmail(mailSubject, mailBody);
                return RedirectToAction("Index");
            }
            return View(unoinCommitte);
        }

        // GET: UnoinCommittes/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UnoinCommitte unoinCommitte = db.UnoinCommittes.Find(id);
            if (unoinCommitte == null)
            {
                return HttpNotFound();
            }
            return View(unoinCommitte);
        }

        // POST: UnoinCommittes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            UnoinCommitte unoinCommitte = db.UnoinCommittes.Find(id);
            db.UnoinCommittes.Remove(unoinCommitte);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
