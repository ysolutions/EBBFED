﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;

using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class NationalCompetitionsController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: NationalCompetitions
        public ActionResult Index()
        {
            return View(db.NationalCompetitions.OrderByDescending(r => r.ID).ToList());
        }

        // GET: NationalCompetitions/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NationalCompetition nationalCompetition = db.NationalCompetitions.Find(id);
            if (nationalCompetition == null)
            {
                return HttpNotFound();
            }
            return View(nationalCompetition);
        }

        // GET: NationalCompetitions/Create
        public ActionResult Create()
        {
            List<Gender> genderList = new List<Gender> {
                                             new Gender{ Name="رجال",   ID = 1},
                                             new Gender{ Name="سيدات", ID = 2}
                                             };
            ViewBag.Genders = new SelectList(genderList, "ID", "Name");
            ViewBag.AllAges = new SelectList(db.Ages, "ID", "NameA");
            return View(new NationalCompetitionModel());
        }

        // POST: NationalCompetitions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NationalCompetitionModel nationalCompetition)
        {
            if (ModelState.IsValid)
            {
                NationalCompetition nc = new NationalCompetition()
                {
                    Date = nationalCompetition.Date,
                    NameA = nationalCompetition.NameA,
                    NameE = nationalCompetition.NameE,
                    Gender = nationalCompetition.Gender == 2 ? "سيدات" : "رجال",
                };
                db.NationalCompetitions.Add(nc);
                db.SaveChanges();
                for (int i = 0; i < nationalCompetition.Ages.Count; i++)
                {
                    NationalAge CA = new NationalAge();
                    CA.AgesID = nationalCompetition.Ages[i];
                    CA.NationaCompetitionID = nc.ID;
                    db.NationalAges.Add(CA);
                }
                db.SaveChanges();

                //********* Start Email Notification *********//
                DateTime date = (DateTime)nc.Date;

                SendEmail email = new SendEmail();
                string EmailSubject = "جديد: اضافه مسابقه جديده";
                string Category = "مسابقات المنتخبات";
                string imgUrl = "http://yaken.cloudapp.net/EBBFEDSiteTest/assets/images/logo-1.png";
                string shortBrief = $"تمت اضافه {nc.NameA}";
                string LongDescription = $"تم اضافه {nc.NameA} المقامه للـ{nc.Gender} بتاريخ {date.ToShortDateString()}";
                email.Send(EmailSubject, Category, EmailSubject, imgUrl, "", shortBrief, LongDescription);
                //********* End Email Notification *********//

                return RedirectToAction("Index");
            }
            ViewBag.AllAges = new SelectList(db.Ages, "ID", "NameA");
            List<Gender> genderList = new List<Gender> {
                                             new Gender{ Name="رجال",   ID = 1},
                                             new Gender{ Name="سيدات", ID = 2}
                                             };
            ViewBag.Genders = new SelectList(genderList, "ID", "Name");
            return View(nationalCompetition);
        }

        // GET: NationalCompetitions/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NationalCompetition nationalCompetition = db.NationalCompetitions.Find(id);
            if (nationalCompetition == null)
            {
                return HttpNotFound();
            }
            List<long> Age = db.NationalAges.Where(n => n.NationaCompetitionID == id).Select(n => n.AgesID).ToList();
            NationalCompetitionModel nc = new NationalCompetitionModel()
            {
                ID = nationalCompetition.ID,
                Date = (DateTime)nationalCompetition.Date,
                NameA = nationalCompetition.NameA,
                NameE = nationalCompetition.NameE,
                Gender = nationalCompetition.Gender == "رجال" ? 1 : 2,
                Ages = Age
            };
            List<Gender> genderList = new List<Gender> {
                                             new Gender{ Name="رجال",   ID = 1},
                                             new Gender{ Name="سيدات", ID = 2}
                                             };
            ViewBag.Genders = new SelectList(genderList, "ID", "Name");
            ViewBag.AllAges = new SelectList(db.Ages, "ID", "NameA");
            return View(nc);
        }

        // POST: NationalCompetitions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(NationalCompetitionModel nationalCompetition)
        {
            if (ModelState.IsValid)
            {
                NationalCompetition nc = new NationalCompetition()
                {
                    ID = nationalCompetition.ID,
                    Date = nationalCompetition.Date,
                    NameA = nationalCompetition.NameA,
                    NameE = nationalCompetition.NameE,
                    Gender = nationalCompetition.Gender == 1 ? "رجال" : "سيدات",
                };
                db.Entry(nc).State = EntityState.Modified;
                db.SaveChanges();
                var nationalAge = db.NationalAges.Where(na => na.NationaCompetitionID == nationalCompetition.ID).ToList();
                foreach (var item in nationalAge)
                {
                    db.NationalAges.Remove(item);
                }
                for (int i = 0; i < nationalCompetition.Ages.Count; i++)
                {
                    NationalAge CA = new NationalAge();
                    CA.AgesID = nationalCompetition.Ages[i];
                    CA.NationaCompetitionID = nc.ID;
                    db.NationalAges.Add(CA);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            List<Gender> genderList = new List<Gender> {
                                             new Gender{ Name="رجال",   ID = 1},
                                             new Gender{ Name="سيدات", ID = 2}
                                             };
            ViewBag.Genders = new SelectList(genderList, "ID", "Name");
            ViewBag.AllAges = new SelectList(db.Ages, "ID", "NameA");
            return View(nationalCompetition);
        }

        // GET: NationalCompetitions/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NationalCompetition nationalCompetition = db.NationalCompetitions.Find(id);
            if (nationalCompetition == null)
            {
                return HttpNotFound();
            }
            return View(nationalCompetition);
        }

        // POST: NationalCompetitions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            NationalCompetition nationalCompetition = db.NationalCompetitions.Find(id);
            db.NationalCompetitions.Remove(nationalCompetition);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
