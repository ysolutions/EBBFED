﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class ArchiveCategoriesController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: ArchiveCategories
        public ActionResult Index()
        {
            var archiveCategories = db.ArchiveCategories.Include(a => a.Archive);
            return View(archiveCategories.OrderByDescending(r => r.ID).ToList());
        }

        // GET: ArchiveCategories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ArchiveCategory archiveCategory = db.ArchiveCategories.Find(id);
            if (archiveCategory == null)
            {
                return HttpNotFound();
            }
            return View(archiveCategory);
        }

        // GET: ArchiveCategories/Create
        public ActionResult Create()
        {
            ViewBag.ArchiveID = new SelectList(db.Archive, "ID", "ID");
            return View();
        }

        // POST: ArchiveCategories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,NameA,NameE,About,ArchiveID")] ArchiveCategory archiveCategory)
        {
            if (ModelState.IsValid)
            {
                db.ArchiveCategories.Add(archiveCategory);
                db.SaveChanges();
                return RedirectToAction("Index", "Archive");
            }

            ViewBag.ArchiveID = new SelectList(db.Archive, "ID", "ID", archiveCategory.ArchiveID);
            return View(archiveCategory);
        }

        // GET: ArchiveCategories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ArchiveCategory archiveCategory = db.ArchiveCategories.Find(id);
            if (archiveCategory == null)
            {
                return HttpNotFound();
            }
            ViewBag.ArchiveID = new SelectList(db.Archive, "ID", "ID", archiveCategory.ArchiveID);
            return View(archiveCategory);
        }

        // POST: ArchiveCategories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,NameA,NameE,About,ArchiveID")] ArchiveCategory archiveCategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(archiveCategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Archive");
            }
            ViewBag.ArchiveID = new SelectList(db.Archive, "ID", "ID", archiveCategory.ArchiveID);
            return View(archiveCategory);
        }

        // GET: ArchiveCategories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ArchiveCategory archiveCategory = db.ArchiveCategories.Find(id);
            if (archiveCategory == null)
            {
                return HttpNotFound();
            }
            return View(archiveCategory);
        }

        // POST: ArchiveCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ArchiveCategory archiveCategory = db.ArchiveCategories.Find(id);
            db.ArchiveCategories.Remove(archiveCategory);
            db.SaveChanges();
            return RedirectToAction("Index", "Archive");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
