﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;

using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class NewsController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: News
        public ActionResult Index()
        {
            return View(db.News.OrderByDescending(r => r.ID).ToList());
        }

        // GET: News/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        // GET: News/Create
        public ActionResult Create()
        {
            NewsModel NM = new NewsModel();
            var Clubs = db.Clubs.Select(c => new SelectListItem() { Text = c.NameA, Value = c.ID.ToString() }).ToList();
            ViewBag.Clubs = Clubs;
            var Nationals = db.NationalTeams.Select(c => new SelectListItem() { Text = c.NameA, Value = c.ID.ToString() }).ToList();
            ViewBag.AllNationals = Nationals;
            var Players = db.Players.Select(c => new SelectListItem() { Text = c.NameA, Value = c.ID.ToString() }).ToList();
            ViewBag.AllPlayers = Players;
            return View(new NewsModel());
        }

        // POST: News/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NewsModel news, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                news.ImageUrl = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                News News = new News()
                {
                    TitleA = news.TitleA,
                    TitleE = news.TitleE,
                    ShortDescriptionA = news.ShortDescriptionA,
                    ShortDescriptionE = news.ShortDescriptionE,
                    LongDescriptionE = news.LongDescriptionE,
                    LongDescriptionA = news.LongDescriptionA,
                    ImageUrl = news.ImageUrl,
                    ShareLink = news.ShareLink,
                    Views = 0,
                    CreationDate = DateTime.Now,
                    Important = news.Important
                };
                News.ModifiedDate = News.CreationDate;
                db.News.Add(News);
                db.SaveChanges();
                for (int i = 0; i < news.Clubs.Count; i++)
                {
                    NewsClub club = new NewsClub();
                    club.ClubID = news.Clubs[i];
                    club.NewsID = News.ID;
                    db.NewsClubs.Add(club);
                }

                for (int i = 0; i < news.Nationals.Count; i++)
                {
                    NationalNew NN = new NationalNew();
                    NN.NationalID = news.Nationals[i];
                    NN.NewsID = News.ID;
                    db.NationalNews.Add(NN);
                }
                for (int i = 0; i < news.Players.Count; i++)
                {
                    PlayerNew NN = new PlayerNew();
                    NN.PlayerID = news.Players[i];
                    NN.NewsID = News.ID;
                    db.PlayerNews.Add(NN);
                }
                db.SaveChanges();
                News.ModifiedDate = News.CreationDate;

                //********* Start Email Notification *********//
                SendEmail email = new SendEmail();
                string EmailSubject = "جديد: "+ News.TitleA;
                string Category = "اخر الأخبار الخاصه بالاتحاد المصرى لكره السله";
                string imgUrl = "http://yaken.cloudapp.net/EBBFEDSiteTest/assets/images/logo-1.png";
                string shortBrief = News.ShortDescriptionA;
                string LongDescription = News.LongDescriptionA;
                email.Send(EmailSubject, Category, EmailSubject, imgUrl, News.ShareLink??"", shortBrief, LongDescription);
                //********* End Email Notification *********//

                return RedirectToAction("Index");
            }
            return View(news);
        }

        // GET: News/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            var Clubs = db.Clubs.Select(c => new SelectListItem() { Text = c.NameA, Value = c.ID.ToString() }).ToList();
            ViewBag.AllClubs = Clubs;
            var AllNationals = db.NationalTeams.Select(c => new SelectListItem() { Text = c.NameA, Value = c.ID.ToString() }).ToList();
            ViewBag.AllNationals = AllNationals;
            var AllPlayers = db.Players.Select(c => new SelectListItem() { Text = c.NameA, Value = c.ID.ToString() }).ToList();
            ViewBag.AllPlayers = AllPlayers;
            List<long> Club = db.NewsClubs.Where(c => c.News.ID == id).Select(c => c.Club.ID).ToList();
            List<long> Nationals = db.NationalNews.Where(n => n.NewsID == id).Select(n => n.NationalID).ToList();
            List<long> Players = db.PlayerNews.Where(n => n.NewsID == id).Select(n => n.PlayerID).ToList();

            NewsModel newsModel = new NewsModel()
            {
                ID = news.ID,
                TitleA = news.TitleA,
                TitleE = news.TitleE,
                ShortDescriptionA = news.ShortDescriptionA,
                ShortDescriptionE = news.ShortDescriptionE,
                LongDescriptionA = news.LongDescriptionA,
                LongDescriptionE = news.LongDescriptionE,
                ImageUrl = news.ImageUrl,
                ShareLink = news.ShareLink,
                Views = news.Views ?? 0,
                CreationDate = (DateTime)news.CreationDate,
                ModifiedDate = (DateTime)news.ModifiedDate,
                Clubs = Club,
                Nationals = Nationals,
                Players = Players,
                Important = (news.Important == true)
            };
            return View(newsModel);
        }

        // POST: News/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(NewsModel news, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/images"), upload.FileName);
                upload.SaveAs(fileName);
                news.ImageUrl = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                news.ModifiedDate = DateTime.Now;
                var newsClubs = db.NewsClubs.Where(nc => nc.NewsID == news.ID).ToList();
                foreach (var item in newsClubs)
                {
                    db.NewsClubs.Remove(item);
                }
                var newsNationals = db.NationalNews.Where(nc => nc.NewsID == news.ID).ToList();
                foreach (var item in newsNationals)
                {
                    db.NationalNews.Remove(item);
                }
                var newsPlayers = db.PlayerNews.Where(nc => nc.NewsID == news.ID).ToList();
                foreach (var item in newsPlayers)
                {
                    db.PlayerNews.Remove(item);
                }
                News newsModel = new News()
                {
                    ID = news.ID,
                    TitleA = news.TitleA,
                    TitleE = news.TitleE,
                    ShortDescriptionA = news.ShortDescriptionA,
                    ShortDescriptionE = news.ShortDescriptionE,
                    LongDescriptionE = news.LongDescriptionE,
                    LongDescriptionA = news.LongDescriptionA,
                    ImageUrl = news.ImageUrl,
                    ShareLink = news.ShareLink,
                    Views = news.Views,
                    CreationDate = (DateTime)news.CreationDate,
                    ModifiedDate = (DateTime)news.ModifiedDate,
                    Important = news.Important
                };
                db.Entry(newsModel).State = EntityState.Modified;
                db.SaveChanges();
                for (int i = 0; i < news.Clubs.Count; i++)
                {
                    NewsClub newsClub = new NewsClub();
                    newsClub.ClubID = news.Clubs[i];
                    newsClub.NewsID = news.ID;
                    db.NewsClubs.Add(newsClub);
                }

                for (int i = 0; i < news.Nationals.Count; i++)
                {
                    NationalNew NN = new NationalNew();
                    NN.NationalID = news.Nationals[i];
                    NN.NewsID = news.ID;
                    db.NationalNews.Add(NN);
                }

                for (int i = 0; i < news.Players.Count; i++)
                {
                    PlayerNew NN = new PlayerNew();
                    NN.PlayerID = news.Players[i];
                    NN.NewsID = news.ID;
                    db.PlayerNews.Add(NN);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(news);
        }

        // GET: News/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        // POST: News/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            News news = db.News.Find(id);
            db.News.Remove(news);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
