﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;

using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class CompetitionAgesController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: CompetitionAges
        public ActionResult Index()
        {
            return View(db.CompetitionAges.OrderByDescending(r => r.ID).ToList());
        }

        // GET: CompetitionAges/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompetitionAge competitionAge = db.CompetitionAges.Find(id);
            if (competitionAge == null)
            {
                return HttpNotFound();
            }
            return View(competitionAge);
        }

        // GET: CompetitionAges/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CompetitionAges/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CompetitionAge competitionAge)
        {
            if (ModelState.IsValid)
            {
                db.CompetitionAges.Add(competitionAge);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(competitionAge);
        }

        // GET: CompetitionAges/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompetitionAge competitionAge = db.CompetitionAges.Find(id);
            if (competitionAge == null)
            {
                return HttpNotFound();
            }
            ViewBag.Competitions = db.Competitions.Select(c => new SelectListItem { Text = c.NameE, Value = c.ID.ToString() }).ToList();
            return View(competitionAge);
        }

        // POST: CompetitionAges/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CompetitionAge competitionAge)
        {
            if (ModelState.IsValid)
            {
                db.Entry(competitionAge).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Competitions = db.Competitions.Select(c => new SelectListItem { Text = c.NameE, Value = c.ID.ToString() }).ToList();
            return View(competitionAge);
        }

        // GET: CompetitionAges/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompetitionAge competitionAge = db.CompetitionAges.Find(id);
            if (competitionAge == null)
            {
                return HttpNotFound();
            }
            return View(competitionAge);
        }

        // POST: CompetitionAges/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            CompetitionAge competitionAge = db.CompetitionAges.Find(id);
            db.CompetitionAges.Remove(competitionAge);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
