﻿using EBBFED.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class SettingsController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: Settings
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult EditSettings()
        {
            Settings settings = db.Settings.Find(1);
            if (settings == null)
            {
                Settings initialSettings = new Settings()
                {
                    ID = 1,
                    CoachPDFFile = "Enter CoachPDFFile Here."
                };
                db.Settings.Add(initialSettings);
                db.SaveChanges();

                settings = db.Settings.Find(1);
            }
            return View(settings);
        }

        [HttpPost]
        public ActionResult EditSettings([Bind(Include = "ID,CoachPDFFile")] Settings settings, HttpPostedFileBase pdfFile)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (pdfFile != null)
                    {
                        pdfFile.SaveAs(HttpContext.Server.MapPath("~/Content/Pdfs/")
                                                              + pdfFile.FileName);
                        settings.CoachPDFFile = Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~/Content/Pdfs/") + pdfFile.FileName;
                    }
                    db.Entry(settings).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("EditSettings");
                }
                return View(settings);
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }
    }
}