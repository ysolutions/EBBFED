﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class SeasonTeamsController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: SeasonTeams
        public ActionResult Index(string id)
        {
            long sId = Convert.ToInt64(id);
            ViewBag.SeasonId = sId;
            var seasonTeams = db.SeasonTeams.Where(x=>x.SeasonId== sId).Include(s => s.Season).Include(s => s.Team);
            return View(seasonTeams.OrderByDescending(r => r.ID).ToList());
        }

        // GET: SeasonTeams/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeasonTeam seasonTeam = db.SeasonTeams.Find(id);
            if (seasonTeam == null)
            {
                return HttpNotFound();
            }
            return View(seasonTeam);
        }

        // GET: SeasonTeams/Create
        public ActionResult Create(string id)
        {
            long sId = Convert.ToInt64(id);
            var season = db.Seasons.Find(sId);
            ViewBag.SeasonId = sId;
            ViewBag.TeamId = new SelectList(db.Teams, "ID", "NameA");
            return View(season);
        }

        // POST: SeasonTeams/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TeamId,SeasonId,Rank")] SeasonTeam seasonTeam)
        {
            if (ModelState.IsValid)
            {
                db.SeasonTeams.Add(seasonTeam);
                db.SaveChanges();
                return RedirectToAction("Index","SeasonTeams", new { id= seasonTeam.SeasonId.ToString() });
            }
            long sId = seasonTeam.ID;
            var season = db.Seasons.Find(sId);
            ViewBag.SeasonId = sId;
            ViewBag.TeamId = new SelectList(db.Teams, "ID", "NameA", seasonTeam.TeamId);
            return View(seasonTeam);
        }

        // GET: SeasonTeams/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeasonTeam seasonTeam = db.SeasonTeams.Find(id);
            if (seasonTeam == null)
            {
                return HttpNotFound();
            }
            ViewBag.Sid = seasonTeam.SeasonId;
            ViewBag.SeasonId = new SelectList(db.Seasons, "ID", "Name", seasonTeam.SeasonId);
            ViewBag.TeamId = new SelectList(db.Teams, "ID", "NameA", seasonTeam.TeamId);
            return View(seasonTeam);
        }

        // POST: SeasonTeams/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,TeamId,SeasonId,Rank")] SeasonTeam seasonTeam)
        {
            if (ModelState.IsValid)
            {
                db.Entry(seasonTeam).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SeasonId = new SelectList(db.Seasons, "ID", "Name", seasonTeam.SeasonId);
            ViewBag.TeamId = new SelectList(db.Teams, "ID", "NameA", seasonTeam.TeamId);
            return View(seasonTeam);
        }

        // GET: SeasonTeams/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeasonTeam seasonTeam = db.SeasonTeams.Find(id);
            if (seasonTeam == null)
            {
                return HttpNotFound();
            }
            ViewBag.SeasonId = seasonTeam.SeasonId;
            return View(seasonTeam);
        }

        // POST: SeasonTeams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            SeasonTeam seasonTeam = db.SeasonTeams.Find(id);
            db.SeasonTeams.Remove(seasonTeam);
            db.SaveChanges();
            return RedirectToAction("Index", "Seasonss");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
