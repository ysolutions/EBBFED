﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;

using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class NationalAgesController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: NationalAges
        public ActionResult Index()
        {
            var nationalAges = db.NationalAges.Include(n => n.Age).Include(n => n.NationalCompetition);
            return View(nationalAges.OrderByDescending(r => r.ID).ToList());
        }

        // GET: NationalAges/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NationalAge nationalAge = db.NationalAges.Find(id);
            if (nationalAge == null)
            {
                return HttpNotFound();
            }
            return View(nationalAge);
        }

        // GET: NationalAges/Create
        public ActionResult Create()
        {
            ViewBag.AgesID = new SelectList(db.Ages, "ID", "NameA");
            ViewBag.NationaCompetitionID = new SelectList(db.NationalCompetitions, "ID", "NameA");
            return View();
        }

        // POST: NationalAges/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,AgesID,NationaCompetitionID")] NationalAge nationalAge)
        {
            if (ModelState.IsValid)
            {
                db.NationalAges.Add(nationalAge);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AgesID = new SelectList(db.Ages, "ID", "NameA", nationalAge.AgesID);
            ViewBag.NationaCompetitionID = new SelectList(db.NationalCompetitions, "ID", "NameA", nationalAge.NationaCompetitionID);
            return View(nationalAge);
        }

        // GET: NationalAges/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NationalAge nationalAge = db.NationalAges.Find(id);
            if (nationalAge == null)
            {
                return HttpNotFound();
            }
            ViewBag.AgesID = new SelectList(db.Ages, "ID", "NameA", nationalAge.AgesID);
            ViewBag.NationaCompetitionID = new SelectList(db.NationalCompetitions, "ID", "NameA", nationalAge.NationaCompetitionID);
            return View(nationalAge);
        }

        // POST: NationalAges/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,AgesID,NationaCompetitionID")] NationalAge nationalAge)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nationalAge).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AgesID = new SelectList(db.Ages, "ID", "NameA", nationalAge.AgesID);
            ViewBag.NationaCompetitionID = new SelectList(db.NationalCompetitions, "ID", "NameA", nationalAge.NationaCompetitionID);
            return View(nationalAge);
        }

        // GET: NationalAges/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NationalAge nationalAge = db.NationalAges.Find(id);
            if (nationalAge == null)
            {
                return HttpNotFound();
            }
            return View(nationalAge);
        }

        // POST: NationalAges/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            NationalAge nationalAge = db.NationalAges.Find(id);
            db.NationalAges.Remove(nationalAge);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
