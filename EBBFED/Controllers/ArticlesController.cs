﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBBFED.Models;
using EBBFED.Infra;

using static EBBFED.FilterConfig;

namespace EBBFED.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class ArticlesController : Controller
    {
        private KoraEntities db = new KoraEntities();

        // GET: Articles
        public ActionResult Index()
        {
            return View(db.Articles.OrderByDescending(r => r.ID).ToList());
        }

        // GET: Articles/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Article article = db.Articles.Find(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            return View(article);
        }

        // GET: Articles/Create
        public ActionResult Create()
        {
            return View(new ArticlesModel());
        }

        // POST: Articles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ArticlesModel article, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                article.ImageUrl = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                Article Ar = new Article()
                {
                    Date = article.Date,
                    ImageUrl = article.ImageUrl,
                    TitleA = article.TitleA,
                    TitleE = article.TitleE,
                    ShortDescriptionA = article.ShortDescriptionA,
                    ShortDescriptionE = article.ShortDescriptionE,
                    LongDescriptionA = article.LongDescriptionA,
                    LongDescriptionE = article.LongDescriptionE
                };
                db.Articles.Add(Ar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(article);
        }

        // GET: Articles/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Article article = db.Articles.Find(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            ArticlesModel Ar = new ArticlesModel()
            {
                ID = article.ID,
                Date = (DateTime)article.Date,
                ImageUrl = article.ImageUrl,
                TitleA = article.TitleA,
                TitleE = article.TitleE,
                ShortDescriptionA = article.ShortDescriptionA,
                ShortDescriptionE = article.ShortDescriptionE,
                LongDescriptionA = article.LongDescriptionA,
                LongDescriptionE = article.LongDescriptionE
            };
            return View(Ar);
        }

        // POST: Articles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ArticlesModel article, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var time = DateTimeOffset.Now.ToUnixTimeSeconds();
                string fileName = Path.Combine(Server.MapPath("~/Content/Images"), upload.FileName);
                upload.SaveAs(fileName);
                article.ImageUrl = upload.FileName;
            }
            if (ModelState.IsValid)
            {
                Article Ar = new Article()
                {
                    ID = article.ID,
                    Date = article.Date,
                    ImageUrl = article.ImageUrl,
                    TitleA = article.TitleA,
                    TitleE = article.TitleE,
                    ShortDescriptionA = article.ShortDescriptionA,
                    ShortDescriptionE = article.ShortDescriptionE,
                    LongDescriptionA = article.LongDescriptionA,
                    LongDescriptionE = article.LongDescriptionE
                };
                db.Entry(Ar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(article);
        }

        // GET: Articles/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Article article = db.Articles.Find(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            return View(article);
        }

        // POST: Articles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Article article = db.Articles.Find(id);
            db.Articles.Remove(article);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
