USE [Kora]
GO
SET IDENTITY_INSERT [dbo].[Positions] ON 

INSERT [dbo].[Positions] ([ID], [NameA], [NameE]) VALUES (2, N'جناح', N'Winger')
INSERT [dbo].[Positions] ([ID], [NameA], [NameE]) VALUES (3, N'صانع ألعاب', N'Playmaker')
INSERT [dbo].[Positions] ([ID], [NameA], [NameE]) VALUES (4, N'إرتكاز', N'Pivot')
SET IDENTITY_INSERT [dbo].[Positions] OFF
SET IDENTITY_INSERT [dbo].[Divisions] ON 

INSERT [dbo].[Divisions] ([ID], [NameA], [NameE]) VALUES (1, N'درجه الاولى ', N'First Division')
INSERT [dbo].[Divisions] ([ID], [NameA], [NameE]) VALUES (2, N'درجه الثانيه', N'Second Division')
INSERT [dbo].[Divisions] ([ID], [NameA], [NameE]) VALUES (3, N'درجه الثالثه', N'Third Division')
SET IDENTITY_INSERT [dbo].[Divisions] OFF
SET IDENTITY_INSERT [dbo].[Clubs] ON 

INSERT [dbo].[Clubs] ([ID], [NameE], [NameA], [DivisionID], [Logo], [EstablishedYear], [TitlesWon], [DescriptionA], [DescriptionE]) VALUES (13, N'NO CLUB', N'لا يوجد نادى', 1, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Clubs] ([ID], [NameE], [NameA], [DivisionID], [Logo], [EstablishedYear], [TitlesWon], [DescriptionA], [DescriptionE]) VALUES (14, N'AlAhly', N'أهلي', 1, N'alahly logo.jpeg', 1907, N'<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	فاز بالدوري المصري 4 مرات</p>
<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	فاز بكأس مصر 5 مرات</p>
<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	فاز ببطولة أفريقيا 4 مرات</p>
<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	فاز بالبطولة العربية 2 مرات</p>
', N'<p>
	<span style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">نادي القرن في أفريقيا</span></p>
', N'<p>
	<span style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">نادي القرن في أفريقيا</span></p>
')
INSERT [dbo].[Clubs] ([ID], [NameE], [NameA], [DivisionID], [Logo], [EstablishedYear], [TitlesWon], [DescriptionA], [DescriptionE]) VALUES (15, N'Zamalek', N'زمالك', 1, N'200px-Zamalek_SC_logo.png', 0, N'<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	فاز بالدوري المصري 4 مرات</p>
<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	فاز بكأس مصر 5 مرات</p>
<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	فاز ببطولة أفريقيا 4 مرات</p>
<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	فاز بالبطولة العربية 2 مرات</p>
<div>
	&nbsp;</div>
', N'<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	<span style="background-color: rgb(253, 253, 253);">ملوك الصالات</span></p>
', N'<p>
	<span style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">ملوك الصالات</span></p>
')
INSERT [dbo].[Clubs] ([ID], [NameE], [NameA], [DivisionID], [Logo], [EstablishedYear], [TitlesWon], [DescriptionA], [DescriptionE]) VALUES (16, N'Smouha', N'سموحة', 2, N'220px-Smouha_SC_LOGO.png', 1940, N'<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	فاز بالدوري المصري 4 مرات</p>
<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	فاز بكأس مصر 5 مرات</p>
<div>
	&nbsp;</div>
', N'<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	نادي سموحة الرياضي بالاسكندرية</p>
<div>
	&nbsp;</div>
', N'<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	نادي سموحة الرياضي بالاسكندرية</p>
<div>
	&nbsp;</div>
')
INSERT [dbo].[Clubs] ([ID], [NameE], [NameA], [DivisionID], [Logo], [EstablishedYear], [TitlesWon], [DescriptionA], [DescriptionE]) VALUES (17, N'Ittihad', N'الاتحاد السكندري', 1, N'Al-Ittihad_Alexandria_Club_logo.png', 1912, N'<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	فاز بالدوري المصري 12 مرات</p>
<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	فاز بكأس مصر 11 مرات</p>
<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	فاز ببطولة أفريقيا 6 مرات</p>
<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	فاز بالبطولة العربية 5 مرات</p>
<div>
	&nbsp;</div>
', N'<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	سيد البلد</p>
<div>
	&nbsp;</div>
', N'<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	سيد البلد</p>
<div>
	&nbsp;</div>
')
INSERT [dbo].[Clubs] ([ID], [NameE], [NameA], [DivisionID], [Logo], [EstablishedYear], [TitlesWon], [DescriptionA], [DescriptionE]) VALUES (18, N'Altiram', N'الترام', 2, NULL, 1928, N'<p>
	No Titles Won</p>
', N'<p>
	<span style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">نادي الترام الرياضي</span></p>
', N'<p>
	<span style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">نادي الترام الرياضي</span></p>
')
SET IDENTITY_INSERT [dbo].[Clubs] OFF
SET IDENTITY_INSERT [dbo].[Players] ON 

INSERT [dbo].[Players] ([ID], [ImageUrl], [Age], [PositionID], [Height], [Weight], [InternationalMatches], [ClubID], [PlayerType], [NameE], [NameA], [PlayerNumber]) VALUES (5, N'aly khalifa.jpeg', 25, 2, 190, 91, 20, 14, N'National', N'Ali Khalifa', N'علي خليفة', 10)
INSERT [dbo].[Players] ([ID], [ImageUrl], [Age], [PositionID], [Height], [Weight], [InternationalMatches], [ClubID], [PlayerType], [NameE], [NameA], [PlayerNumber]) VALUES (6, N'assem marei.jpg', 24, 3, 188, 85, 20, 17, N'InterNational', N'Assem Marei', N'عاصم مرعي', 11)
SET IDENTITY_INSERT [dbo].[Players] OFF
SET IDENTITY_INSERT [dbo].[News] ON 

INSERT [dbo].[News] ([ID], [TitleE], [TitleA], [ShortDescriptionE], [ShortDescriptionA], [CreationDate], [ModifiedDate], [ImageUrl], [Views], [ShareLink], [LongDescriptionE], [LongDescriptionA], [Important]) VALUES (19, N'عاصم مرعي بريء', N'عاصم مرعي بريء', N'<h1 class="art-postheader entry-title" style="font-size: 24px; font-family: Tahoma; margin: 0px 0px 5px; padding: 0px; font-weight: normal; color: rgb(255, 255, 255);">
	<span class="art-postheadericon"><span mce_style="color: #333333;" style="color: rgb(51, 51, 51);">الاتحاد المصري يرفض اتهام اللاعب عاصم مرعي بالتهرب من الانضمام للمنتخب</span></span></h1>
', N'<h1 class="art-postheader entry-title" style="font-size: 24px; font-family: Tahoma; margin: 0px 0px 5px; padding: 0px; font-weight: normal; color: rgb(255, 255, 255);">
	<span class="art-postheadericon"><span mce_style="color: #333333;" style="color: rgb(51, 51, 51);">الاتحاد المصري يرفض اتهام اللاعب عاصم مرعي بالتهرب من الانضمام للمنتخب</span></span></h1>
', CAST(N'2018-12-05' AS Date), CAST(N'2018-12-19' AS Date), N'news3.jpeg', 1, NULL, N'<h2 class="_5wj-" dir="rtl" style="margin: 0px 0px 20px; padding: 0px; font-size: 24px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; color: rgb(144, 67, 4); text-align: right; background-color: rgba(255, 255, 255, 0.8);">
	<span style="font-size:16px;"><span style="color:#000000;">أكد الدكتور مجدي أبو فريخة رئيس الاتحاد المصري لكرة السلة على ثقته في انتماء اللاعب عاصم مرعي المحترف بالدوري التركي ورغبته الدائمة في ارتداء قميص منتخب بلاده.<br />
	ورفض رئيس الاتحاد الاتهامات الموجهة إلى اللاعب بأنه تقاعس عن تمثيل المنتخب المصري في المرحلة الخامسة من التصفيات الأفريقية المؤهلة لكأس العالم 2019 بالصين والتي أقيمت في أنجولا.<br />
	وشدد أبو فريخة ان مجلس الادارة بالكامل يثق في لاعبه الدولي وأن عاصم مرعي لم يتأخر يومًا عن تلبية نداء المنتخب الوطني في أي مباراة منذ أن بدأ ممارسة اللعبة وأن الإصابة كانت السبب وراء عدم انضمامه رفقة زملاءه في التصفيات الأخيرة متمنيًا له التوفيق مع ناديه التركي في الفترة المقبلة.</span></span></h2>
', N'<h2 class="_5wj-" dir="rtl" style="margin: 0px 0px 20px; padding: 0px; font-size: 24px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; color: rgb(144, 67, 4); text-align: right; background-color: rgba(255, 255, 255, 0.8);">
	<span style="font-size: 16px;"><span style="color: rgb(0, 0, 0);">أكد الدكتور مجدي أبو فريخة رئيس الاتحاد المصري لكرة السلة على ثقته في انتماء اللاعب عاصم مرعي المحترف بالدوري التركي ورغبته الدائمة في ارتداء قميص منتخب بلاده.<br />
	ورفض رئيس الاتحاد الاتهامات الموجهة إلى اللاعب بأنه تقاعس عن تمثيل المنتخب المصري في المرحلة الخامسة من التصفيات الأفريقية المؤهلة لكأس العالم 2019 بالصين والتي أقيمت في أنجولا.<br />
	وشدد أبو فريخة ان مجلس الادارة بالكامل يثق في لاعبه الدولي وأن عاصم مرعي لم يتأخر يومًا عن تلبية نداء المنتخب الوطني في أي مباراة منذ أن بدأ ممارسة اللعبة وأن الإصابة كانت السبب وراء عدم انضمامه رفقة زملاءه في التصفيات الأخيرة متمنيًا له التوفيق مع ناديه التركي في الفترة المقبلة.</span></span></h2>
', 1)
INSERT [dbo].[News] ([ID], [TitleE], [TitleA], [ShortDescriptionE], [ShortDescriptionA], [CreationDate], [ModifiedDate], [ImageUrl], [Views], [ShareLink], [LongDescriptionE], [LongDescriptionA], [Important]) VALUES (22, N'علي خليفة في الطريق إلى ال-NBA؟', N'علي خليفة في الطريق إلى ال-NBA؟', N'<h1 class="art-postheader entry-title" style="margin: 0px 0px 5px; padding: 0px; font-size: 24px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; font-weight: normal; color: rgb(255, 255, 255);">
	<span style="font-size:16px;"><span style="color:#000000;"><span class="art-postheadericon">الاتحاد الدولي يختار علي خليفة للمشاركة في معسكر BWB</span></span></span></h1>
', N'<h1 class="art-postheader entry-title" style="margin: 0px 0px 5px; padding: 0px; font-size: 24px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; font-weight: normal; color: rgb(255, 255, 255);">
	<span style="font-size:16px;"><span style="color: rgb(0, 0, 0);">الاتحاد الدولي يختار علي خليفة للمشاركة في معسكر BWB</span></span></h1>
', CAST(N'2018-12-05' AS Date), CAST(N'2018-12-17' AS Date), N'news2.jpeg', 2, NULL, N'<h2 dir="rtl" style="margin: 0px 0px 20px; padding: 0px; font-size: 24px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; color: rgb(144, 67, 4); background-color: rgba(255, 255, 255, 0.8);">
	&nbsp;</h2>
<p style="margin: 10px 0px; padding: 0px; text-align: right;">
	<span style="font-size:16px;"><span style="font-family:arial,helvetica,sans-serif;">تلقى الإتحاد المصري لكرة السلة خطابًا من الرابطة الوطنية لكرة السلة الأمريكية NBA اليوم الثلاثاء يعلن فيه إختيار اللاعب علي خليفة للمشاركة في معسكر النسخة الخامسة من BWB تحت إشراف الرابطة والاتحاد الدولي لكرة السلة FIBA.</span></span></p>
<p style="margin: 10px 0px; padding: 0px; text-align: right;">
	<span style="font-size:16px;"><span style="font-family:arial,helvetica,sans-serif;">ويتم إختيار كل لاعب للمشاركة في هذا المعسكر بناءً على مهاراته المتميزة في اللعبة وقدراته القيادية داخل أرض الملعب.</span></span></p>
<p style="margin: 10px 0px; padding: 0px; text-align: right;">
	<span style="font-size:16px;"><span style="font-family:arial,helvetica,sans-serif;">وأقامت الرابطة الوطنية لكرة السلة الأمريكية (NBA) و الاتحاد الدولى لكرة السلة (FIBA) معسكر BWB في 35 مدينة في 28 دولة في ست قارات منذ عام 2001 ، وتم تصعيد 53 من خريجي BWB للعب في الـ NBA</span></span></p>
', N'<p>
	&nbsp;</p>
<p style="margin: 10px 0px; padding: 0px; text-align: right;">
	<span style="font-size: 16px;"><span style="font-family: arial, helvetica, sans-serif;">تلقى الإتحاد المصري لكرة السلة خطابًا من الرابطة الوطنية لكرة السلة الأمريكية NBA اليوم الثلاثاء يعلن فيه إختيار اللاعب علي خليفة للمشاركة في معسكر النسخة الخامسة من BWB تحت إشراف الرابطة والاتحاد الدولي لكرة السلة FIBA.</span></span></p>
<p style="margin: 10px 0px; padding: 0px; text-align: right;">
	<span style="font-size: 16px;"><span style="font-family: arial, helvetica, sans-serif;">ويتم إختيار كل لاعب للمشاركة في هذا المعسكر بناءً على مهاراته المتميزة في اللعبة وقدراته القيادية داخل أرض الملعب.</span></span></p>
<p style="margin: 10px 0px; padding: 0px; text-align: right;">
	<span style="font-size: 16px;"><span style="font-family: arial, helvetica, sans-serif;">وأقامت الرابطة الوطنية لكرة السلة الأمريكية (NBA) و الاتحاد الدولى لكرة السلة (FIBA) معسكر BWB في 35 مدينة في 28 دولة في ست قارات منذ عام 2001 ، وتم تصعيد 53 من خريجي BWB للعب في الـ NBA</span></span></p>
', 0)
INSERT [dbo].[News] ([ID], [TitleE], [TitleA], [ShortDescriptionE], [ShortDescriptionA], [CreationDate], [ModifiedDate], [ImageUrl], [Views], [ShareLink], [LongDescriptionE], [LongDescriptionA], [Important]) VALUES (23, N'بحث تنظيم دخول الجماهير', N'بحث تنظيم دخول الجماهير', N'<h1 class="art-postheader entry-title" style="margin: 0px 0px 5px; padding: 0px; font-size: 24px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; font-weight: normal; color: rgb(255, 255, 255);">
	<span style="color: rgb(0, 0, 0);">دعوة أندية الممتاز لاجتماع مع قيادات الامن لبحث تنظيم دخول الجماهير</span></h1>
', N'<h1 class="art-postheader entry-title" style="margin: 0px 0px 5px; padding: 0px; font-size: 24px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; font-weight: normal; color: rgb(255, 255, 255);">
	<span style="color:#000000;"><span class="art-postheadericon">دعوة أندية الممتاز لاجتماع مع قيادات الامن لبحث تنظيم دخول الجماهير</span></span></h1>
', CAST(N'2018-12-11' AS Date), CAST(N'2018-12-17' AS Date), N'news4.jpg', 0, NULL, N'<p>
	wer</p>
<h2 dir="rtl" style="margin: 0px 0px 20px; padding: 0px; font-size: 24px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; color: rgb(144, 67, 4); text-align: right; background-color: rgba(255, 255, 255, 0.8);">
	<span style="color: rgb(0, 0, 0);">أرسل الاتحاد المصري لكرة السلة برئاسة الدكتور مجدي أبو فريخة خطابًا لأندية الممتاز من أجل دعوة ممثليهم لاجتماع بحضور مسئولي وزارة الداخلية لبحث ملف تنظيم دخول الجماهير في أقرب وقت خلال مسابقات الاتحاد&nbsp;</span></h2>
', N'<h2 dir="rtl" style="margin: 0px 0px 20px; padding: 0px; font-size: 24px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; color: rgb(144, 67, 4); text-align: right; background-color: rgba(255, 255, 255, 0.8);">
	<span style="color:#000000;">أرسل الاتحاد المصري لكرة السلة برئاسة الدكتور مجدي أبو فريخة خطابًا لأندية الممتاز من أجل دعوة ممثليهم لاجتماع بحضور مسئولي وزارة الداخلية لبحث ملف تنظيم دخول الجماهير في أقرب وقت خلال مسابقات الاتحاد&nbsp;</span></h2>
', 1)
INSERT [dbo].[News] ([ID], [TitleE], [TitleA], [ShortDescriptionE], [ShortDescriptionA], [CreationDate], [ModifiedDate], [ImageUrl], [Views], [ShareLink], [LongDescriptionE], [LongDescriptionA], [Important]) VALUES (27, N'قبول استقالة عمرو ابو الخير من منصبه', N'قبول استقالة عمرو ابو الخير من منصبه', NULL, NULL, CAST(N'2018-12-13' AS Date), CAST(N'2018-12-17' AS Date), N'16831304_B.png', 1, NULL, N'<h2 class="_5wj-" dir="rtl" style="margin: 0px 0px 20px; padding: 0px; font-size: 24px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; color: rgb(144, 67, 4); text-align: right; background-color: rgba(255, 255, 255, 0.8);">
	<span style="color: rgb(0, 0, 0);">تقدم كابتن عمرو أبو الخير المدير الفني للمنتخب المصري وجهازه الفني بالإستقالة من منصبه بعد عودته لمصر وعدم التأهل لكأس العالم الصين 2019.</span></h2>
<h2 class="_5wj-" dir="rtl" style="margin: 0px 0px 20px; padding: 0px; font-size: 24px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; color: rgb(144, 67, 4); text-align: right; background-color: rgba(255, 255, 255, 0.8);">
	<span style="color: rgb(0, 0, 0);">وقرر مجلس الإدارة برئاسة الدكتور مجدي أبو فريخة قبول الاستقالة وتوجيه الشكر لأبو الخير على الفترة التي قضاها مع المنتخب المصري</span></h2>
', N'<h2 class="_5wj-" dir="rtl" style="margin: 0px 0px 20px; padding: 0px; font-size: 24px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; color: rgb(144, 67, 4); text-align: right; background-color: rgba(255, 255, 255, 0.8);">
	<span style="color:#000000;">تقدم كابتن عمرو أبو الخير المدير الفني للمنتخب المصري وجهازه الفني بالإستقالة من منصبه بعد عودته لمصر وعدم التأهل لكأس العالم الصين 2019.</span></h2>
<h2 class="_5wj-" dir="rtl" style="margin: 0px 0px 20px; padding: 0px; font-size: 24px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; color: rgb(144, 67, 4); text-align: right; background-color: rgba(255, 255, 255, 0.8);">
	<span style="color:#000000;">وقرر مجلس الإدارة برئاسة الدكتور مجدي أبو فريخة قبول الاستقالة وتوجيه الشكر لأبو الخير على الفترة التي قضاها مع المنتخب المصري</span></h2>
', 0)
SET IDENTITY_INSERT [dbo].[News] OFF
SET IDENTITY_INSERT [dbo].[PlayerNews] ON 

INSERT [dbo].[PlayerNews] ([ID], [PlayerID], [NewsID]) VALUES (4, 6, 19)
SET IDENTITY_INSERT [dbo].[PlayerNews] OFF
SET IDENTITY_INSERT [dbo].[Galleries] ON 

INSERT [dbo].[Galleries] ([ID], [ImageUrl], [VideoUrl], [DescriptionA], [DescriptionE], [Date]) VALUES (9, N'gallery1.jpg', N'16831304_B.png', N'<h1 class="sow-headline" style="margin-right: 0px; margin-bottom: 12px; margin-left: 0px; padding: 0px; font-size: 28px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; color: rgb(0, 0, 0); line-height: 1.4em; background-color: rgba(255, 255, 255, 0.8); margin-top: 0px !important; text-align: right;">
	المؤتمر الصحفى للإعلان عن إستضافة كأس العالم للشباب 2017</h1>
<p>
	&nbsp;</p>
', N'<h1 class="sow-headline" style="margin-right: 0px; margin-bottom: 12px; margin-left: 0px; padding: 0px; font-size: 28px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; color: rgb(0, 0, 0); line-height: 1.4em; background-color: rgba(255, 255, 255, 0.8); text-align: right; margin-top: 0px !important;">
	المؤتمر الصحفى للإعلان عن إستضافة كأس العالم للشباب 2017</h1>
', CAST(N'2018-12-17' AS Date))
INSERT [dbo].[Galleries] ([ID], [ImageUrl], [VideoUrl], [DescriptionA], [DescriptionE], [Date]) VALUES (10, N'gallery2.jpg', N'16831304_B.png', N'<h1 class="sow-headline" style="margin-right: 0px; margin-bottom: 12px; margin-left: 0px; padding: 0px; font-size: 28px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; color: rgb(0, 0, 0); line-height: 1.4em; background-color: rgba(255, 255, 255, 0.8); text-align: right; margin-top: 0px !important;">
	المؤتمر الصحفى للإعلان عن إستضافة كأس العالم للشباب 2017</h1>
', N'<h1 class="sow-headline" style="margin-right: 0px; margin-bottom: 12px; margin-left: 0px; padding: 0px; font-size: 28px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; color: rgb(0, 0, 0); line-height: 1.4em; background-color: rgba(255, 255, 255, 0.8); text-align: right; margin-top: 0px !important;">
	المؤتمر الصحفى للإعلان عن إستضافة كأس العالم للشباب 2017</h1>
', CAST(N'2018-12-17' AS Date))
INSERT [dbo].[Galleries] ([ID], [ImageUrl], [VideoUrl], [DescriptionA], [DescriptionE], [Date]) VALUES (11, N'gallery3.jpg', N'16831304_B.png', N'<p style="text-align: right;">
	<span style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">المنتخب الحائز على البطولة الإفريقية ١٩٧٠</span></p>
', N'<p style="text-align: right;">
	<span style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; text-align: right; background-color: rgb(253, 253, 253);">المنتخب الحائز على البطولة الإفريقية ١٩٧٠</span></p>
', CAST(N'2018-12-17' AS Date))
INSERT [dbo].[Galleries] ([ID], [ImageUrl], [VideoUrl], [DescriptionA], [DescriptionE], [Date]) VALUES (13, NULL, NULL, N'<h2 style="text-align: right;">
	<span style="font-family:arial,helvetica,sans-serif;">بث مباشر مباراة الاهلي و هومنتمن لبنان البطولة العربية للأندية سيدات رقم 20</span></h2>
', N'<h2 style="text-align: right;">
	<span style="font-family: arial, helvetica, sans-serif;">بث مباشر مباراة الاهلي و هومنتمن لبنان البطولة العربية للأندية سيدات رقم 20</span></h2>
', CAST(N'2018-12-17' AS Date))
SET IDENTITY_INSERT [dbo].[Galleries] OFF
SET IDENTITY_INSERT [dbo].[Transfars] ON 

INSERT [dbo].[Transfars] ([ID], [TransfarFrom], [TransfarTo], [Year], [PLayerID]) VALUES (25, N'Ittihad', N'Turkey', 2017, 6)
SET IDENTITY_INSERT [dbo].[Transfars] OFF
SET IDENTITY_INSERT [dbo].[UnoinCommittes] ON 

INSERT [dbo].[UnoinCommittes] ([ID], [ImageUrl], [NameA], [NameE], [ObjectivesA], [ObjectivesE]) VALUES (2, N'16831304_B.png', N'اللجنة الفنية والتخطيط', N'Technical & Planning Committee', N'<div dir="rtl" style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	التخطيط الفني لمسابقات</div>
<div dir="rtl" style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	برامج إعداد المنتخبات</div>
', N'<div dir="rtl" style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	التخطيط الفني لمسابقات</div>
<div dir="rtl" style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	برامج إعداد المنتخبات</div>
')
INSERT [dbo].[UnoinCommittes] ([ID], [ImageUrl], [NameA], [NameE], [ObjectivesA], [ObjectivesE]) VALUES (4, N'16831304_B.png', N'لجنة حكام 3×3', N'3x3 Judges Committee', N'<p>
	لجنة حكام 3&times;3</p>
', N'<p>
	لجنة حكام 3&times;3</p>
')
SET IDENTITY_INSERT [dbo].[UnoinCommittes] OFF
SET IDENTITY_INSERT [dbo].[ComitteMembers] ON 

INSERT [dbo].[ComitteMembers] ([ID], [NameA], [NameE], [CommitteID]) VALUES (1, N'م. محمد عبدالمطلب سليمان	', N'y', 2)
INSERT [dbo].[ComitteMembers] ([ID], [NameA], [NameE], [CommitteID]) VALUES (2, N'أ. عمرو احمد مصيلحي	', N'z', 2)
INSERT [dbo].[ComitteMembers] ([ID], [NameA], [NameE], [CommitteID]) VALUES (3, N'المستشار . علاء زهران	', N'y', 2)
SET IDENTITY_INSERT [dbo].[ComitteMembers] OFF
SET IDENTITY_INSERT [dbo].[NationalTeams] ON 

INSERT [dbo].[NationalTeams] ([ID], [NameA], [NameE], [ImageUrl]) VALUES (4, N'متخب مصر عمومي رجال', N'Men''s National Team', N'16831304_B.png')
INSERT [dbo].[NationalTeams] ([ID], [NameA], [NameE], [ImageUrl]) VALUES (5, N'منتخب مصر عمومي سيدات', N'Women''s National Team', N'16831304_B.png')
INSERT [dbo].[NationalTeams] ([ID], [NameA], [NameE], [ImageUrl]) VALUES (6, N'منتخب مصر انسات تحت ١٨ سنة', N'Women U18 National Team', NULL)
INSERT [dbo].[NationalTeams] ([ID], [NameA], [NameE], [ImageUrl]) VALUES (7, N'متخب غانا عمومي رجال', N'Ghana National Team', NULL)
SET IDENTITY_INSERT [dbo].[NationalTeams] OFF
SET IDENTITY_INSERT [dbo].[NationalNews] ON 

INSERT [dbo].[NationalNews] ([ID], [NewsID], [NationalID]) VALUES (25, 27, 4)
INSERT [dbo].[NationalNews] ([ID], [NewsID], [NationalID]) VALUES (26, 27, 5)
SET IDENTITY_INSERT [dbo].[NationalNews] OFF
SET IDENTITY_INSERT [dbo].[NewsClubs] ON 

INSERT [dbo].[NewsClubs] ([ID], [ClubID], [NewsID]) VALUES (65, 14, 22)
SET IDENTITY_INSERT [dbo].[NewsClubs] OFF
SET IDENTITY_INSERT [dbo].[Ages] ON 

INSERT [dbo].[Ages] ([ID], [NameA], [NameE]) VALUES (1, N'تحت 18 سنه', N'teens')
INSERT [dbo].[Ages] ([ID], [NameA], [NameE]) VALUES (4, N'عمومي رجال', N'Men')
INSERT [dbo].[Ages] ([ID], [NameA], [NameE]) VALUES (5, N'المرتبط سيدات', N'ladies Youngsters')
SET IDENTITY_INSERT [dbo].[Ages] OFF
SET IDENTITY_INSERT [dbo].[Competitions] ON 

INSERT [dbo].[Competitions] ([ID], [NameA], [NameE], [Date], [Location], [RegisterLink], [Gender]) VALUES (5, N'Rules1', N'sdf', CAST(N'2018-12-14' AS Date), N'cairo', N'asdasd', NULL)
INSERT [dbo].[Competitions] ([ID], [NameA], [NameE], [Date], [Location], [RegisterLink], [Gender]) VALUES (6, N'Rules1', N'sdf', CAST(N'2018-12-21' AS Date), N'cairo', N'asdasd', NULL)
SET IDENTITY_INSERT [dbo].[Competitions] OFF
SET IDENTITY_INSERT [dbo].[CompetitionAges] ON 

INSERT [dbo].[CompetitionAges] ([ID], [AgesID], [CompetitionID]) VALUES (3, 1, 5)
INSERT [dbo].[CompetitionAges] ([ID], [AgesID], [CompetitionID]) VALUES (4, 1, 6)
INSERT [dbo].[CompetitionAges] ([ID], [AgesID], [CompetitionID]) VALUES (5, 4, 6)
SET IDENTITY_INSERT [dbo].[CompetitionAges] OFF
SET IDENTITY_INSERT [dbo].[Matches] ON 

INSERT [dbo].[Matches] ([ID], [ClubA], [ClubB], [Gender], [Date], [Place], [LiveLink], [Ages], [DivisionID], [ScoreA], [ScoreB], [Time]) VALUES (3, 14, 15, N'Male', CAST(N'2018-12-18' AS Date), N'Cairo', NULL, 1, 1, 0, 0, CAST(N'18:00:00' AS Time))
INSERT [dbo].[Matches] ([ID], [ClubA], [ClubB], [Gender], [Date], [Place], [LiveLink], [Ages], [DivisionID], [ScoreA], [ScoreB], [Time]) VALUES (4, 15, 17, N'Male', CAST(N'2018-12-18' AS Date), N'Cairo', NULL, 4, 1, 78, 70, CAST(N'14:00:00' AS Time))
INSERT [dbo].[Matches] ([ID], [ClubA], [ClubB], [Gender], [Date], [Place], [LiveLink], [Ages], [DivisionID], [ScoreA], [ScoreB], [Time]) VALUES (5, 16, 18, N'Male', CAST(N'2018-12-12' AS Date), N'Alex', NULL, 4, 2, 77, 60, CAST(N'18:00:00' AS Time))
INSERT [dbo].[Matches] ([ID], [ClubA], [ClubB], [Gender], [Date], [Place], [LiveLink], [Ages], [DivisionID], [ScoreA], [ScoreB], [Time]) VALUES (6, 14, 15, N'Female', CAST(N'2018-12-19' AS Date), N'Cairo', NULL, 5, 1, 0, 0, CAST(N'17:00:00' AS Time))
INSERT [dbo].[Matches] ([ID], [ClubA], [ClubB], [Gender], [Date], [Place], [LiveLink], [Ages], [DivisionID], [ScoreA], [ScoreB], [Time]) VALUES (7, 17, 14, N'Male', CAST(N'2018-12-26' AS Date), N'Cairo', NULL, 4, 1, 0, 0, CAST(N'19:00:00' AS Time))
INSERT [dbo].[Matches] ([ID], [ClubA], [ClubB], [Gender], [Date], [Place], [LiveLink], [Ages], [DivisionID], [ScoreA], [ScoreB], [Time]) VALUES (9, 15, 14, N'Female', CAST(N'2018-12-10' AS Date), N'Cairo', NULL, 1, 1, 55, 75, CAST(N'15:00:00' AS Time))
SET IDENTITY_INSERT [dbo].[Matches] OFF
SET IDENTITY_INSERT [dbo].[NationalCompetitions] ON 

INSERT [dbo].[NationalCompetitions] ([ID], [NameA], [NameE], [Gender], [Date]) VALUES (2, N'تصفيات كأس العالم 2019', N'World Cup Qualifications', N'Male', CAST(N'2018-12-01' AS Date))
SET IDENTITY_INSERT [dbo].[NationalCompetitions] OFF
SET IDENTITY_INSERT [dbo].[NationalAges] ON 

INSERT [dbo].[NationalAges] ([ID], [AgesID], [NationaCompetitionID]) VALUES (4, 4, 2)
SET IDENTITY_INSERT [dbo].[NationalAges] OFF
SET IDENTITY_INSERT [dbo].[NationalMatches] ON 

INSERT [dbo].[NationalMatches] ([ID], [NationalA], [NationalB], [Date], [Place], [LiveLink], [ScoreA], [ScoreB], [CompetitionID]) VALUES (4, 4, 7, CAST(N'2018-12-18' AS Date), N'cairo', NULL, 77, 69, 2)
SET IDENTITY_INSERT [dbo].[NationalMatches] OFF
SET IDENTITY_INSERT [dbo].[CompetitionMatches] ON 

INSERT [dbo].[CompetitionMatches] ([ID], [TeamA], [TeamB], [CompetitionID], [AgesID], [ScoreA], [ScoreB]) VALUES (6, 8, 9, 5, 0, 1, 2)
SET IDENTITY_INSERT [dbo].[CompetitionMatches] OFF
SET IDENTITY_INSERT [dbo].[PointsPrizes] ON 

INSERT [dbo].[PointsPrizes] ([ID], [NameA], [NameE], [Points], [CompetitionID]) VALUES (1, N'sdf', N'Ahly', 123, 1)
INSERT [dbo].[PointsPrizes] ([ID], [NameA], [NameE], [Points], [CompetitionID]) VALUES (2, N'Rules1', N'Ahly', 123, 1)
SET IDENTITY_INSERT [dbo].[PointsPrizes] OFF
SET IDENTITY_INSERT [dbo].[CompetitionTeams] ON 

INSERT [dbo].[CompetitionTeams] ([ID], [NameA], [NameE], [Logo]) VALUES (8, N'Rules1', N'sdf', N'16831304_B.png')
INSERT [dbo].[CompetitionTeams] ([ID], [NameA], [NameE], [Logo]) VALUES (9, N'اهلى ', N'sdf', N'unnamed.jpg')
SET IDENTITY_INSERT [dbo].[CompetitionTeams] OFF
SET IDENTITY_INSERT [dbo].[Coahes] ON 

INSERT [dbo].[Coahes] ([ID], [NameA], [NameE], [ImageUrl], [Age], [ClubID], [Category], [DescriptionA], [DescriptionE], [TitleA], [TitleE]) VALUES (3, N'عمرو أبو الخير', N'Amr Abo Elkheir', N'news5.png', 49, 13, N'الدرجة الأولى', N'<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	لاعب نادي الاتحاد السكندري و منتخب مصر السابق</p>
<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	المدير الفني لنادي الاتحاد السكندري سابقاً</p>
<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	المدير الفني لنادي إتحاد جدة سابقاً</p>
<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	المدير الفني لمنتخب مصر الأول للرجال سابقاً</p>
', N'<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	لاعب نادي الاتحاد السكندري و منتخب مصر السابق</p>
<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	المدير الفني لنادي الاتحاد السكندري سابقاً</p>
<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	المدير الفني لنادي إتحاد جدة سابقاً</p>
<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	المدير الفني لمنتخب مصر الأول للرجال سابقاً</p>
', N'<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	حائز على البطولة الإفريقيا 2 مرات</p>
<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	حائز على البطولة العربية للأندية 2 مرات</p>
<div>
	&nbsp;</div>
', N'<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	حائز على البطولة الإفريقيا 2 مرات</p>
<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	حائز على البطولة العربية للأندية 2 مرات</p>
<div>
	&nbsp;</div>
')
SET IDENTITY_INSERT [dbo].[Coahes] OFF

SET IDENTITY_INSERT [dbo].[Articles] ON 

INSERT [dbo].[Articles] ([ID], [TitleA], [TitleE], [ShortDescriptionA], [ShortDescriptionE], [LongDescriptionE], [LongDescriptionA], [Date], [ImageUrl]) VALUES (3, N'العاب اطفال', N'TEST', N'<p>
	<u><strong><em>asdfsdf</em></strong></u></p>
<p>
	<u><strong><em>asdfsdf</em></strong></u></p>
', N'<p>
	<u><strong><em>asdfsdf</em></strong></u></p>
', N'<p>
	<u><strong><em>asdfsdfsd</em></strong></u></p>
<p>
	<u><strong><em>sd</em></strong></u></p>
', N'<p>
	<u><strong><em>asdfsdf</em></strong></u></p>
', CAST(N'2018-12-28' AS Date), N'16831304_B.png')
SET IDENTITY_INSERT [dbo].[Articles] OFF
SET IDENTITY_INSERT [dbo].[CouncilPresidents] ON 

INSERT [dbo].[CouncilPresidents] ([ID], [ImageUrl], [NameA], [NameE], [BriefA], [BriefE], [TitleA], [TitleE]) VALUES (2, N'Screen Shot 2018-12-13 at 18.37.52.png', N'ا.د. مجدي أبو فريخة', N'Magdy Abo Freikha', N'<p>
	<span style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">رئيس الاتحاد المصري لكرة السلة الحالي وحتى عام ٢٠٢٠</span></p>
', N'<p>
	<span style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">رئيس الاتحاد المصري لكرة السلة الحالي وحتى عام ٢٠٢٠</span></p>
', N'رئيس الاتحاد', N'Head of Board')
INSERT [dbo].[CouncilPresidents] ([ID], [ImageUrl], [NameA], [NameE], [BriefA], [BriefE], [TitleA], [TitleE]) VALUES (3, N'abdelmotaled ebbfed.jpeg', N'م.محمد عبدالمطلب', N'Mohamed Abdelmotaleb', N'<h2 dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px;">
	<span style="font-size:16px;"><span style="font-family:arial,helvetica,sans-serif;">لاعب وقائد نادي الاتحاد السكندري السابق</span></span></h2>
<div dir="rtl">
	<h2 dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">
		<span style="font-size:16px;"><span style="font-family:arial,helvetica,sans-serif;">لاعب وقائد منتخب مصر السابق</span></span></h2>
	<h2>
		&nbsp;</h2>
</div>
<p>
	&nbsp;</p>
', N'<h2 dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px;">
	<span style="font-size: 16px;"><span style="font-family: arial, helvetica, sans-serif;">لاعب وقائد نادي الاتحاد السكندري السابق</span></span></h2>
<div dir="rtl">
	<h2 dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">
		<span style="font-size: 16px;"><span style="font-family: arial, helvetica, sans-serif;">لاعب وقائد منتخب مصر السابق</span></span></h2>
	<h2>
		&nbsp;</h2>
</div>
<p>
	&nbsp;</p>
', N'نائب رئيس الاتحاد', N'نائب رئيس الاتحاد')
SET IDENTITY_INSERT [dbo].[CouncilPresidents] OFF
SET IDENTITY_INSERT [dbo].[Decisions] ON 

INSERT [dbo].[Decisions] ([ID], [TitleA], [TitleE], [ShortDescriptionA], [ShortDescriptionE], [LongDescriptionE], [LongDescriptionA], [CreationDate], [ModifiedDate]) VALUES (4, N'dsf', N'sdf       ', N'<p>
	<strong>sdf</strong></p>
<p>
	sdfsdf</p>
<p>
	<strong>sdf</strong></p>
', N'<p>
	<strong>sdf</strong></p>
<p>
	&nbsp;</p>
<p>
	<strong>sdf</strong></p>
', N'<p>
	<strong>sdf</strong></p>
<p>
	&nbsp;</p>
<p>
	<strong>sdf</strong></p>
', N'<p>
	<strong>sdf</strong></p>
<p>
	&nbsp;</p>
<p>
	<strong>sdf</strong></p>
', CAST(N'0001-01-01' AS Date), CAST(N'2018-12-13' AS Date))
SET IDENTITY_INSERT [dbo].[Decisions] OFF
SET IDENTITY_INSERT [dbo].[HistoryUnions] ON 

INSERT [dbo].[HistoryUnions] ([ID], [EstablishedYearA], [EstablishedYearE], [AchievementA], [AchievementE]) VALUES (2, N'<p style="text-align: right;">
	<span style="color: rgb(28, 28, 28); background-color: rgba(255, 255, 255, 0.8); font-size: 14pt; font-family: &quot;arial black&quot;, sans-serif;"><strong><span style="color: rgb(0, 0, 0);">تم تكوين أول اتحاد عام بصفة رسمية وكان عددالفرق لا يزيد عن عشرة وكلها أجنبية ما عدا فريقي الحرس الملكي</span></strong></span><br style="color: rgb(28, 28, 28); font-family: &quot;Arial Unicode MS&quot;, Arial, Helvetica, sans-serif; font-size: 16px; background-color: rgba(255, 255, 255, 0.8);" />
	<span style="color: rgb(28, 28, 28); background-color: rgba(255, 255, 255, 0.8); font-size: 14pt; font-family: &quot;arial black&quot;, sans-serif;"><strong><span style="color: rgb(0, 0, 0);">والشبان المسيحية وكان ذلك عام 1930م</span></strong></span></p>
<p style="text-align: right;">
	&nbsp;</p>
', N'<p style="text-align: right;">
	<span style="color: rgb(28, 28, 28); background-color: rgba(255, 255, 255, 0.8); font-size: 14pt; font-family: &quot;arial black&quot;, sans-serif;"><strong><span style="color: rgb(0, 0, 0);">تم تكوين أول اتحاد عام بصفة رسمية وكان عددالفرق لا يزيد عن عشرة وكلها أجنبية ما عدا فريقي الحرس الملكي</span></strong></span><br style="color: rgb(28, 28, 28); font-family: &quot;Arial Unicode MS&quot;, Arial, Helvetica, sans-serif; font-size: 16px; background-color: rgba(255, 255, 255, 0.8);" />
	<span style="color: rgb(28, 28, 28); background-color: rgba(255, 255, 255, 0.8); font-size: 14pt; font-family: &quot;arial black&quot;, sans-serif;"><strong><span style="color: rgb(0, 0, 0);">والشبان المسيحية وكان ذلك عام 1930م</span></strong></span></p>
<div>
	&nbsp;</div>
', N'<p align="center" dir="rtl" style="margin: 10px 0px; padding: 0px; text-align: right;">
	<span style="font-size: 14pt; font-family: &quot;arial black&quot;, sans-serif;"><strong><span style="color: rgb(0, 0, 0);">وفي عام 1948م أشتركت مصر في دورة لندن ولكن لم تفز بأي من المراكز الأولى.</span></strong></span></p>
<p align="center" dir="rtl" style="margin: 10px 0px; padding: 0px; text-align: right;">
	<span style="font-size: 14pt; font-family: &quot;arial black&quot;, sans-serif;"><strong><span style="color: rgb(0, 0, 0);">وفي عام 1949مأقيمت بطولة أوروبا في القاهرة بعد اعتذار روسيا عن أقامتها، وظهر فيها الفريق المصري بمظهر مشرف إذ حازعلى المركز الأول بفوزه على فرنسا.</span></strong></span></p>
<p align="center" dir="rtl" style="margin: 10px 0px; padding: 0px; text-align: right;">
	<span style="font-size: 14pt; font-family: &quot;arial black&quot;, sans-serif;"><strong><span style="color: rgb(0, 0, 0);">وفي بيونس أيرس بالأرجنتين اشتركت مصر في بطولة العالم وفازت بالمرتبة الثالثة&nbsp;.</span></strong></span></p>
<p align="center" dir="rtl" style="margin: 10px 0px; padding: 0px; text-align: right;">
	<span style="font-size: 14pt; font-family: &quot;arial black&quot;, sans-serif;"><strong><span style="color: rgb(0, 0, 0);">وفي عام 1951 بمدينة الإسكندرية أقيمت دورة البحر الأبيض المتوسط وأنشئت وقتها أولصالة خاصة بكرة السلة وهي صالة ملعب بلدية الإسكندرية وفازت مصر بالمركز الأول، ثم أقامت مصر أول دورة عربية بالإسكندرية سنة 1952م&nbsp;&nbsp;وفازت فيها بالمرتبة الأولى.</span></strong></span></p>
<p align="center" dir="rtl" style="margin: 10px 0px; padding: 0px; text-align: right;">
	<span style="font-size: 14pt; font-family: &quot;arial black&quot;, sans-serif;"><strong><span style="color: rgb(0, 0, 0);">ثم اشتركت في الدورة الأوليمبية بهلسنكي&nbsp;،ثم بطولة أوروبا سنة 1953م في أسبانيا، وفي سنة 1954م في بطولة البحرالأبيض المتوسط في أسبانيا، وقد سافر الفريق المصرى إلى موسكو عام 1957م للاشتراكفي أعياد الشباب.</span></strong></span></p>
<p align="center" dir="rtl" style="margin: 10px 0px; padding: 0px; text-align: right;">
	<span style="font-size: 14pt; font-family: &quot;arial black&quot;, sans-serif;"><strong><span style="color: rgb(0, 0, 0);">وتوالت بعد ذلك الدورات المختلفة منهاالعالمية أو الأفريقية أو العربية.</span></strong></span></p>
<div class="sharedaddy sd-sharing-enabled" style="clear: both;">
	<div class="robots-nocontent sd-block sd-social sd-social-icon sd-sharing">
		<div class="sd-content">
			<ul style="margin: 0px 0px 0.7em !important; padding-right: 0px !important; padding-left: 0px !important; list-style: none !important;">
				<li class="share-whatsapp" style="margin: 0px 5px 5px 0px; padding: 0px; font-size: 16px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; color: rgb(41, 41, 41); overflow: visible hidden; display: inline-block; background-color: rgba(255, 255, 255, 0.8);">
					&nbsp;</li>
				<li class="share-facebook" style="margin: 0px 5px 5px 0px; padding: 0px; font-size: 16px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; color: rgb(41, 41, 41); overflow: visible hidden; display: inline-block; background-color: rgba(255, 255, 255, 0.8);">
					&nbsp;</li>
			</ul>
		</div>
	</div>
</div>
<p>
	&nbsp;</p>
', N'<p align="center" dir="rtl" style="margin: 10px 0px; padding: 0px; text-align: right;">
	<span style="font-size: 14pt; font-family: &quot;arial black&quot;, sans-serif;"><strong><span style="color: rgb(0, 0, 0);">وفي عام 1948م أشتركت مصر في دورة لندن ولكن لم تفز بأي من المراكز الأولى.</span></strong></span></p>
<p align="center" dir="rtl" style="margin: 10px 0px; padding: 0px; text-align: right;">
	<span style="font-size: 14pt; font-family: &quot;arial black&quot;, sans-serif;"><strong><span style="color: rgb(0, 0, 0);">وفي عام 1949مأقيمت بطولة أوروبا في القاهرة بعد اعتذار روسيا عن أقامتها، وظهر فيها الفريق المصري بمظهر مشرف إذ حازعلى المركز الأول بفوزه على فرنسا.</span></strong></span></p>
<p align="center" dir="rtl" style="margin: 10px 0px; padding: 0px; text-align: right;">
	<span style="font-size: 14pt; font-family: &quot;arial black&quot;, sans-serif;"><strong><span style="color: rgb(0, 0, 0);">وفي بيونس أيرس بالأرجنتين اشتركت مصر في بطولة العالم وفازت بالمرتبة الثالثة&nbsp;.</span></strong></span></p>
<p align="center" dir="rtl" style="margin: 10px 0px; padding: 0px; text-align: right;">
	<span style="font-size: 14pt; font-family: &quot;arial black&quot;, sans-serif;"><strong><span style="color: rgb(0, 0, 0);">وفي عام 1951 بمدينة الإسكندرية أقيمت دورة البحر الأبيض المتوسط وأنشئت وقتها أولصالة خاصة بكرة السلة وهي صالة ملعب بلدية الإسكندرية وفازت مصر بالمركز الأول، ثم أقامت مصر أول دورة عربية بالإسكندرية سنة 1952م&nbsp;&nbsp;وفازت فيها بالمرتبة الأولى.</span></strong></span></p>
<p align="center" dir="rtl" style="margin: 10px 0px; padding: 0px; text-align: right;">
	<span style="font-size: 14pt; font-family: &quot;arial black&quot;, sans-serif;"><strong><span style="color: rgb(0, 0, 0);">ثم اشتركت في الدورة الأوليمبية بهلسنكي&nbsp;،ثم بطولة أوروبا سنة 1953م في أسبانيا، وفي سنة 1954م في بطولة البحرالأبيض المتوسط في أسبانيا، وقد سافر الفريق المصرى إلى موسكو عام 1957م للاشتراكفي أعياد الشباب.</span></strong></span></p>
<p align="center" dir="rtl" style="margin: 10px 0px; padding: 0px; text-align: right;">
	<span style="font-size: 14pt; font-family: &quot;arial black&quot;, sans-serif;"><strong><span style="color: rgb(0, 0, 0);">وتوالت بعد ذلك الدورات المختلفة منهاالعالمية أو الأفريقية أو العربية.</span></strong></span></p>
<div class="sharedaddy sd-sharing-enabled" style="clear: both;">
	<div class="robots-nocontent sd-block sd-social sd-social-icon sd-sharing">
		<div class="sd-content">
			<ul style="margin: 0px 0px 0.7em !important; padding-right: 0px !important; padding-left: 0px !important; list-style: none !important;">
				<li class="share-whatsapp" style="margin: 0px 5px 5px 0px; padding: 0px; font-size: 16px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; color: rgb(41, 41, 41); overflow: visible hidden; display: inline-block; background-color: rgba(255, 255, 255, 0.8);">
					&nbsp;</li>
				<li class="share-facebook" style="margin: 0px 5px 5px 0px; padding: 0px; font-size: 16px; font-family: Arial, &quot;Arial Unicode MS&quot;, Helvetica, sans-serif; color: rgb(41, 41, 41); overflow: visible hidden; display: inline-block; background-color: rgba(255, 255, 255, 0.8);">
					&nbsp;</li>
			</ul>
		</div>
	</div>
</div>
<p>
	&nbsp;</p>
')
INSERT [dbo].[HistoryUnions] ([ID], [EstablishedYearA], [EstablishedYearE], [AchievementA], [AchievementE]) VALUES (3, N'<p>
	dfsd</p>
<p>
	sdfsdf<strong>dsfsdf</strong></p>
', N'<p>
	dfsd</p>
<p>
	sdfsdf<strong>dsfsdfs</strong></p>
', N'<p>
	dfsd</p>
<p>
	sdfsdf<strong>dsfsdf</strong></p>
', N'<p>
	dfsd</p>
<p>
	sdfsdf<strong>dsfsdf</strong></p>
')
SET IDENTITY_INSERT [dbo].[HistoryUnions] OFF
SET IDENTITY_INSERT [dbo].[Judges] ON 

INSERT [dbo].[Judges] ([ID], [NameA], [NameE], [ImageUrl], [Category], [DescriptionA], [DescriptionE]) VALUES (1, N'وجدى نجيب عوض الله', N'Wagdy Naguib', N'16831304_B.png', N'دولى', N'<h2 dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	<span style="font-size:16px;"><span style="font-family:arial,helvetica,sans-serif;">حكم&nbsp;<span style="text-align: center;">دولى من 2013</span></span></span></h2>
<div>
	&nbsp;</div>
', N'<h2 dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	<span style="font-size: 16px;"><span style="font-family: arial, helvetica, sans-serif;">حكم&nbsp;<span style="text-align: center;">دولى من 2013</span></span></span></h2>
<div>
	&nbsp;</div>
')
INSERT [dbo].[Judges] ([ID], [NameA], [NameE], [ImageUrl], [Category], [DescriptionA], [DescriptionE]) VALUES (2, N'هبه محمد وجيه الحديدى', N'Heba Mohamed', N'16831304_B.png', N'دولى', N'<h2 dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	<span style="font-size: 16px;"><span style="font-family: arial, helvetica, sans-serif;">حكم&nbsp;<span style="text-align: center;">دولى من 2015</span></span></span></h2>
<div>
	&nbsp;</div>
', N'<h2 dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	<span style="font-size: 16px;"><span style="font-family: arial, helvetica, sans-serif;">حكم&nbsp;<span style="text-align: center;">دولى من 2015</span></span></span></h2>
')
INSERT [dbo].[Judges] ([ID], [NameA], [NameE], [ImageUrl], [Category], [DescriptionA], [DescriptionE]) VALUES (3, N'خالد محمد رضوان احمد', N'Khaled Mohamed', N'16831304_B.png', N'دولى', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Judges] OFF
SET IDENTITY_INSERT [dbo].[RulesAndLaws] ON 

INSERT [dbo].[RulesAndLaws] ([ID], [NameA], [PdfName], [NameE], [ShortDescriptionA], [ShortDescriptionE]) VALUES (4, N'قوانين مسابقات الرجال', N'طلب-قيد-مدرب.pdf', N'Men competitons'' rules', NULL, NULL)
SET IDENTITY_INSERT [dbo].[RulesAndLaws] OFF
SET IDENTITY_INSERT [dbo].[TrainingCourses] ON 

INSERT [dbo].[TrainingCourses] ([ID], [TitleA], [TitleE], [ShortDescriptionA], [ShortDescriptionE], [LongDescriptionA], [LongDescriptionE], [Location], [Date], [CourseType]) VALUES (5, N'دورة الترقي لحكام الدرجة الثانية', N'دورة الترقي لحكام الدرجة الثانية', N'<p>
	<span style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">دورة الترقي لحكام الدرجة الثانية</span></p>
', N'<p>
	<span style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">دورة الترقي لحكام الدرجة الثانية</span></p>
', N'<p>
	<span style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">دورة الترقي لحكام الدرجة الثانية</span></p>
', N'<p>
	<span style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">دورة الترقي لحكام الدرجة الثانية</span></p>
', N'Cairo', CAST(N'2018-12-27' AS Date), N'Judges')
INSERT [dbo].[TrainingCourses] ([ID], [TitleA], [TitleE], [ShortDescriptionA], [ShortDescriptionE], [LongDescriptionA], [LongDescriptionE], [Location], [Date], [CourseType]) VALUES (6, N'دورة الترقي لمدربين الدرجة الثانية', N'دورة الترقي لمدربين الدرجة الثانية', N'<p>
	<span style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">دورة الترقي لمدربين الدرجة الثانية</span></p>
', N'<p>
	<span style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">دورة الترقي لمدربين الدرجة الثانية</span></p>
', N'<p>
	<span style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">دورة الترقي لمدربين الدرجة الثانية</span></p>
', N'<p>
	<span style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">دورة الترقي لمدربين الدرجة الثانية</span></p>
', N'Alex', CAST(N'2019-01-09' AS Date), N'Coaches')
SET IDENTITY_INSERT [dbo].[TrainingCourses] OFF
SET IDENTITY_INSERT [dbo].[UnionPersons] ON 

INSERT [dbo].[UnionPersons] ([ID], [ImageUrl], [NameA], [BriefA], [NameE], [BriefE]) VALUES (3, N'Screen Shot 2018-12-17 at 17.34.42.png', N'ناشد بك حنا', N'<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	رئيس الاتحاد بين عام ١٩٤٣ و ١٩٤٨</p>
<div>
	&nbsp;</div>
', N'Nashed Bek Henna', N'<p dir="rtl" style="margin: 0px 0px 0.2em; padding: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px;">
	رئيس الاتحاد بين عام ١٩٤٣ و ١٩٤٨</p>
<div>
	&nbsp;</div>
')
INSERT [dbo].[UnionPersons] ([ID], [ImageUrl], [NameA], [BriefA], [NameE], [BriefE]) VALUES (4, N'Screen Shot 2018-12-17 at 17.34.58.png', N'عبدالمنعم وهبى حسين', N'<p>
	<span style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">رئيس الاتحاد 3 دورات ١٩٥٢ - ١٩٥٣ و- ١٩٦٠ - ١٩٦٣ وأخيراً ١٩٦٩ - ١٩٧٢</span></p>
', N'AbdelMoneom Wahby', N'<p>
	<span style="color: rgb(0, 0, 0); font-family: Tahoma; font-size: 14px; background-color: rgb(253, 253, 253);">رئيس الاتحاد 3 دورات ١٩٥٢ - ١٩٥٣ و- ١٩٦٠ - ١٩٦٣ وأخيراً ١٩٦٩ - ١٩٧٢</span></p>
')
SET IDENTITY_INSERT [dbo].[UnionPersons] OFF
